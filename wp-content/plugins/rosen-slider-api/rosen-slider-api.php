<?php
/**
 * Plugin Name: Rosen Slider API
 * Plugin URI: http://www.eaglevisionit.com
 * Description: Rosen Slider API
 * Version: 1.0.0
 * Author: Eaglevision IT
 * Author URI: http://www.eaglevisionit.com
 * License: GPL2
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//  Define Constants
define('RS_API_DIR', plugin_dir_path(__FILE__));
define('RS_API_URI', plugin_dir_url(__FILE__));

require RS_API_DIR . 'post-types/loader.php';

function rs_api_init() {
	$RS_API_Slider = new RS_API_Slider();
  	$RS_API_Slider->register_routes();
}
add_action( 'rest_api_init', 'rs_api_init' );
class RS_API_Slider extends WP_REST_Controller{
	private $category;
	
	function __construct() {
		if( isset( $_GET['cat'])){
			$this->category = $_GET['cat'];
		}else{
			$this->category = false;
		}
    }
   	
   	public function register_routes() {
        $version = '1';
        $namespace = 'rosen';
        $base = '/';
        
        register_rest_route( $namespace, '/vc-style' . $base, array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_vc_style' ),
                'args'            => array(),
            )
        ) );

        register_rest_route( $namespace, '/slider/all' . $base, array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_all' ),
                'args'            => array(),
            )
        ) );

        register_rest_route( $namespace, '/status-tester' . $base, array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'set_status_based_on_date' ),
                'args'            => array(),
            )
        ) );


        register_rest_route( $namespace, '/sliders' . $base, array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_rs_slider' ),
                'args'            => array(),
            )
        ) );

        register_rest_route( $namespace, '/news' . $base, array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_rs_news' ),
                'args'            => array(),
            )
        ) );

        register_rest_route( $namespace, '/notice' . $base, array(
            array(
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => array( $this, 'get_rs_notice' ),
                'args'            => array(),
            )
        ) );
	}


	public function set_status_based_on_date($request){
		$args = array(
			'post_type'   		=> array( 'slider','news'),
			'post_status' 		=> array('publish','scheduled'),
			'posts_per_page'    => -1
		);

		file_put_contents(__DIR__.'/log.txt', date("Y-m-d H:i:s",time())."\n", FILE_APPEND);

	    $all_post = new WP_Query($args);
	    if($all_post->have_posts()):
	        while($all_post->have_posts()):$all_post->the_post();
	            $start_date 	= get_post_meta(get_the_ID(), 'start_date', true);
	            $end_date 		= get_post_meta(get_the_ID(), 'end_date', true);
	            $current_date 	= date('Y-m-d');
	            $new_data = array();
	            if( !empty($start_date) && !empty($end_date) ){
	                if( ($start_date <= $current_date) && ($end_date >= $current_date) ){
	                    $new_data = array(
	                        'ID'    =>  get_the_ID(),
	                        'post_status'   =>  'publish'
	                    );
	                }else if( $current_date > $end_date ){
	                    $new_data = array(
	                        'ID'    =>  get_the_ID(),
	                        'post_status'   =>  'expired'
	                    );
	                }else if( $current_date < $start_date ){
	                    $new_data = array(
	                        'ID'    => get_the_ID(),
	                        'post_status'   =>  'scheduled'
	                    );
	                }
	            }else{
	                $new_data = array(
	                    'ID'    =>  get_the_ID(),
	                    'post_status'   =>  'publish'
	                );
	            }

	            wp_update_post( $new_data );

	        endwhile;

	        wp_reset_postdata();
	    endif;
	}

	public function getResponse( $data, $msg='success', $status=200 ){
   		$response 					= new WP_REST_Response();
        $message['status'] 			= $status;
        $message['currentTimestamp'] = current_time('timestamp', 1);
		$message['message'] 		= $msg;
		$message['data'] 			= $data;
		

		$response->set_data( $message);
		$response->set_status( $status );
		$response->header("Access-Control-Allow-Origin","*");
		return $response;
    }
    
    public function get_all( $request ) {
    	$sliders = $this->get_rs_slider( $request );
    	$news = $this->get_rs_news( $request );
    	$notices = $this->get_rs_notice( $request );
    	$all_plugins = get_plugins();
    	$vc_style = '';
    	if( in_array('js_composer/js_composer.php', array_keys($all_plugins) ) ) {
    		$vc_style = plugin_dir_url( 'js_composer/js_composer.php').'assets/css/js_composer.min.css';
    	}

    	$settings = array(
    			'rosen_logo' 		=> html_entity_decode( get_option( 'rosen_slider_logo') ),
    			'rosen_news_title' 	=> html_entity_decode( get_option('rosen_news_title' ) ),
    			'vc_style'			=> $vc_style
    		);

    	$return = array(
    		'sliders' 		=> $sliders->data['data'],
    		'news'			=> $news->data['data'],
    		'notices'		=> $notices->data['data'],
    		'settings' 		=> $settings
    	);

    	return $this->getResponse( $return );
    }

    public function get_vc_style( $request ) {
    // 	$styles = WP_Styles();
    // 	print_r( $styles);
    }

	public function get_rs_slider( $request ) {

		WPBMap::addAllMappedShortcodes(); // This does all the work

		$current_date 	= date('Y-m-d');

		$args = array(
			'post_type'   => 'slider',
			'post_status' => 'publish',
			'order'               => 'DESC',
			'posts_per_page'         => -1,
			'meta_query' => array(
				'relation' => 'OR',
				
				array(
					'relation' => 'OR',
					array(
						'key'     	=> 'start_date',
						'value'	  	=> ''	
					),
					array(
						'key'     	=> 'end_date',
						'value' 	=> '',
					),
				),

				array(
					'relation' => 'AND',
					array(
						'key'     => 'start_date',
						'value'   => $current_date,
						'compare' => '<=',
					),
					array(
						'key'     => 'end_date',
						'value'   => $current_date,
						'compare' => '>=',
					)
				)
			)
		);

		$term = get_term_by('slug', $this->category, 'slider-category');
		if( $term && $this->category ){
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'slider-category',
					'field'    => 'slug',
					'terms'    => $this->category,
				),
			);
		}
		
		$the_query = new WP_Query( $args );
		
		$data = array();
		if ( $the_query->have_posts() ) :
			$post_count = 1 ; 
			while ( $the_query->have_posts() ) : $the_query->the_post();
			 $slider_time = get_post_meta( get_the_ID(), 'rosen_slider_time', true );
			 $rosen_slider_effect = get_post_meta( get_the_ID(), 'rosen_slider_effect', true );
			 $rosen_slider_speed = get_post_meta( get_the_ID(), 'rosen_slider_speed', true );
			 $is_fullwidth = get_post_meta( get_the_ID(), 'rosen_slider_is_fullwidth', true );
			 $custom_css = get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
			 $custom_css = $custom_css ? $custom_css : false;

			 $is_fullwidth = $is_fullwidth ? $is_fullwidth : "no";

			  $temp['ID'] = get_the_ID();
			  $temp['title'] = esc_attr(get_the_title());
			  $temp['autoslide'] = $slider_time;
			  $temp['transition'] = $rosen_slider_effect;
			  $temp['transition_speed'] = $rosen_slider_speed;
			  $temp['is_fullwidth'] = $is_fullwidth;

			  $temp['content'] =  apply_filters( 'the_content', get_the_content() );
			  if ($post_count == $the_query->post_date_column_time - 1 ){
			  	$temp['last_post'] = true;
			  }

			  
			  $temp['custom_css'] = $custom_css;

			  $data[] = $temp;
			  $post_count ++;
			endwhile;
		endif;
		$data['term'] = $term;
		// Reset Post Data
		wp_reset_postdata();

		return $this->getResponse( $data );
		
	}

	public function get_rs_news( $request ) {
		WPBMap::addAllMappedShortcodes(); // This does all the work

		$current_date 	= date('Y-m-d');
		$args = array(
			'post_type'   => 'news',
			'post_status' => 'publish',
			'order'       => 'DESC',
			'orderby'     => 'DATE',
			'posts_per_page'   => -1,
			'meta_query' => array(
				'relation' => 'OR',
				
				array(
					'relation' => 'OR',
					array(
						'key'     	=> 'start_date',
						'value'	  	=> ''	
					),
					array(
						'key'     	=> 'end_date',
						'value' 	=> '',
					),
				),

				array(
					'relation' => 'AND',
					array(
						'key'     => 'start_date',
						'value'   => $current_date,
						'compare' => '<=',
					),
					array(
						'key'     => 'end_date',
						'value'   => $current_date,
						'compare' => '>=',
					)
				)
			)
		);

        $term = get_term_by('slug', $this->category, 'slider-category');
        if( $term && $this->category ){
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'slider-category',
                    'field'    => 'slug',
                    'terms'    => $this->category,
                ),
            );
        }

		$the_query = new WP_Query( $args );
		$data = array();
		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
			  $temp['ID'] = get_the_ID();
			  $temp['title'] = esc_attr(get_the_title());
			  $temp['content'] =  apply_filters( 'the_content', get_the_content() );
			  $temp['date']	= get_the_date( );
			  $temp['slide_time'] = get_post_meta($temp['ID'], 'slide_time', true);
			  
			  $data[] = $temp;
			endwhile;
		endif;
		// Reset Post Data
		wp_reset_postdata();

		return $this->getResponse( $data );
	}

	public function get_rs_notice( $request ) {
		//WPBMap::addAllMappedShortcodes(); // This does all the work

		$current_day 	= ltrim(date("d"), '0');
		$current_month 	= ltrim(date("m"), '0')-1;
	
		$args = array(	
			'post_type'   => 'notice',
			'post_status' => 'publish',
			'order'       => 'DESC',
			'orderby'     => 'DATE',
			'posts_per_page'   => -1,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key'     => 'show_day',
					'value'   => $current_day,
					'compare' => '=',
				),
				array(
					'key'     => 'show_month',
					'value'   => $current_month,
					'compare' => '=',
				),
			)
		);
	
		$the_query = new WP_Query( $args );
		$data = array();
		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
			  $temp['ID'] = get_the_ID();
			  $temp['title'] = esc_attr(get_the_title());
			  $temp['date']	= get_the_date( );

                /*Check for birthday notice*/
                $is_birthday_notice = get_post_meta(get_the_ID(), 'is_birthday_notice', true);
                if('yes' == $is_birthday_notice){
                    $temp['birthday'] = 'birthday';
                }else{
                    $temp['birthday'] = '';
                }
                /**/

			  $data[] = $temp;
			endwhile;
		endif;
		// Reset Post Data
		wp_reset_postdata();

		return $this->getResponse( $data );
	}
}
