<?php

include __DIR__.'/slider.php';
include __DIR__.'/notice.php';
include __DIR__.'/news.php';


/* Extra column for slider and news to show the publishing date */
add_action( 'manage_slider_posts_custom_column' , 'date_based_custom_column', 10, 2 );
add_action( 'manage_news_posts_custom_column' , 'date_based_custom_column', 10, 2 );

function date_based_custom_column( $column, $post_id ) {
	switch ( $column ) {
		case 'publish_status':
			$start_date 	= get_post_meta($post_id, 'start_date', true);
	 		$end_date 		= get_post_meta($post_id, 'end_date', true);
	 		$current_date 	= date('Y-m-d');

	 		if($start_date && $end_date){
	 			if( ( $start_date <= $current_date ) && ($current_date <= $end_date) ){
		 			/* Running */
		 			$period = sprintf( __( 'Started %s ago', 'rosenlundsakeri' ) ,human_time_diff( current_time('timestamp'), strtotime($start_date) ) );
		 		}else if( $current_date > $end_date ){
		 			/* Expired */
		 			$period = sprintf( __( 'Expired %s ago', 'rosenlundsakeri' ) ,human_time_diff( strtotime($end_date), current_time('timestamp') ) );
		 		}else {
		 			/* Upcomming: $current_date < $start_date */
		 			$period = sprintf( __( '%s to start', 'rosenlundsakeri' ) ,human_time_diff( strtotime($start_date), current_time('timestamp') ) );
		 		}
	 		}else{
	 			$period = __( 'Forever', 'rosenlundsakeri' );
	 		}

	 		

	 		echo $period;
		break;
	}
}

add_filter( 'manage_edit-slider_columns', 'my_edit_movie_columns' ) ;
add_filter( 'manage_edit-news_columns', 'my_edit_movie_columns' ) ;
function my_edit_movie_columns( $columns ) {
	$columns['publish_status'] = __('Publish Status','rosenlundsakeri');
	return $columns;
}


/*add_action('admin_init','set_status_based_on_date');
function set_status_based_on_date(){
	$args = array(
		'post_type'   		=> array( 'slider','news'),
		'post_status' 		=> array('publish','scheduled'),
		'posts_per_page'    => -1
	);

    $all_post = new WP_Query($args);
    if($all_post->have_posts()):
        while($all_post->have_posts()):$all_post->the_post();
            $start_date 	= get_post_meta(get_the_ID(), 'start_date', true);
            $end_date 		= get_post_meta(get_the_ID(), 'end_date', true);
            $current_date 	= date('Y-m-d');
            $new_data = array();
            if( !empty($start_date) && !empty($end_date) ){
                if( ($start_date <= $current_date) && ($end_date >= $current_date) ){
                    $new_data = array(
                        'ID'    =>  get_the_ID(),
                        'post_status'   =>  'publish'
                    );
                }else if( $current_date > $end_date ){
                    $new_data = array(
                        'ID'    =>  get_the_ID(),
                        'post_status'   =>  'expired'
                    );
                }else if( $current_date < $start_date ){
                    $new_data = array(
                        'ID'    => get_the_ID(),
                        'post_status'   =>  'scheduled'
                    );
                }
            }else{
                $new_data = array(
                    'ID'    =>  get_the_ID(),
                    'post_status'   =>  'publish'
                );
            }


            // unhook this function so it doesn't loop infinitely
            remove_action('save_post', 'update_news_metadata');
            remove_action('save_post', 'update_notice_metadata');
            remove_action('save_post', 'update_slide_metadata');

            // update the post, which calls save_post again
            wp_update_post( $new_data );

            // re-hook this function
            add_action('save_post', 'update_news_metadata');
            add_action('save_post', 'update_notice_metadata');
            add_action('save_post', 'update_slide_metadata');

            //file_put_contents(__DIR__.'/test.txt',print_r($new_data,true),FILE_APPEND);
        endwhile;wp_reset_postdata();
    endif;
}*/


function slider_and_news_status(){
	register_post_status( 'expired', array(
		'label'                     => _x( 'Expired', 'slider' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Expired <span class="count">(%s)</span>', 'Expired <span class="count">(%s)</span>' ),
	) );

	register_post_status( 'scheduled', array(
		'label'                     => _x( 'Scheduled', 'slider' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Scheduled <span class="count">(%s)</span>', 'Scheduled <span class="count">(%s)</span>' ),
	) );
}
add_action( 'init', 'slider_and_news_status' );



add_action('admin_footer', 'append_slider_and_news_post_status');
function append_slider_and_news_post_status(){
    global $post;
    $val = '';
    $text = '';
    if($post){
        if( $post->post_type == 'slider' || $post->post_type == 'news' ){
            if($post->post_status == 'expired'){
                $val = 'expired';
                $text = 'Expired';
            }else if($post->post_status == 'scheduled'){
                $val = 'scheduled';
                $text = 'Scheduled';
            }

            $options = 'jQuery("select#post_status").append("<option value=\"publish\" >Publish</option>");';
            $span = '';

            if($text){
                $options .= 'jQuery("select#post_status").append("<option value=\"'.$val.'\" selected >'.$text.'</option>");';
                $span = 'jQuery(".misc-pub-section #post-status-display").text("'.$text.'");';
            }
        }
    }
}


function maybe_notify_users($post_id){
	/*

	$users = null;
	$post = get_post($post_id);
	$shall_notify = $_POST['notify'];

	if( !$shall_notify ||  $post->post_status == 'auto-draft' ){
        return;
    }

	//$notified 	= get_post_meta( $post_id, 'notified', true );

   	$users = get_users(array(
		'blog_id'      => 3,
		'role__not_in' => array('administrator'),
		'fields' => array('user_email')
	));

	foreach ( $users as $user ){
		wp_mail( $user->user_email, 'test', 'Test Email' );
	}

    //update_post_meta( $post_id, 'notified', 0 );
    */
}
