<?php

add_action( 'init', 'notice_init' );
function notice_init() {
	register_post_type( 'notice', array(
		'labels'            => array(
			'name'                => __( 'Födelsedagar', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Födelsedagar', 'rosenlundsakeri' ),
			'all_items'           => __( 'Alla Födelsedagar', 'rosenlundsakeri' ),
			'new_item'            => __( 'Lägg till ny födelsedag', 'rosenlundsakeri' ),
			'add_new'             => __( 'Lägg till ny födelsedag', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Lägg till ny födelsedag', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Födelsedagar', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Födelsedagar', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Födelsedagar', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Födelsedagar found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Födelsedagar found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Födelsedagar', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Födelsedagar', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'		=> 27,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-nametag',
		'show_in_rest'      => true,
		'rest_base'         => 'notice',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );
}

add_filter( 'post_updated_messages', 'notice_updated_messages' );
function notice_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['notice'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Notice updated. <a target="_blank" href="%s">View notice</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'rosenlundsakeri'),
		3 => __('Custom field deleted.', 'rosenlundsakeri'),
		4 => __('Notice updated.', 'rosenlundsakeri'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Notice restored to revision from %s', 'rosenlundsakeri'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Notice published. <a href="%s">View notice</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		7 => __('Notice saved.', 'rosenlundsakeri'),
		8 => sprintf( __('Notice submitted. <a target="_blank" href="%s">Preview notice</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Notice scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview notice</a>', 'rosenlundsakeri'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Notice draft updated. <a target="_blank" href="%s">Preview notice</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}

add_action( 'add_meta_boxes', 'rosen_notice_metadata' );
function rosen_notice_metadata(){
	add_meta_box( 'notice', __( 'Notice Date', 'rosenlundsakeri' ),
		'notice_meta_box', 
		'notice', 
		'side',
		'high'
	);
    add_meta_box( 'birthday', __( 'Birthday', 'rosenlundsakeri' ),
        'birthday_meta_box',
        'notice',
        'side',
        'high'
    );
}

function notice_meta_box( $post ){
    $show_month = get_post_meta($post->ID, 'show_month', true);
    $show_day 	= get_post_meta($post->ID, 'show_day', true);

    $months = array(
    	__('January', 'rosenlundsakeri'),
    	__('February', 'rosenlundsakeri'),
    	__('March', 'rosenlundsakeri'),
    	__('April', 'rosenlundsakeri'),
    	__('May', 'rosenlundsakeri'),
    	__('June', 'rosenlundsakeri'),
    	__('July', 'rosenlundsakeri'),
    	__('August', 'rosenlundsakeri'),
    	__('September', 'rosenlundsakeri'),
    	__('October', 'rosenlundsakeri'),
    	__('November', 'rosenlundsakeri'),
    	__('December', 'rosenlundsakeri')
    );

    wp_nonce_field( 'notice_box_data', 'notice_box_nonce' );
?>
    <table class="form-table">
		<tbody>
			<tr>
				<td colspan="2" >
					<select name="show_month">
						<?php
							echo "<option value='' >".__('Select Month', 'rosenlundsakeri')."</option>";
							for( $i=0; $i<count($months); $i++ ){
								echo "<option value='".$i."' ".( ($i == $show_month)?'selected':'' )." >".$months[$i]."</option>";
							}
						?>
					</select>

					<select name="show_day">
						<?php
							echo "<option value='' >".__('Select Day', 'rosenlundsakeri')."</option>";
							for( $i=1; $i<=31; $i++ ){
								echo "<option value='".$i."' ".( ($i == $show_day)?'selected':'' )." >".$i."</option>";
							}
						?>
					</select>

				</td>
			</tr>
		</tbody>
	</table>
    <?php
}


function birthday_meta_box($post){
    $is_birthday_notice = get_post_meta($post->ID, 'is_birthday_notice', true);
    wp_nonce_field( 'notice_box_data', 'notice_box_nonce' );
    ?>
    <table class="form-table">
        <tbody>
        <tr>
            <td colspan="2" >
                <label for="is-birthday-notice">
                    <input type="checkbox" name="is_birthday_notice" id="is-birthday-notice" value="yes" <?php if ( !empty ( $is_birthday_notice ) ) checked( $is_birthday_notice, 'yes' ); ?> />
                    <?php _e( 'Is Birthday Notice?', 'rosenlundsakeri' )?>
                </label>
            </td>
        </tr>
        </tbody>
    </table>
<?php
}
 
add_action( "save_post", "update_notice_metadata" );
function update_notice_metadata( $post_id ){
    if( wp_verify_nonce( @$_POST['notice_box_nonce'], 'notice_box_data' ) ){
    	
    	$show_day = isset($_POST['show_day'])?$_POST['show_day']:'';
    	$show_month = isset($_POST['show_month'])?$_POST['show_month']:'';
        $is_birthday_notice = isset($_POST['is_birthday_notice']) ? 'yes': '';

        update_post_meta( $post_id, 'show_day', $show_day );
        update_post_meta( $post_id, 'show_month', $show_month );
        update_post_meta( $post_id, 'is_birthday_notice', $is_birthday_notice );

    }
}