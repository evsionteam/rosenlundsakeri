<?php

function slider_init() {
	register_post_type( 'slider', array(
		'labels'            => array(
			'name'                => __( 'Bildspel', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Bildspel', 'rosenlundsakeri' ),
			'all_items'           => __( 'Alla slides', 'rosenlundsakeri' ),
			'new_item'            => __( 'New Bildspel', 'rosenlundsakeri' ),
			'add_new'             => __( 'Lägg till nytt', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Lägg till nytt', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Bildspel', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Bildspel', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Bildspel', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Bildspel found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Bildspel found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Bildspel', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Bildspel', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'		=> 28,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor','thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'taxonomies'         => array('slider-category'),
		'menu_icon'         => 'dashicons-format-gallery',
		'show_in_rest'      => true,
		'rest_base'         => 'slider',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'slider_init' );

function slider_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['slider'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Slider updated. <a target="_blank" href="%s">View slider</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'rosenlundsakeri'),
		3 => __('Custom field deleted.', 'rosenlundsakeri'),
		4 => __('Slider updated.', 'rosenlundsakeri'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Slider restored to revision from %s', 'rosenlundsakeri'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Slider published. <a href="%s">View slider</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		7 => __('Slider saved.', 'rosenlundsakeri'),
		8 => sprintf( __('Slider submitted. <a target="_blank" href="%s">Preview slider</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Slider scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview slider</a>', 'rosenlundsakeri'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Slider draft updated. <a target="_blank" href="%s">Preview slider</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'slider_updated_messages' );


add_action( 'add_meta_boxes', 'rosen_slider_metadata' );
function rosen_slider_metadata(){
	add_meta_box( 'slider', __( 'Slider inställningar', 'rosenlundsakeri' ),
		'slider_meta_box', 
		'slider', 
		'side', //, 'normal', 'side' or  'advanced'
		'high' // ('high', 'low').
	);
}

function slider_meta_box( $post ){
    $start_date 	= get_post_meta($post->ID, 'start_date', true);
 	$end_date 		= get_post_meta($post->ID, 'end_date', true);
    $rosen_slider_time = get_post_meta( $post->ID,'rosen_slider_time', true );
	$rosen_slider_effect = get_post_meta( $post->ID,'rosen_slider_effect', true );
	$rosen_slider_speed = get_post_meta( $post->ID,'rosen_slider_speed', true );
	$rosen_slider_is_fullwidth = get_post_meta( $post->ID,'rosen_slider_is_fullwidth', true );
	
    wp_nonce_field( 'slider_box_data', 'slider_box_nonce' );
?>
    <table class="form-table">
		<tbody>
			<tr>
				<th><?php _e('Start Date', 'rosenlundsakeri'); ?></th>
				<td>
					<input type="date" name="start_date" value="<?php echo $start_date; ?>" />
				</td>
			</tr>

			<tr>
				<th><?php _e( 'End Date', 'rosenlundsakeri' ); ?></th>
				<td>
					<input type="date" name="end_date" value="<?php echo $end_date; ?>" />
				</td>
			</tr>

			<tr>
				<th><?php _e('Tid', 'rosenlundsakeri'); ?></th>
				<td>
					<input type="text" placeholder="5000" name="rosen_slider_time" value="<?php echo $rosen_slider_time; ?>" />
				</td>
			</tr>
			
			<tr>
				<th><?php _e('Effect', 'rosenlundsakeri'); ?></th>
				<td>
					<select name="rosen_slider_effect">
						<option value="none" <?php selected( 'none', $rosen_slider_effect ); ?>>none</option>
						<option value="fade" <?php selected( 'fade', $rosen_slider_effect ); ?>>fade</option>
						<option value="slide-in" <?php selected( 'slide-in', $rosen_slider_effect ); ?>>slide-in</option>
						<option value="slide-out" <?php selected( 'slide-out', $rosen_slider_effect ); ?>>slide-out</option>
						<option value="slide" <?php selected( 'slide', $rosen_slider_effect ); ?>>slide</option>
						<option value="convex" <?php selected( 'convex', $rosen_slider_effect ); ?>>convex</option>
						<option value="concave" <?php selected( 'concave', $rosen_slider_effect ); ?>>concave</option>
						<option value="zoom" <?php selected( 'zoom', $rosen_slider_effect ); ?>>zoom</option>
					</select>
				</td>
			</tr>

			<tr>
				<th><?php _e('Speed', 'rosenlundsakeri'); ?></th>
				<td>
					<select name="rosen_slider_speed">
						<option value="default" <?php selected( 'default', $rosen_slider_speed ); ?>>default</option>
						<option value="fast" <?php selected( 'fast', $rosen_slider_speed ); ?>>fast</option>
						<option value="slow" <?php selected( 'slow', $rosen_slider_speed ); ?>>slow</option>
					</select>

				</td>
			</tr>

			<tr>
				<th><?php _e('Helskärm (Ja/Nej)', 'rosenlundsakeri'); ?></th>
				<td>
					<select name="rosen_slider_is_fullwidth">
						<option value="no" <?php selected( 'no', $rosen_slider_is_fullwidth ); ?>><?php _e("No", 'rosenlundsakeri'); ?></option>
						<option value="yes" <?php selected( 'yes', $rosen_slider_is_fullwidth ); ?>><?php _e("Yes", 'rosenlundsakeri'); ?></option>
					</select>

				</td>
			</tr>
		</tbody>
	</table>
    <?php
}
 
add_action( "save_post", "update_slide_metadata" );
function update_slide_metadata( $post_id ){
    if( wp_verify_nonce( @$_POST['slider_box_nonce'], 'slider_box_data' ) ){

 		$start_date 				= isset($_POST['start_date'])?$_POST['start_date']:'';
 		$end_date 					= isset($_POST['end_date'])?$_POST['end_date']:'';
 		$rosen_slider_time 			= isset($_POST['rosen_slider_time'])?$_POST['rosen_slider_time']:'';
 		$rosen_slider_effect 		= isset($_POST['rosen_slider_effect'])?$_POST['rosen_slider_effect']:'';
 		$rosen_slider_speed 		= isset($_POST['rosen_slider_speed'])?$_POST['rosen_slider_speed']:'';
 		$rosen_slider_is_fullwidth 	= isset($_POST['rosen_slider_is_fullwidth'])?$_POST['rosen_slider_is_fullwidth']:'';

        update_post_meta( $post_id, 'rosen_slider_time', $rosen_slider_time );
        update_post_meta( $post_id, 'rosen_slider_effect', $rosen_slider_effect );
        update_post_meta( $post_id, 'rosen_slider_speed', $rosen_slider_speed );
        update_post_meta( $post_id, 'rosen_slider_is_fullwidth', $rosen_slider_is_fullwidth );

        if( empty($start_date) && !empty($end_date) ){
			$start_date = date('Y-m-d');
		}

        update_post_meta( $post_id, 'start_date', $start_date );
        update_post_meta( $post_id, 'end_date', $end_date );

        maybe_notify_users($post_id);

    }
}

add_action( 'init', 'create_slider_taxonomies', 0 );

function create_slider_taxonomies() {
	$labels = array(
		'name'              => _x( 'Monitor', 'taxonomy general name', 'rosenlundsakeri' ),
		'singular_name'     => _x( 'Monitor', 'taxonomy singular name', 'rosenlundsakeri' ),
		'search_items'      => __( 'Search Monitor', 'rosenlundsakeri' ),
		'all_items'         => __( 'Alla alternativ', 'rosenlundsakeri' ),
		'parent_item'       => __( 'Parent Monitor', 'rosenlundsakeri' ),
		'parent_item_colon' => __( 'Parent Monitor:', 'rosenlundsakeri' ),
		'edit_item'         => __( 'Edit Monitor', 'rosenlundsakeri' ),
		'update_item'       => __( 'Update Monitor', 'rosenlundsakeri' ),
		'add_new_item'      => __( 'Add New Monitor', 'rosenlundsakeri' ),
		'new_item_name'     => __( 'New Monitor Name', 'rosenlundsakeri' ),
		'menu_name'         => __( 'Monitor', 'rosenlundsakeri' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'slider-category' ),
	);

	register_taxonomy( 'slider-category', array( 'slider','news' ), $args );
}