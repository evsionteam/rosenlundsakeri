<?php

function news_init() {
	register_post_type( 'news', array(
		'labels'            => array(
			'name'                => __( 'Nyheter', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Nyheter', 'rosenlundsakeri' ),
			'all_items'           => __( 'All Nyheter', 'rosenlundsakeri' ),
			'new_item'            => __( 'New Nyheter', 'rosenlundsakeri' ),
			'add_new'             => __( 'Lägg till nyhet', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Lägg till nyhet', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Nyheter', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Nyheter', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Nyheter', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Nyheter found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Nyheter found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Nyheter', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Nyheter', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'		=> 26,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor','thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
        'taxonomies'         => array('slider-category'),
		'menu_icon'         => 'dashicons-media-text',
		'show_in_rest'      => true,
		'rest_base'         => 'news',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'news_init' );

function news_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['news'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('News updated. <a target="_blank" href="%s">View News</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'rosenlundsakeri'),
		3 => __('Custom field deleted.', 'rosenlundsakeri'),
		4 => __('News updated.', 'rosenlundsakeri'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('News restored to revision from %s', 'rosenlundsakeri'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('News published. <a href="%s">View News</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		7 => __('News saved.', 'rosenlundsakeri'),
		8 => sprintf( __('News submitted. <a target="_blank" href="%s">Preview News</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('News scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview News</a>', 'rosenlundsakeri'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('News draft updated. <a target="_blank" href="%s">Preview News</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'news_updated_messages' );

add_action( 'add_meta_boxes', 'rosen_news_metadata' );
function rosen_news_metadata(){
	add_meta_box( 'news', __( 'News Settings', 'rosenlundsakeri' ),
		'news_meta_box', 
		'news', 
		'side',
		'high'
	);
}

function news_meta_box( $post ){
    $start_date 	= get_post_meta($post->ID, 'start_date', true);
 	$end_date 		= get_post_meta($post->ID, 'end_date', true);
 	$slide_time 	= get_post_meta($post->ID, 'slide_time', true);
	$slide_time     = empty( $slide_time ) ? 4000 : $slide_time;
    wp_nonce_field( 'news_box_data', 'news_box_nonce' );
?>
    <table class="form-table">
		<tbody>
			<tr>
				<td>
					<p><?php _e('Slide Time (ms)', 'rosenlundsakeri'); ?></p>
					<input type="text" name="slide_time" value="<?php echo $slide_time; ?>" />
				</td>
			</tr>

			<tr>
				<td>
					<p><?php _e('Start Date', 'rosenlundsakeri'); ?></p>
					<input type="date" name="start_date" value="<?php echo $start_date; ?>" />
				</td>
			</tr>

			<tr>
				<td>
				    <p><?php _e('End Date', 'rosenlundsakeri'); ?></p>
					<input type="date" name="end_date" value="<?php echo $end_date; ?>" />
				</td>
			</tr>
		</tbody>
	</table>
    <?php
}
 
add_action( "save_post", "update_news_metadata" );
function update_news_metadata( $post_id ){
    if( wp_verify_nonce( @$_POST['news_box_nonce'], 'news_box_data' ) ){

 		$start_date 				= isset($_POST['start_date'])?$_POST['start_date']:'';
 		$end_date 					= isset($_POST['end_date'])?$_POST['end_date']:'';
 		$slide_time 					= isset($_POST['slide_time'])?$_POST['slide_time']:'4000';

 		if( empty($start_date) && !empty($end_date) ){
			$start_date = date('Y-m-d');			
		}
		
        update_post_meta( $post_id, 'start_date', $start_date );
        update_post_meta( $post_id, 'end_date', $end_date );
        update_post_meta( $post_id, 'slide_time', $slide_time );

        maybe_notify_users($post_id);
    }
}


add_action( 'post_submitbox_misc_actions', 'publish_public' );
add_action( 'save_post', 'save_publish_public' );
function publish_public() {
    global $post;
    if (get_post_type($post) == 'news') {
        echo '<div class="misc-pub-section misc-pub-section-last" style="border-top: 1px solid #eee;">';
        wp_nonce_field( plugin_basename(__FILE__), 'publish_public_nonce' );
        $publish_to_main_site = get_post_meta( $post->ID, '_publish_to_main', true );
        $publish_to_intranet_site = get_post_meta( $post->ID, '_publish_to_intranet', true );
        ?>
        <input type="checkbox" name="publish_to_main" id="publish-to-main" value="1" <?php checked( $publish_to_main_site, '1' ); ?> />
        <?php _e( 'Publish To Main Site', 'rosenlundsakeri' )?><br/>
        <input type="checkbox" name="publish_to_intranet" id="publish-to-intranet" value="1" <?php checked( $publish_to_intranet_site, '1' ); ?> />
        <?php _e( 'Publish To Intranet', 'rosenlundsakeri' )?>
        <?php echo '</div>';
    }
}
function save_publish_public($post_id) {

    if (!isset($_POST['post_type']) )
        return $post_id;

    if ( !wp_verify_nonce( $_POST['publish_public_nonce'], plugin_basename(__FILE__) ) )
        return $post_id;

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
        return $post_id;

    if ( 'post' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id ) )
        return $post_id;


    $insert_data = array(
        'post_title'    => $_POST['post_title'],
        'post_content'  => ev_get_safe_content($_POST['content']),
        'post_status'   => 'publish',
        'post_type'     => 'news',
    );

    $thumbnail = $thumbnail_url = '';
    if( isset($_POST['_thumbnail_id']) && !empty($_POST['_thumbnail_id'])){
        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ),'full' );
        if(!empty($thumbnail)){
            $thumbnail_url = $thumbnail[0];
        }
    }

    if (isset($_POST['publish_to_main'])){

        update_post_meta( $post_id, '_publish_to_main', $_POST['publish_to_main'] );

        $public_mirrored_post = get_post_meta( $post_id, '_public_mirrored_post', true );

        /*only create new post it is not already inserted*/
        if(empty($public_mirrored_post)){

            /*remove action to avoid infinite loops*/
            remove_action( 'save_post', 'save_publish_public' );
            /**/

            /*insert this news post in the main site too*/
            switch_to_blog(1);
            $new_post_id = wp_insert_post($insert_data);

            /*set thumbnail image to newly created post */
            if(!empty($new_post_id)){
                if(!empty($thumbnail_url)){
                    rosen_set_created_post_ft_image($thumbnail_url,$new_post_id);
                }
            }
            /**/

            restore_current_blog();
            /**/

            /*set the new inserted post id as a flag in current post's post meta*/
            if($new_post_id){
                update_post_meta( $post_id, '_public_mirrored_post', $new_post_id );
            }
            /**/

            /*reattach the removed hook*/
            add_action( 'save_post', 'save_publish_public' );
            /**/
        }

    }else{
        update_post_meta( $post_id, '_publish_to_main', 0 );
    }


    if (isset($_POST['publish_to_intranet'])){

        update_post_meta( $post_id, '_publish_to_intranet', $_POST['publish_to_intranet'] );

        $intranet_mirrored_post = get_post_meta( $post_id, '_intranet_mirrored_post', true );

        /*only create new post if it is not already inserted in the intranet site*/
        if(empty($intranet_mirrored_post)){

            /*remove action to avoid infinite loops*/
            remove_action( 'save_post', 'save_publish_public' );
            /**/

            /*insert this news post in the intranet site too*/
            switch_to_blog(3);
            $new_intranet_post_id = wp_insert_post($insert_data);

            /*set thumbnail image to newly created post */
            if(!empty($new_intranet_post_id)){
                if(!empty($thumbnail_url)){
                    rosen_set_created_post_ft_image($thumbnail_url,$new_intranet_post_id);
                }
            }
            /**/

            restore_current_blog();
            /**/

            /*set the newly inserted post id as a flag in current post's post meta*/
            if($new_intranet_post_id){
                update_post_meta( $post_id, '_intranet_mirrored_post', $new_intranet_post_id );
            }
            /**/

            /*reattach the removed hook*/
            add_action( 'save_post', 'save_publish_public' );
            /**/
        }

    }
    else {
        update_post_meta( $post_id, '_publish_to_intranet', 0);
    }

}

/*set the post featured image to newly create post*/
if ( ! function_exists( 'rosen_set_created_post_ft_image' ) ) :
function rosen_set_created_post_ft_image($thumbnail_url,$created_post_id){

    $wp_upload_dir = wp_upload_dir();
    $image_data = file_get_contents($thumbnail_url);
    $filename   = basename($thumbnail_url);
    $filetype = wp_check_filetype( $filename, null );

    if( wp_mkdir_p( $wp_upload_dir['path'] ) ) {
        $file = $wp_upload_dir['path'] . '/' . $filename;
    } else {
        $file = $wp_upload_dir['basedir'] . '/' . $filename;
    }

    file_put_contents( $file, $image_data );

    $attachment = array(
        'post_mime_type' => $filetype['type'],
        'post_title'     => preg_replace( '/\.[^.]+$/', '', $filename ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    $attach_id = wp_insert_attachment( $attachment, $file, $created_post_id );

    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    set_post_thumbnail( $created_post_id, $attach_id );
}
endif;

/* Removes VC shortcode */
if ( ! function_exists( 'ev_get_safe_content' ) ) :
function ev_get_safe_content($content){
    $content = str_replace('vc_column_text', '', $content);
    $content = str_replace('vc_column', '', $content);
    $content = str_replace('vc_row', '', $content);
    $content = str_replace('[]', '', $content);
    $content = str_replace('[/]', '', $content);
    return $content;
}
endif;