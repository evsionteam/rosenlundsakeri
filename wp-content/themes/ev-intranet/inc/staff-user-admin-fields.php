<?php
/*user admin columns */
function staff_user_status( $column ) {
    $column['staff_status'] = 'Staff Status';
    return $column;
}
add_filter( 'manage_users_columns', 'staff_user_status' );

function staff_user_status_fn( $val, $column_name, $user_id ) {

    $user_data = get_userdata($user_id);
    $roles = $user_data->roles;

    $role_arr = array();

    foreach($roles as $role){
        $role_arr[] = $role;
    }

    if(in_array('rosen_staff',$role_arr)){
        switch ($column_name) {
            case 'staff_status' :
                $user_status = get_user_meta( $user_id, 'user_status', true );
                if( 0 == $user_status ){
                    $output = '<div class="status">'.__('Inactive','ev-intranet').'</div>';
                }elseif( 1 == $user_status ){
                    $output = '<div class="status">'.__('Active','ev-intranet').'</div>';
                }else{
                    $output = '<div class="status">'.__('-','ev-intranet').'</div>';
                }
                return $output;
                break;
            default:
        }
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'staff_user_status_fn', 10, 3 );


/*Make the new column sortable*/
function staff_user_status_sortable_fn( $columns ) {
    $columns['staff_status'] = 'staff_status';
    return $columns;
}
add_filter( 'manage_users_sortable_columns', 'staff_user_status_sortable_fn' );

function staff_sort_user_by_status_fn( $query ) {
    if(is_admin()){
        if ( 'staff_status' == $query->get( 'orderby' ) ) {
            $query->set( 'orderby', 'meta_value_num' );
            $query->set( 'meta_key', 'user_status' );
        }
    }
}
add_action( 'pre_get_users', 'staff_sort_user_by_status_fn' );

/*Set Is active status */
add_action( 'show_user_profile', 'staff_user_profile_fields' );
add_action( 'edit_user_profile', 'staff_user_profile_fields' );
function staff_user_profile_fields( $user ) {

    $current_user = wp_get_current_user();
    if(in_array('administrator',$current_user->roles)){
        $is_active =  get_user_meta( $user->ID, 'user_status', true );
        ?>
        <table class="form-table staff-info-table">
            <tr>
                <th><?php _e( 'Staff Status','ev-intranet' ); ?></th>
                <td>
                    <label>
                        <input type="checkbox" name="user_status" value="1" <?php checked( $is_active, 1 ); ?>/>
                        <?php _e( 'Is Active?' ); ?>
                    </label>
                </td>
            </tr>
        </table>
    <?php
    }
}

add_action( 'personal_options_update', 'staff_save_staff_profile_fields' );
add_action( 'edit_user_profile_update', 'staff_save_staff_profile_fields' );
function staff_save_staff_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    $current_user = wp_get_current_user();
    if(in_array('administrator',$current_user->roles)){
        if(isset($_POST['user_status'])){
            update_user_meta( $user_id, 'user_status', 1 );
        }
        else{
            update_user_meta( $user_id, 'user_status', 0 );
        }
    }
}