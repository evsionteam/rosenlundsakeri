<?php

function staff_register_form_fn() {
    if(!is_user_logged_in()) {
        $registration_enabled = get_option('users_can_register');
        if($registration_enabled) {
            ob_start();
            staff_register_fn();
            $output = ob_get_clean();
        } else {
            $output = "<div class='form-error alert alert-danger'>".__('User registration is not enabled.','ev-intranet')."</div>";
        }
        return $output;
    }
}
add_shortcode('staff_register_form', 'staff_register_form_fn');

function staff_register_fn() {

    global $registered;
    $registered = false;
    $first_name = $last_name = $username =  $email = '' ;

    if ( isset($_POST['submit_register']) && wp_verify_nonce($_POST['staff_register_nonce'], 'staff-register-nonce') ) {

        /*Validate submitted form fields*/
        staff_registration_validation(
            $_POST['username'],
            $_POST['password'],
            $_POST['retype_password'],
            $_POST['email']
        );

        /*Sanitize form input fields*/
        $first_name	= 	sanitize_text_field($_POST['first_name']);
        $last_name	= 	sanitize_text_field($_POST['last_name']);
        $username	= 	sanitize_user($_POST['username']);
        $password 	= 	esc_attr($_POST['password']);
        $email 		= 	sanitize_email($_POST['email']);

        /*Register only when no WP_error is found*/
        staff_complete_registration(
            $first_name,
            $last_name,
            $username,
            $password,
            $email
        );
    }

    if( false == $registered){
        /*Display Registration Form*/
        staff_registration_form(
            $first_name,
            $last_name,
            $username,
            $email
        );
    }
}

function staff_registration_form( $first_name = '', $last_name = '', $username = '', $email = '' ) {
    ?>

    <div class="registration-form staff-form">
        <h3 class="staff_header"><?php _e('Registrera ett konto'); ?></h3>
        <form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
            <fieldset>
               <div class="form-group">
                    <label for="first_name" class="col-xs-4 control-label"><?php _e('Förnamn','ev-intranet'); ?></label>
                    <div class="col-xs-8 form-row">
                    <input name="first_name" id="first_name" type="text" value="<?php echo $first_name; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_name" class="col-xs-4 control-label"><?php _e('Efternamn','ev-intranet'); ?></label>
                    <div class="col-xs-8 form-row">
                    <input name="last_name" id="last_name" type="text" value="<?php echo $last_name; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="col-xs-4 control-label"><?php _e('Anställnings ID','ev-intranet'); ?>*</label>
                    <div class="col-xs-8 form-row">
                        <input name="username" id="username" type="text" value="<?php echo $username; ?>" required/> 
                        </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-xs-4 control-label"><?php _e('Email','ev-intranet'); ?>*</label>
                    <div class="col-xs-8 form-row">
                    <input name="email" id="email"  type="email" value="<?php echo $email;?>" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-xs-4 control-label"><?php _e('Lösenord','ev-intranet'); ?>*</label>
                    <div class="col-xs-8 form-row">
                    <input name="password" id="password"  type="password" required/> 
                    </div>
                </div>
                <div class="form-group">
                    <label for="retype-password" class="col-xs-4 control-label"><?php _e('Skriv lösenord igen','ev-intranet'); ?>*</label>
                    <div class="col-xs-8 form-row">
                        <input name="retype_password" id="retype-password"  type="password" required/>
                    </div>
                </div>
               <div class="form-group">
                   <label class="col-xs-4 control-label"></label>
                   <div class="col-xs-8">
                        <input type="hidden" name="staff_register_nonce" value="<?php echo wp_create_nonce('staff-register-nonce'); ?>"/>
                        <input type="submit" name="submit_register" value="<?php _e('Registrera konto','ev-intranet'); ?>"/>
                   </div>
                </div>
                <div class="register-here">
                    <span><?php echo "Har du redan ett konto?</span> <a href='".get_site_url()."/login'>Logga in här</a>";?>
                </div>
            </fieldset>
        </form>
    </div><!-- registration-form -->
<?php
}

function staff_registration_validation( $username, $password, $retype_password , $email ) {
    global $reg_errors;
    $reg_errors = new WP_Error;

    if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
        $reg_errors->add('field', __('Vänligen fyll i alla nödvändiga fält.','ev-intranet'));
    }

    if(!empty($username)){
        if ( username_exists( $username ) ){
            $reg_errors->add('user_name', __('Ledsen, personal id finns redan!','ev-intranet'));
        }

        if ( !validate_username( $username ) ) {
            $reg_errors->add('username_invalid', __('Ledsen, Personal ID är inte tillåtet.','ev-intranet'));
        }
    }

    if(!empty($password)){
        if ( strlen( $password ) < 5 ) {
            $reg_errors->add('password', __('Lösenords längd måste vara mer än 5 tecken.','ev-intranet'));
        }

        if ($password !== $retype_password){
            $reg_errors->add('password_not_matched', __('Lösenorden matchar ej.','ev-intranet'));
        }
    }

    if(!empty($email)){
        if ( !is_email( $email ) ) {
            $reg_errors->add('email_invalid', __('Emailadressen är inte giltig.','ev-intranet'));
        }

        if ( email_exists( $email ) ) {
            $reg_errors->add('email', __('Email finns redan.','ev-intranet'));
        }
    }

    if ( is_wp_error( $reg_errors ) ) {
        echo '<div class="form-validation-errors">';
            foreach ( $reg_errors->get_error_messages() as $error ) {
                echo "<p class='form-error alert alert-danger'>$error</p>";
            }
        echo '</div>';
    }
}

function staff_complete_registration($first_name, $last_name, $username, $password, $email) {
    global $reg_errors, $registered;
    if ( count($reg_errors->get_error_messages()) < 1 ) {
        $user_data = array(
            'user_login'	    => 	$username,
            'user_email' 	    => 	$email,
            'user_pass' 	    => 	$password,
            'first_name' 	    => 	$first_name,
            'last_name' 	    => 	$last_name,
            'user_registered'	=>  date('Y-m-d H:i:s'),
            'role'				=> 'rosen_staff'
        );
        $user_id = wp_insert_user( $user_data );
        if($user_id) {
            /*
             * set user status
             * 0 => inactive
             * 1 => active
            */
            add_user_meta( $user_id, 'user_status', 0);
            /*send an email to the admin alerting them of the registration*/
            wp_new_user_notification($user_id);
            echo '<div class="form-success alert alert-success">Ditt konto har blivit skapat<a href="' . get_site_url() . '/login">Du kan logga in här</a></div>';
            $registered = true;
        }else{
            echo '<div class="form-error alert alert-danger">'.__('Det går tyvärr inte att registrera. Vänligen försök igen.','ev-intranet').'</div>';
        }
    }
}