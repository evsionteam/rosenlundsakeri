<?php
    
    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "ev_intranet";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'EV Intranet', 'ev-intranet' ),
        'page_title'           => __( 'EV Intranet', 'ev-intranet' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    /*$args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'ev-intranet' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'ev-intranet' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'ev-intranet' ),
    );*/

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
  /*  $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );*/

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'ev-intranet' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'ev-intranet' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'ev-intranet' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'ev-intranet' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'ev-intranet' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'ev-intranet' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'ev-intranet' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'ev-intranet' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header Section', 'ev-intranet' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic field with no subsections.', 'ev-intranet' ),
        'icon'   => 'el el-home',
    /*        'fields' => array(
            array(
                'id'       => 'opt-text',
                'type'     => 'text',
                'title'    => __( 'Example Text', 'ev-intranet' ),
                'desc'     => __( 'Example description.', 'ev-intranet' ),
                'subtitle' => __( 'Example subtitle.', 'ev-intranet' ),
                'hint'     => array(
                    'content' => 'This is a <b>hint</b> tool-tip for the text field.<br/><br/>Add any HTML based text you like here.',
                )
            )
        )*/
    ) );
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Fav Icons', 'rosenlundsakeri' ),
        'id'         => 'opt-favicon-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'favicon-16',
                'type'     => 'media',
                'title'    => __( 'Favicon (16x16)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'favicon-32',
                'type'     => 'media',
                'title'    => __( 'Favicon (32x32)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'favicon-96',
                'type'     => 'media',
                'title'    => __( 'Favicon (96x96)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-57',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (57x57)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-60',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (60x60)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-72',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (72x72)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-76',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (76x76)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-114',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (114x114)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-120',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (120x120)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-144',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (144x144)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-152',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (152x152)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-180',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (180x180)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'android-icon-192',
                'type'     => 'media',
                'title'    => __( 'Android Icon (192x192)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'favicon-theme-color',
                'type'     => 'color',
                'title'    => __('Favicon Background Color', 'rosenlundsakeri'),
                'subtitle' => __('Pick a background color for Windows shortcut', 'rosenlundsakeri'),
                'default'  => '#FFFFFF',
            ),
            array(
                'id'       => 'favicon-title-color',
                'type'     => 'color',
                'title'    => __('Favicon Title Color', 'rosenlundsakeri'), 
                'subtitle' => __('Pick a title color for Windows shortcut', 'rosenlundsakeri'),
                'default'  => '#333333',
            )
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Section', 'ev-intranet' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'opt-text-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'logo-image',
                'type'     => 'media',
                'title'    => __( 'Site Logo', 'ev-intranet' ),
                /*'subtitle' => __( 'Subtitle', 'ev-intranet' ),*/
                'desc'     => __( 'Insert Site Logo', 'ev-intranet' ),
            ),
            array(
                'id'       => 'default-image-1',
                'type'     => 'media',
                'title'    => __( 'Default Image for 1 column', 'ev-intranet' ),
                /*'subtitle' => __( 'Subtitle', 'ev-intranet' ),*/
                'desc'     => __( 'Place the default Image(Column 1) which is displayed if particular image is not being set', 'ev-intranet' ),
            ),
           array(
                'id'       => 'default-image-2',
                'type'     => 'media',
                'title'    => __( 'Default Image for 2 column', 'ev-intranet' ),
                /*'subtitle' => __( 'Subtitle', 'ev-intranet' ),*/
                'desc'     => __( 'Place the default Image(Column 2) which is displayed if particular image is not being set', 'ev-intranet' ),
            ),
           array(
                'id'       => 'default-image-3',
                'type'     => 'media',
                'title'    => __( 'Default Image for 3 column', 'ev-intranet' ),
                /*'subtitle' => __( 'Subtitle', 'ev-intranet' ),*/
                'desc'     => __( 'Place the default Image(Column 3) which is displayed if particular image is not being set', 'ev-intranet' ),
            ),
           array(
                'id'       => 'default-image-list',
                'type'     => 'media',
                'title'    => __( 'Default Image for List', 'ev-intranet' ),
                /*'subtitle' => __( 'Subtitle', 'ev-intranet' ),*/
                'desc'     => __( 'Place the default Image(List) which is displayed if particular image is not being set', 'ev-intranet' ),
            ),
           array(
                'id'       => 'loading-image',
                'type'     => 'media',
                'title'    => __( 'Loading Image', 'ev-intranet' ),
            ),
        )
    ) );
    // -> START email settings
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Notify Email Settings', 'ev-intranet' ),
        'id'               => 'notify-email-settings',
        'icon'             => 'el el-edit',
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Notify News', 'ev-intranet' ),
        'id'               => 'notify-news-email',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'news-subject',
                'type'     => 'text',
                'title'    => __( 'Email Subject', 'ev-intranet' ),
            ),
            array(
                'id'       => 'news-banner-image',
                'type'     => 'media',
                'title'    => __( 'Banner Image', 'ev-intranet' ),
            ),
            array(
                'id'       => 'news-banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'ev-intranet' ),
            ),
            array(
                'id'       => 'news-heading',
                'type'     => 'text',
                'title'    => __( 'Heading', 'ev-intranet' ),
                'default'    => '{postTitle}',

            ),
            array(
                'id'       => 'news-content',
                'type'     => 'textarea',
                'title'    => __( 'Content', 'ev-intranet' ),
                'default'    => '{postContent}',
            ),
            array(
                'id'       => 'news-btn-text',
                'type'     => 'text',
                'title'    => __( 'Button Text', 'ev-intranet' ),
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Notify Course', 'ev-intranet' ),
        'id'               => 'notify-course-email',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'course-subject',
                'type'     => 'text',
                'title'    => __( 'Email Subject', 'ev-intranet' ),
            ),
            array(
                'id'       => 'course-banner-image',
                'type'     => 'media',
                'title'    => __( 'Banner Image', 'ev-intranet' ),
            ),
            array(
                'id'       => 'course-banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'ev-intranet' ),
            ),
            array(
                'id'       => 'course-heading',
                'type'     => 'text',
                'title'    => __( 'Heading', 'ev-intranet' ),
                'default'    => '{postTitle}',

            ),
            array(
                'id'       => 'course-content',
                'type'     => 'textarea',
                'title'    => __( 'Content', 'ev-intranet' ),
                'default'    => '{postContent}',
            ),
            array(
                'id'       => 'course-btn-text',
                'type'     => 'text',
                'title'    => __( 'Button Text', 'ev-intranet' ),
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Notify Job', 'ev-intranet' ),
        'id'               => 'notify-job-email',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'job-subject',
                'type'     => 'text',
                'title'    => __( 'Email Subject', 'ev-intranet' ),
            ),
            array(
                'id'       => 'job-banner-image',
                'type'     => 'media',
                'title'    => __( 'Banner Image', 'ev-intranet' ),
            ),
            array(
                'id'       => 'job-banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'ev-intranet' ),
            ),
            array(
                'id'       => 'job-heading',
                'type'     => 'text',
                'title'    => __( 'Heading', 'ev-intranet' ),
                'default'    => '{postTitle}',

            ),
            array(
                'id'       => 'job-content',
                'type'     => 'textarea',
                'title'    => __( 'Content', 'ev-intranet' ),
                'default'    => '{postContent}',
            ),
            array(
                'id'       => 'job-btn-text',
                'type'     => 'text',
                'title'    => __( 'Button Text', 'ev-intranet' ),
            ),
        )
    ) );

    /*
     * <--- END SECTIONS
     */

