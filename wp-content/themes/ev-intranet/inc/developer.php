<?php
add_filter( 'wp_get_attachment_image_src', 'fillup_broken_image_src',10,2);
function fillup_broken_image_src($image, $attachment_id){
	$path = get_attached_file($attachment_id);
	if(!file_exists($path)){
		$w = isset($image[1])?$image[1]:1348;
		$h = isset($image[2])?$image[2]:600;
		$image[0] = 'http://placehold.it/'.$w.'x'.$h;
	}
	return $image;
}
add_filter( 'redux/options/ev_intranet/global_variable', 'fillup_broken_image_option' );
function fillup_broken_image_option($data){
	$site_url = rtrim(site_url(), '/');
	$abspath = rtrim(ABSPATH, '/');
	foreach($data as &$opt){
		if(is_array($opt)){
			if(isset($opt['thumbnail'])){
				$file = str_replace($site_url,$abspath, $opt['url']);
				if(!file_exists($file)){
					$opt['url'] = 'http://placehold.it/'.$opt['width'].'x'.$opt['height'];
					$opt['thumbnail'] = 'http://placehold.it/'.$opt['width'].'x'.$opt['height'];
				}
			}	
		}
	}
	return $data;
}
add_filter('post_thumbnail_html','fillup_broken_image_html',10,5);
function fillup_broken_image_html($html, $post_id, $thumb_id, $size,$attr){
	return $html;
}
function ev_dump( $obj,$type=false ){
	if(!defined('WP_DEBUG') || WP_DEBUG != true ){
		return;
	}
	echo '<pre>';
	if($type){
		var_dump($obj);
	}else{
		print_r($obj);
	}
	echo '<pre>';
}
function ev_log($id, $obj, $append=false ){
	if(!defined('WP_DEBUG') || WP_DEBUG != true ){
		return;
	}
	if($append){
		file_put_contents(__DIR__.'/log-'.$id.'.txt', print_r($obj,true), FILE_APPEND );
	}else{
		file_put_contents(__DIR__.'/log-'.$id.'.txt', print_r($obj,true) );
	}
}          