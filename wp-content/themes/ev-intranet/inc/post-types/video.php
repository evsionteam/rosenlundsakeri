<?php

function video_init() {
	register_post_type( 'video', array(
		'labels'            => array(
			'name'                => __( 'Video', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Video', 'rosenlundsakeri' ),
			'all_items'           => __( 'All Video', 'rosenlundsakeri' ),
			'new_item'            => __( 'New Video', 'rosenlundsakeri' ),
			'add_new'             => __( 'Add New', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Add New Video', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Video', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Video', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Video', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Video found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Video found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Video', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Video', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-format-video',
		'show_in_rest'      => true,
		'rest_base'         => 'video',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'video_init' );

add_action( 'add_meta_boxes', 'youtube_video_text' );
function youtube_video_text()
{
    add_meta_box( 'youtube-video-text', 'Youtube Video URL', 'youtube_video_callback', 'video', 'normal', 'high' );
}

function youtube_video_callback( $post ){
    $youtube_video_link = get_post_meta( $post->ID, 'youtube_video_link', true );

    wp_nonce_field( 'youtube_video_link_nonce', 'my_youtube_video_link_nonce' );
    ?>
    <p>
        <label for="youtube_video_link"><p>Enter Youtube Video URL</p></label>
        <input type="text" name="youtube_video_link" id="youtube_video_link" size="100" value="<?php echo $youtube_video_link; ?>" />
    </p>
    <?php   
}

add_action( 'save_post', 'youtube_video_link' );
function youtube_video_link( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_youtube_video_link_nonce'] ) || !wp_verify_nonce( $_POST['my_youtube_video_link_nonce'], 'youtube_video_link_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['youtube_video_link'] ) )
        update_post_meta( $post_id, 'youtube_video_link', $_POST['youtube_video_link'] );

}




add_action('add_meta_boxes', 'arrangement_post_meta_boxes', 1);
function arrangement_post_meta_boxes() {
    add_meta_box( 'arrangement-post-meta', __( 'Upload Video' ), 'arrangements_settings_display', 'video', 'normal', 'default');
}


function arrangements_settings_display($post) {

    $video_upload = get_post_meta( $post->ID, 'video_upload', true);
    wp_nonce_field( 'video_meta_box_nonce', 'video_meta_box' );
    ?>
    <div id="image_field">
        <label for="video-upload"><?php _e( 'Video Upload: ', 'ev-intranet' )?></label><br/>
        <input type="hidden" id="video-upload" name="video_upload" value="<?php echo $video_upload; ?>" />
        <input id="upload_video" type="button" class="button" value="<?php _e( 'Video Upload', 'ev-intranet' ); ?>" />
        <?php if ( '' != $video_upload): ?>
            <input id="delete_image" type="button" class="button" value="<?php _e( 'Delete Image', 'ev-intranet' ); ?>" />
        <?php endif; ?>
    </div>
    <div id="image_preview">
        <p class="description">
            <?php echo $video_upload; ?>
        </p>
    </div>
<?php
} 

add_action('save_post', 'video_meta_box_save');
function video_meta_box_save($post_id) {

    if ( ! isset( $_POST['video_meta_box'] ) || ! wp_verify_nonce( $_POST['video_meta_box'], 'video_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'video_upload' ] ) ) {
        update_post_meta( $post_id, 'video_upload', $_POST[ 'video_upload' ] );
    }
}
