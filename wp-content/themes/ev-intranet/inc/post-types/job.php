<?php

function job_init() {
	register_post_type( 'job', array(
		'labels'            => array(
			'name'                => __( 'Jobs', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Jobs', 'rosenlundsakeri' ),
			'all_items'           => __( 'All Jobs', 'rosenlundsakeri' ),
			'new_item'            => __( 'New Jobs', 'rosenlundsakeri' ),
			'add_new'             => __( 'Add New', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Add New Jobs', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Jobs', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Jobs', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Jobs', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Jobs found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Jobs found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Jobs', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Jobs', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'job',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

	register_taxonomy( 'job-category', array( 'job' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Categories', 'rosenlundsakeri' ),
			'singular_name'              => _x( 'Category', 'taxonomy general name', 'rosenlundsakeri' ),
			'search_items'               => __( 'Search categories', 'rosenlundsakeri' ),
			'popular_items'              => __( 'Popular categories', 'rosenlundsakeri' ),
			'all_items'                  => __( 'All categories', 'rosenlundsakeri' ),
			'parent_item'                => __( 'Parent category', 'rosenlundsakeri' ),
			'parent_item_colon'          => __( 'Parent category:', 'rosenlundsakeri' ),
			'edit_item'                  => __( 'Edit category', 'rosenlundsakeri' ),
			'update_item'                => __( 'Update category', 'rosenlundsakeri' ),
			'add_new_item'               => __( 'New category', 'rosenlundsakeri' ),
			'new_item_name'              => __( 'New category', 'rosenlundsakeri' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'rosenlundsakeri' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'rosenlundsakeri' ),
			'choose_from_most_used'      => __( 'Choose from the most used categories', 'rosenlundsakeri' ),
			'not_found'                  => __( 'No categories found.', 'rosenlundsakeri' ),
			'menu_name'                  => __( 'Categories', 'rosenlundsakeri' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'category',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'job_init' );

function job_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['job'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Jobs updated. <a target="_blank" href="%s">View Jobs</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'rosenlundsakeri'),
		3 => __('Custom field deleted.', 'rosenlundsakeri'),
		4 => __('Jobs updated.', 'rosenlundsakeri'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Jobs restored to revision from %s', 'rosenlundsakeri'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Jobs published. <a href="%s">View Jobs</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		7 => __('Jobs saved.', 'rosenlundsakeri'),
		8 => sprintf( __('Jobs submitted. <a target="_blank" href="%s">Preview Jobs</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Jobs scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Jobs</a>', 'rosenlundsakeri'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Jobs draft updated. <a target="_blank" href="%s">Preview Jobs</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'job_updated_messages' );


add_action( 'add_meta_boxes', 'intranet_job_contact' );
function intranet_job_contact()
{
    add_meta_box( 'intranet-job-contact', 'Contact', 'intranet_job_contact_callback', 'job', 'normal', 'high' );
}

function intranet_job_contact_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['intranet_job_contact_text'] ) ? $values1['intranet_job_contact_text'][0] : '';

    wp_nonce_field( 'intranet_job_contact_text_nonce', 'my_intranet_job_contact_text_nonce' );
    ?>
    <p>
        <textarea rows="6" cols="70" name="intranet_job_contact_text"  id="intranet_job_contact_text"><?php echo $selected1; ?></textarea>
    </p>
    <?php   
}

add_action( 'save_post', 'intranet_job_contact_save' );
function intranet_job_contact_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_intranet_job_contact_text_nonce'] ) || !wp_verify_nonce( $_POST['my_intranet_job_contact_text_nonce'], 'intranet_job_contact_text_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['intranet_job_contact_text'] ) )
        update_post_meta( $post_id, 'intranet_job_contact_text', $_POST['intranet_job_contact_text'] );

}

add_action( 'post_submitbox_misc_actions', 'job_publish_public' );
add_action( 'save_post', 'job_save_job_publish_public' );
function job_publish_public() {
    global $post;
    if (get_post_type($post) == 'job') {
        echo '<div class="misc-pub-section misc-pub-section-last" style="border-top: 1px solid #eee;">';
        wp_nonce_field( 'job_publish_public_nonce_text', 'job_publish_public_nonce' );
        $val = get_post_meta( $post->ID, '_job_publish_public', true ) ? get_post_meta( $post->ID, '_job_publish_public', true ) : 'article';?>
        <input type="checkbox" name="job_publish_public" id="featured-menu" value="1" <?php if ( isset ( $val ) ) checked( $val, '1' ); ?> />
         <?php _e( 'Publish in Public Site', 'rosenlundsakeri' )?>
        <?php echo '</div>';
    }
}
function job_save_job_publish_public($post_id) {

    if (!isset($_POST['post_type']) )
        return $post_id;

    if ( !wp_verify_nonce( $_POST['job_publish_public_nonce'], 'job_publish_public_nonce_text' ) )
        return $post_id;

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
        return $post_id;

    if ( 'post' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id ) )
        return $post_id;
    
    if (isset($_POST['job_publish_public'])){

        update_post_meta( $post_id, '_job_publish_public', $_POST['job_publish_public'] );

        $public_mirrored_post = get_post_meta( $post_id, '_public_mirrored_post', true );

        /*only create new post if it is not already inserted in the main site*/
        if(empty($public_mirrored_post)){

            /*remove action to avoid infinite loops*/
            remove_action( 'save_post', 'job_save_job_publish_public' );
            /**/

            /*insert this news post in the main site too*/
            $insert_data = array(
                'post_title'    => $_POST['post_title'],
                'post_content'  => $_POST['content'],
                'post_status'   => 'publish',
                'post_type'     => 'job',
            );

            $thumbnail = $thumbnail_url = '';
            if( isset($_POST['_thumbnail_id']) && !empty($_POST['_thumbnail_id'])){
                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ),'full' );
                if(!empty($thumbnail)){
                    $thumbnail_url = $thumbnail[0];
                }
            }

            switch_to_blog(1);
            $new_post_id = wp_insert_post($insert_data);

            /*set thumbnail image to newly created post */
            if(!empty($new_post_id)){
                if(!empty($thumbnail_url)){
                    rosen_set_created_post_ft_image($thumbnail_url,$new_post_id);
                }
            }
            /**/

            restore_current_blog();
            /**/

            /*set the newly inserted post id as a flag in current post's postmeta*/
            if($new_post_id){
                update_post_meta( $post_id, '_public_mirrored_post', $new_post_id );
            }
            /**/

            /*reattach the removed hook*/
            add_action( 'save_post', 'job_save_job_publish_public' );
            /**/
        }

    }else{
        update_post_meta( $post_id, '_job_publish_public', 0 );
    }

}