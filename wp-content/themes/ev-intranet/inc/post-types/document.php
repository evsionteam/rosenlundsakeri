<?php

function document_init() {
	register_post_type( 'document', array(
		'labels'            => array(
			'name'                => __( 'Dokument', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Dokument', 'rosenlundsakeri' ),
			'all_items'           => __( 'All Dokument', 'rosenlundsakeri' ),
			'new_item'            => __( 'New Dokument', 'rosenlundsakeri' ),
			'add_new'             => __( 'Add New', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Add New Dokument', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Dokument', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Dokument', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Dokument', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Dokument found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Dokument found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Dokument', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Dokument', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-text',
		'show_in_rest'      => true,
		'rest_base'         => 'document',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'document_init' );

add_action('add_meta_boxes', 'document_upload_meta_boxes', 1);
function document_upload_meta_boxes() {
    add_meta_box( 'document-post-meta', __( 'Upload Document' ), 'document_settings_display', 'document', 'side', 'default');
}


function document_settings_display($post) {

    $document_upload = get_post_meta( $post->ID, 'document_upload', true);
    wp_nonce_field( 'document_meta_box_nonce', 'document_meta_box' );
    ?>
    <div id="document_field">
        <label for="document-upload"><?php _e( 'Document Upload: ', 'ev-intranet' )?></label><br/>
        <input type="hidden" id="document-upload" name="document_upload" value="<?php echo $document_upload; ?>" />
        <input id="upload_document" type="button" class="button" value="<?php _e( 'Document Upload', 'ev-intranet' ); ?>" />
        <?php if ( '' != $document_upload): ?>
            <input id="delete_document" type="button" class="button" value="<?php _e( 'Delete Document', 'ev-intranet' ); ?>" />
        <?php endif; ?>
    </div>
    <div id="document_preview">
        <p class="description">
            <?php echo $document_upload; ?>
        </p>
    </div>
<?php
} 

add_action('save_post', 'document_meta_box_save');
function document_meta_box_save($post_id) {

    if ( ! isset( $_POST['document_meta_box'] ) || ! wp_verify_nonce( $_POST['document_meta_box'], 'document_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'document_upload' ] ) ) {
        update_post_meta( $post_id, 'document_upload', $_POST[ 'document_upload' ] );
    }
}
