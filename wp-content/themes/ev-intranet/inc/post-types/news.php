<?php

function news_init() {
	register_post_type( 'news', array(
		'labels'            => array(
			'name'                => __( 'News', 'rosenlundsakeri' ),
			'singular_name'       => __( 'News', 'rosenlundsakeri' ),
			'all_items'           => __( 'All News', 'rosenlundsakeri' ),
			'new_item'            => __( 'New News', 'rosenlundsakeri' ),
			'add_new'             => __( 'Add New', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Add New News', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit News', 'rosenlundsakeri' ),
			'view_item'           => __( 'View News', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search News', 'rosenlundsakeri' ),
			'not_found'           => __( 'No News found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No News found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent News', 'rosenlundsakeri' ),
			'menu_name'           => __( 'News', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'excerpt', 'thumbnail','comments' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'news',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

	register_taxonomy( 'news-category', array( 'news' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Categories', 'rosenlundsakeri' ),
			'singular_name'              => _x( 'Category', 'taxonomy general name', 'rosenlundsakeri' ),
			'search_items'               => __( 'Search categories', 'rosenlundsakeri' ),
			'popular_items'              => __( 'Popular categories', 'rosenlundsakeri' ),
			'all_items'                  => __( 'All categories', 'rosenlundsakeri' ),
			'parent_item'                => __( 'Parent category', 'rosenlundsakeri' ),
			'parent_item_colon'          => __( 'Parent category:', 'rosenlundsakeri' ),
			'edit_item'                  => __( 'Edit category', 'rosenlundsakeri' ),
			'update_item'                => __( 'Update category', 'rosenlundsakeri' ),
			'add_new_item'               => __( 'New category', 'rosenlundsakeri' ),
			'new_item_name'              => __( 'New category', 'rosenlundsakeri' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'rosenlundsakeri' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'rosenlundsakeri' ),
			'choose_from_most_used'      => __( 'Choose from the most used categories', 'rosenlundsakeri' ),
			'not_found'                  => __( 'No categories found.', 'rosenlundsakeri' ),
			'menu_name'                  => __( 'Categories', 'rosenlundsakeri' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'category',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );
}
add_action( 'init', 'news_init' );

function news_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['news'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('News updated. <a target="_blank" href="%s">View News</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'rosenlundsakeri'),
		3 => __('Custom field deleted.', 'rosenlundsakeri'),
		4 => __('News updated.', 'rosenlundsakeri'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('News restored to revision from %s', 'rosenlundsakeri'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('News published. <a href="%s">View News</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		7 => __('News saved.', 'rosenlundsakeri'),
		8 => sprintf( __('News submitted. <a target="_blank" href="%s">Preview News</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('News scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview News</a>', 'rosenlundsakeri'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('News draft updated. <a target="_blank" href="%s">Preview News</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'news_updated_messages' );


//Design Select

add_action( 'add_meta_boxes', 'intranet_news_design_select' );

function intranet_news_design_select($post){
    add_meta_box('news-design-select', 'News Design select', 'intranet_news_design_select_meta', 'news', 'normal' , 'high');
}

add_action('save_post', 'intranet_news_design_save_metabox');

function intranet_news_design_save_metabox(){ 
    global $post;
    if(isset($_POST["intranet_news_design_select"])){
        $meta_element_class = $_POST['intranet_news_design_select'];

        update_post_meta($post->ID, 'intranet_news_design_select_meta', $meta_element_class);
    }
}

function intranet_news_design_select_meta($post){
    $meta_element_class = get_post_meta($post->ID, 'intranet_news_design_select_meta', true); //true ensures you get just one value instead of an array
    ?>   
    <label>Choose the Design Type of the news :  </label>

    <select name="intranet_news_design_select" id="intranet_news_design_select">
      <option value="design_1" <?php selected( $meta_element_class, 'design_1' ); ?>>Single Column</option>
      <option value="design_2" <?php selected( $meta_element_class, 'design_2' ); ?>>Two Column</option>
      <option value="design_3" <?php selected( $meta_element_class, 'design_3' ); ?>>Three Column</option>
    </select>
    <?php
}


add_action( 'post_submitbox_misc_actions', 'publish_to_main_site' );
add_action( 'save_post', 'save_publish_to_main_site' );
function publish_to_main_site() {
    global $post;
    if (get_post_type($post) == 'news') {
        echo '<div class="misc-pub-section misc-pub-section-last" style="border-top: 1px solid #eee;">';
        wp_nonce_field( plugin_basename(__FILE__), 'publish_public_nonce' );
        $publish_to_main_site = get_post_meta( $post->ID, '_publish_to_main', true );
        ?>
        <input type="checkbox" name="publish_to_main" id="publish-to-main" value="1" <?php checked( $publish_to_main_site, '1' ); ?> />
        <?php _e( 'Publish To Main Site', 'rosenlundsakeri' )?><br/>
        <?php echo '</div>';
    }
}
function save_publish_to_main_site($post_id) {

    if (!isset($_POST['post_type']) )
        return $post_id;

    if ( !wp_verify_nonce( $_POST['publish_public_nonce'], plugin_basename(__FILE__) ) )
        return $post_id;

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    if ( 'post' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    if (isset($_POST['publish_to_main'])){

        update_post_meta( $post_id, '_publish_to_main', $_POST['publish_to_main'] );

        $public_mirrored_post = get_post_meta( $post_id, '_public_mirrored_post', true );

        /*only create new post if it is not already inserted in the main site*/
        if(empty($public_mirrored_post)){

            /*remove action to avoid infinite loops*/
            remove_action( 'save_post', 'save_publish_to_main_site' );
            /**/

            /*insert this news post in the main site too*/
            $insert_data = array(
                'post_title'    => $_POST['post_title'],
                'post_content'  => $_POST['content'],
                'post_status'   => 'publish',
                'post_type'     => 'news',
            );

            $thumbnail = $thumbnail_url = '';
            if( isset($_POST['_thumbnail_id']) && !empty($_POST['_thumbnail_id'])){
                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ),'full' );
                if(!empty($thumbnail)){
                    $thumbnail_url = $thumbnail[0];
                }
            }

            switch_to_blog(1);
            $new_post_id = wp_insert_post($insert_data);

            /*set thumbnail image to newly created post */
            if(!empty($new_post_id)){
                if(!empty($thumbnail_url)){
                    rosen_set_created_post_ft_image($thumbnail_url,$new_post_id);
                }
            }
            /**/

            restore_current_blog();
            /**/

            /*set the newly inserted post id as a flag in current post's postmeta*/
            if($new_post_id){
                update_post_meta( $post_id, '_public_mirrored_post', $new_post_id );
            }
            /**/

            /*reattach the removed hook*/
            add_action( 'save_post', 'save_publish_to_main_site' );
            /**/
        }

    }else{
        update_post_meta( $post_id, '_publish_to_main', 0 );
    }

}