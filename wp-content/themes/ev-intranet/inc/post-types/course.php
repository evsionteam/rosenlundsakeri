<?php

function course_init() {
	register_post_type( 'course', array(
		'labels'            => array(
			'name'                => __( 'Courses', 'rosenlundsakeri' ),
			'singular_name'       => __( 'Course', 'rosenlundsakeri' ),
			'all_items'           => __( 'All Courses', 'rosenlundsakeri' ),
			'new_item'            => __( 'New Course', 'rosenlundsakeri' ),
			'add_new'             => __( 'Add New', 'rosenlundsakeri' ),
			'add_new_item'        => __( 'Add New Course', 'rosenlundsakeri' ),
			'edit_item'           => __( 'Edit Course', 'rosenlundsakeri' ),
			'view_item'           => __( 'View Course', 'rosenlundsakeri' ),
			'search_items'        => __( 'Search Courses', 'rosenlundsakeri' ),
			'not_found'           => __( 'No Courses found', 'rosenlundsakeri' ),
			'not_found_in_trash'  => __( 'No Courses found in trash', 'rosenlundsakeri' ),
			'parent_item_colon'   => __( 'Parent Course', 'rosenlundsakeri' ),
			'menu_name'           => __( 'Courses', 'rosenlundsakeri' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'excerpt','thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-book-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'course',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

	register_taxonomy( 'course-category', array( 'course' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Categories', 'rosenlundsakeri' ),
			'singular_name'              => _x( 'Category', 'taxonomy general name', 'rosenlundsakeri' ),
			'search_items'               => __( 'Search categories', 'rosenlundsakeri' ),
			'popular_items'              => __( 'Popular categories', 'rosenlundsakeri' ),
			'all_items'                  => __( 'All categories', 'rosenlundsakeri' ),
			'parent_item'                => __( 'Parent category', 'rosenlundsakeri' ),
			'parent_item_colon'          => __( 'Parent category:', 'rosenlundsakeri' ),
			'edit_item'                  => __( 'Edit category', 'rosenlundsakeri' ),
			'update_item'                => __( 'Update category', 'rosenlundsakeri' ),
			'add_new_item'               => __( 'New category', 'rosenlundsakeri' ),
			'new_item_name'              => __( 'New category', 'rosenlundsakeri' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'rosenlundsakeri' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'rosenlundsakeri' ),
			'choose_from_most_used'      => __( 'Choose from the most used categories', 'rosenlundsakeri' ),
			'not_found'                  => __( 'No categories found.', 'rosenlundsakeri' ),
			'menu_name'                  => __( 'Categories', 'rosenlundsakeri' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'category',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'course_init' );

function course_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['course'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Course updated. <a target="_blank" href="%s">View Course</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'rosenlundsakeri'),
		3 => __('Custom field deleted.', 'rosenlundsakeri'),
		4 => __('Course updated.', 'rosenlundsakeri'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Course restored to revision from %s', 'rosenlundsakeri'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Course published. <a href="%s">View Course</a>', 'rosenlundsakeri'), esc_url( $permalink ) ),
		7 => __('Course saved.', 'rosenlundsakeri'),
		8 => sprintf( __('Course submitted. <a target="_blank" href="%s">Preview Course</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Course scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Course</a>', 'rosenlundsakeri'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Course draft updated. <a target="_blank" href="%s">Preview Course</a>', 'rosenlundsakeri'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'course_updated_messages' );



add_action( 'add_meta_boxes', 'intranet_course_contact' );
function intranet_course_contact()
{
    add_meta_box( 'intranet-course-contact', 'Contact', 'intranet_course_contact_callback', 'course', 'normal', 'high' );
}

function intranet_course_contact_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['intranet_course_contact_text'] ) ? $values1['intranet_course_contact_text'][0] : '';

    wp_nonce_field( 'intranet_course_contact_text_nonce', 'my_intranet_course_contact_text_nonce' );
    ?>
    <p>
        <textarea rows="6" cols="70" name="intranet_course_contact_text"  id="intranet_course_contact_text"><?php echo $selected1; ?></textarea>
    </p>
    <?php   
}

add_action( 'save_post', 'intranet_course_contact_save' );
function intranet_course_contact_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_intranet_course_contact_text_nonce'] ) || !wp_verify_nonce( $_POST['my_intranet_course_contact_text_nonce'], 'intranet_course_contact_text_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['intranet_course_contact_text'] ) )
        update_post_meta( $post_id, 'intranet_course_contact_text', $_POST['intranet_course_contact_text'] );

}



add_action( 'add_meta_boxes', 'featured_post' );
function featured_post()
{
	$types = array( 'course', 'job', 'news' );
	foreach( $types as $type ) {
    add_meta_box( 'intranet-2-course-contact', 'Featured Post', 'featured_post_callback', $type, 'normal', 'high' );
	}
}

function featured_post_callback( $post )
{
    $values2 = get_post_custom( $post->ID );
    $selected2 = isset( $values2['featured_post_text'] ) ? $values2['featured_post_text'][0] : '';

    wp_nonce_field( 'featured_post_text_nonce', 'my_featured_post_text_nonce' );
    ?>
    <p>
        <input type="checkbox" name="featured_post_text" id="featured-menu" value="1" <?php if ( isset ( $selected2 ) ) checked( $selected2, '1' ); ?> />
            <?php _e( 'Is featured Post?', 'ev-intranet' )?>
    </p>
    <?php  
}

add_action( 'save_post', 'featured_post_save' );
function featured_post_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_featured_post_text_nonce'] ) || !wp_verify_nonce( $_POST['my_featured_post_text_nonce'], 'featured_post_text_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['featured_post_text'] ) ){
        update_post_meta( $post_id, 'featured_post_text', $_POST['featured_post_text'] );
        update_option( 'home_featured_post', $post_id );
    } else {
        update_post_meta( $post_id, 'featured_post_text', '' );
        //add_option( 'home_featured_post', '' );
    }

}