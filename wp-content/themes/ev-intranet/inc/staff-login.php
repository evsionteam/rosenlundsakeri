<?php

function staff_login_form_fn() {
    if(!is_user_logged_in()) {
        ob_start();
        staff_login_fn();
        $output = ob_get_clean();
    } else {
        $output = "<div class='form-error alert alert-danger'>".__('Du är redan inloggad.','ev-intranet')."</div>";
    }
    return $output;
}
add_shortcode('staff_login_form', 'staff_login_form_fn');

function staff_login_fn() {

    $username = '';

    if ( isset($_POST['submit_login']) && wp_verify_nonce($_POST['staff_login_nonce'], 'staff-login-nonce') ) {

        /*Validate submitted form fields*/
        staff_login_validation(
            $_POST['username'],
            $_POST['password']
        );

        /*Sanitize form input fields*/
        $username	= 	sanitize_user($_POST['username']);
        $password 	= 	esc_attr($_POST['password']);

        /*Login only when no WP_error is found*/
        staff_complete_login(
            $username,
            $password
        );
    }

    /*Display Login Form*/
    staff_login_form(
        $username
    );
}

function staff_login_form( $username = '' ) {
    ?>

    <div class="login-form staff-form">
        <h3 class="staff_header"><?php _e('Logga in'); ?></h3>
        <form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
            <fieldset>
                <div class="form-group">
                    <label for="username"><?php _e('Anställnings ID / Email ','ev-intranet'); ?>*</label>
                    <input name="username" id="username" type="text" value="<?php echo $username; ?>" required/>
                </div>
                <div class="form-group">
                    <label for="password"><?php _e('Lösenord','ev-intranet'); ?>*</label>
                    <input name="password" id="password"  type="password" required/>
                </div>
                <div class="form-group">
                    <input type="hidden" name="staff_login_nonce" value="<?php echo wp_create_nonce('staff-login-nonce'); ?>"/>
                    <input type="submit" name="submit_login" value="<?php _e('Logga in','ev-intranet'); ?>"/>
                    <div class="register-here">
                        <span><?php echo "Har du inte ett konto? </span> <a href='".get_site_url()."/register'>Registrera ditt konto här</a>";?>
                    </div>
                </div>
            </fieldset>
        </form>
    </div><!-- login-form -->

<?php
}

function staff_login_validation( $username, $password ) {
    global $reg_errors;
    $reg_errors = new WP_Error;
    $login_user = $login_pass = false;

    if ( empty( $username ) || empty( $password ) ) {
        $reg_errors->add('field', __('Vänligen fyll i alla nödvändiga fält.','ev-intranet'));
    }

    /*Check user info from the user name*/
    if(!empty($username)){
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $user = get_user_by('email', $username);
        } else {
            $user = get_user_by('login', $username);
        }
        if(!$user) {
            $reg_errors->add('empty_username', __('Ogiltigt användarnamn eller email.','ev-intranet'));
        }else{
            $login_user = true;
        }
    }

    /*Check the user's login with their password*/
    if( !empty($password) && !empty($user) ){
        if(!wp_check_password($password, $user->user_pass, $user->ID)) {
            /*if the password is incorrect for the specified user*/
            $reg_errors->add('incorrect_password', __('Ogiltigt lösenord.','ev-intranet'));
        }else{
            $login_pass = true;
        }
    }

    /*check for user status*/
    if(true == $login_user && true == $login_pass){
        $user_status = get_user_meta($user->ID,'user_status',true);
        if(1 != $user_status){
            $reg_errors->add('account_not_approved', __('Ledsen men ditt konto har inte blivit godkänt ännu.','ev-intranet'));
        }
    }

    if ( is_wp_error( $reg_errors ) ) {
        echo '<div class="form-validation-errors">';
            foreach ( $reg_errors->get_error_messages() as $error ) {
                echo "<p class='form-error alert alert-danger'>$error</p>";
            }
        echo '</div>';
    }
}

function staff_complete_login($username,$password) {
    global $reg_errors;
    if ( count($reg_errors->get_error_messages()) < 1 ) {
        $creds = array('user_login' => $username, 'user_password' => $password);
        $user = wp_signon( $creds );
        if ( is_wp_error($user) ){
            echo $user->get_error_message();
        }else{
            wp_redirect( site_url() );
            exit;
        }
    }
}