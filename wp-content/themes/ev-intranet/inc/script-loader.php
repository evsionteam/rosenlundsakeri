<?php

add_action( 'wp_enqueue_scripts', 'ev_intranet_scripts' );
function ev_intranet_scripts() {
	$suffix = '.min';
	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}
	
	$assets_url = get_stylesheet_directory_uri().'/assets/build/';

	if( is_home() || is_front_page() ){
        wp_enqueue_style( 'rosenlundsakeri', $assets_url . 'css/homepage-above-fold' . $suffix . '.css', array() );
    } else {
        wp_enqueue_style( 'rosenlundsakeri', $assets_url . 'css/inner-above-fold' . $suffix . '.css', array() );
    }

	wp_enqueue_style( 'ev-intranet-style', $assets_url.'css/main'.$suffix.'.css');

	$main_css = $assets_url.'css/main'.$suffix.'.css';
    $script = "
       var head  = document.getElementsByTagName('head')[0];
       var link  = document.createElement('link');
       link.rel  = 'stylesheet';
       link.type = 'text/css';
       link.href = '".$main_css."';
       link.media = 'all';
       head.appendChild(link);
    ";


	wp_enqueue_script( 'ev-intranet', $assets_url.'js/script'.$suffix.'.js', array('jquery'), null, true );
    wp_enqueue_script('masonry');
	wp_localize_script( 'ev-intranet', 'evAjax' ,array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );

	$webfont = "WebFontConfig = {
					google: { families: [ 'Open+Sans:400,400italic,600,600italic,300,300italic,700,700italic,800:latin' ] }
				};
				(function() {
					var wf = document.createElement('script');
					wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
					wf.type = 'text/javascript';
					wf.async = 'true';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(wf, s);
				})();
				(function(d) {
				    var config = {
				      kitId: 'bct1apf',
				      scriptTimeout: 3000,
				      async: true
				    },
				    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,'')+' wf-inactive';},config.scriptTimeout),tk=d.createElement('script'),f=false,s=d.getElementsByTagName('script')[0],a;h.className+=' wf-loading';tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!='complete'&&a!='loaded')return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
				  })(document);";
	
	wp_add_inline_script('ev-intranet', $webfont );
	wp_add_inline_script('ev-intranet', $script );

	$theme_stylesheet_handle = 'ev-intranet-style';
	$custom_script_handle    = 'ev-intranet';

	if(!is_front_page()){
		wp_add_inline_style( $theme_stylesheet_handle,'body{
			opacity: 0
		}');
		
		$preloader = 'function preloader( param ){

			this.time = param.time;

			this.init = function(){

				var time = this.time;

				jQuery(window).load( function(){
					jQuery( "body" ).animate({
						opacity : 1
					},time);
				})
			}
		} jQuery(document).ready(function(){ new preloader({ time: 1000 }).init(); });';

		wp_add_inline_script( $custom_script_handle,$preloader );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}



add_action( 'admin_enqueue_scripts', 'ev_intranet__admin_scripts' );
function ev_intranet__admin_scripts() {
	$assets_url = get_stylesheet_directory_uri().'/assets/build/';
	$suffix = '.min';
	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}
	wp_enqueue_media();
        wp_register_script( 'ev-intranet-admin', $assets_url.'js/admin-script'.$suffix.'.js', array( 'jquery' ),null,true );
        wp_localize_script( 'ev-intranet-admin', 'meta_image',
            array(
                'title' => __( 'Choose or Upload an Video', 'ev-intranet' ),
                'button' => __( 'Use this Video', 'ev-intranet' ),
                'document_title' => __( 'Choose or Upload an Document', 'ev-intranet' ),
                'document_button' => __( 'Use this Document', 'ev-intranet' ),
            )
        );
	wp_enqueue_script( 'ev-intranet-admin');
}
