<?php
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head', 'feed_links', 2 );
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
add_filter( 'show_recent_comments_widget_style', '__return_false' );

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_filter( 'nav_menu_css_class', 'remove_unwanted_menu_classes' );
add_filter( 'nav_menu_item_id', 'remove_unwanted_menu_classes' );
add_filter( 'page_css_class', 'remove_unwanted_menu_classes' );
add_filter( 'body_class', 'remove_unwanted_body_class', 10, 2 );

function remove_unwanted_menu_classes( $classes ) {
    $allowed =  array (
        'current-menu-item',
        'current-menu-ancestor',
        'menu-item-has-children',
        'current-post-ancestor',
        'first',
        'last',
        'vertical',
        'horizontal',
        'book-btn',
    );

    return $classes;
}

function remove_unwanted_body_class( $wp_classes, $extra_classes ) {
    global $post;
    $whitelist = array( 'portfolio', 'home', 'error404','woocommerce' );
    $wp_classes = array_intersect( $wp_classes, $whitelist );


    if ( isset( $post ) ) {
        $wp_classes[] = $post->post_name;
    }

    if ( is_front_page() ) {
        $wp_classes[] = 'home';
    }

    return array_merge( $wp_classes, (array) $extra_classes );
}


function ev_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) ){
        $src = remove_query_arg( 'ver', $src );
    }
    return $src;
}

add_filter( 'style_loader_src', 'ev_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'ev_remove_wp_ver_css_js', 9999 );


add_filter('the_content','wpautop');
add_filter('get_the_content','wpautop');

show_admin_bar( false );