<?php
function load_more_posts(){

    $output['more_post'] = false;
    $output['data'] = '';
    $offset = $_POST['offset'];

    $args = array(
        'posts_per_page' => 10,
        'offset' => $offset,
    );

    $loading_from = $_POST['loading_from'];
    if(!empty($loading_from)){
        if('home' == $loading_from){
            $args['post_type'] = array( 'news', 'job', 'course' );
        }
        if('news' == $loading_from){
            $args['post_type'] = array( 'news' );
        }
    }

    if(isset($_POST['search']) && !empty($_POST['search'])){
        $args['s'] =  $_POST['search'];
    }

    $query = new WP_Query($args);
    if ($query->have_posts()):
        $output['more_post'] = true;
        while ($query->have_posts()):$query->the_post();
            ob_start();
            item_load_posts();
            $output['data'][] = ob_get_clean();
        endwhile;wp_reset_query();
    else:
        $output['more_post'] = false;
        $output['data'] = __('Inga fler inlägg','ev-intranet');
    endif;

    echo json_encode($output);
    exit;
}
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');
add_action('wp_ajax_load_more_posts', 'load_more_posts');