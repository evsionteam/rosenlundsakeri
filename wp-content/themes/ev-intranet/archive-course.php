<?php

get_header(); 
?>

	<div id="primary" class="content-area main-content">
		<main id="main" class="site-main" role="main">
			<div class="above-container">
				<div class="row">
					<?php require get_template_directory() . '/inc/breadcrumbs.php'; ?>
				</div>
				<div class="c-row">
					<div class="category-section">
							<?php
							$counter = 1;
							while ( have_posts() ) : the_post();?>
							<?php
                                if (strpos($_SERVER['REQUEST_URI'], "list") !== false){
                                ?>
                                    <div class="full-width-content">
                                        <div class="category-list">
                                            <div class="category-content">
                                                <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
                                                <p><?php the_excerpt(); ?></p>
                                                <div class="contact-info">
                                                    <strong><?php _e('Kontakt: ','ev-intranet')?></strong>
                                                    <?php
                                                    $value = get_post_meta( $post->ID ,'intranet_course_contact_text', true );
                                                    if( strlen($value) > 54 ){
                                                        $value = substr( $value, 0, 54 ) . ' ...';
                                                    }
                                                    echo $value;
                                                    ?>
                                                </div>
                                            </div><!-- category-content -->
                                        </div>
                                    </div>
                                <?php
							    } else {
                                    $image_id = get_post_thumbnail_id( get_the_ID() );
                                    $image = wp_get_attachment_image_src( $image_id, 'news-col-3');
							        ?>
                                    <div class="col-sm-4 c-padding">
                                        <div class="category-list">
                                            <div class = "cat-image"><a href="<?php the_permalink();?>"><img src = "<?php echo $image[0];	?>"></a></div>
                                            <div class="category-content">
                                                <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
                                                <p><?php the_excerpt(); ?></p>
                                                <div class="contact-info">
                                                    <strong><?php _e('Kontakt: ','ev-intranet')?></strong>
                                                    <?php
                                                        $value = get_post_meta( $post->ID ,'intranet_course_contact_text', true );
                                                        if( strlen($value) > 54 ){
                                                            $value = substr( $value, 0, 54 ) . ' ...';
                                                        }
                                                        echo $value;
                                                    ?>
                                                </div>
                                            </div><!-- category-content -->
                                        </div><!-- category-list" -->
                                    </div><!-- col-sm-4 -->
							    <?php
                                }
								if ($counter%3 == 0){ ?>
									<div class="clearfix hidden-xs"></div>
								<?php }
								$counter ++;
						         endwhile; // End of the loop.
							?>		
                    </div><!-- category-section  -->
                </div><!-- c-row -->
			</div><!-- container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();