<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EVision_Intranet
 */

?>

	</div><!-- #content -->


	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class = "footer-widget">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('footer-1'); ?>
					<?php dynamic_sidebar('footer-2'); ?>
					<?php dynamic_sidebar('footer-3'); ?>
				</div><!-- row -->
			</div>
		</div><!-- footer-widget -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
