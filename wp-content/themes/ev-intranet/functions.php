<?php
if ( ! function_exists( 'ev_intranet_setup' ) ) :

function ev_intranet_setup() {

	load_theme_textdomain( 'ev-intranet', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'ev-intranet' ),
		'footer-1' => esc_html__( 'Footer 1', 'ev-intranet' ),
		'footer-2' => esc_html__( 'Footer 2', 'ev-intranet' ),
		'footer-3' => esc_html__( 'Footer 3', 'ev-intranet' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	add_image_size( 'news-col-1', 1348, 600, true );
	add_image_size( 'news-col-3', 420, 229, true );
    add_image_size( 'news-col-2', 875, 229, true );
}
endif;
add_action( 'after_setup_theme', 'ev_intranet_setup' );


function ev_intranet_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ev_intranet_content_width', 640 );
}
add_action( 'after_setup_theme', 'ev_intranet_content_width', 0 );


function ev_intranet_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ev-intranet' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'ev-intranet' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<div class="col-sm-4"><section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section></div>',
		'before_title'  => '<h3 class="footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'ev-intranet' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<div class="col-sm-4"><section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section></div>',
		'before_title'  => '<h3 class="footer-title">',
    'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'ev-intranet' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<div class="col-sm-4"><section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section></div>',
		'before_title'  => '<h3 class="footer-title">',
    'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'ev_intranet_widgets_init' );

require get_template_directory() . '/inc/developer.php';
require get_template_directory() . '/inc/helpers.php';
require get_template_directory() . '/inc/script-loader.php';
require get_template_directory() . '/inc/admin/admin.php';
require get_template_directory() . '/inc/maid.php';
require get_template_directory() . '/inc/load-more-posts.php';
require get_template_directory() . '/inc/favicon.php';
require get_template_directory() . '/inc/staff-register.php';
require get_template_directory() . '/inc/staff-login.php';
require get_template_directory() . '/inc/staff-user-admin-fields.php';

function archive_search_pgp($query) {

	if(is_admin() || !$query->is_main_query()){ return $query; }
		$query_var = get_query_var('search');
     if ( is_post_type_archive( 'course' ) && !empty($query_var)) {
	     	$query->set('post_type', array( 'course') );
	        $query->set( 's', $query_var );
    }
     elseif ( is_post_type_archive( 'job' ) && !empty($query_var) ) {
     	$query->set('post_type', array( 'job') );
        $query->set( 's', $query_var );
    }
    elseif ( is_post_type_archive( 'news' ) ) {
        if(!empty($query_var)){
            $query->set('post_type', array( 'news') );
            $query->set( 's', $query_var );
        }
    }
    if ( $query->is_archive() && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', 10 );
    }
    return $query;
}
add_action('pre_get_posts', 'archive_search_pgp');

/*load posts*/
function item_load_posts(){
       $url = '';
       global $ev_intranet;
       if(isset($_POST['url']) && !empty($_POST['url'])){
           $url = $_POST['url'];
       }
       if ( strpos($_SERVER['REQUEST_URI'], "list") !== false || strpos($url, "list") !== false ){
           echo '<div class = "news-list-item clearfix">';
           if(has_post_thumbnail()){
               $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'news-col-3');
           }else{
               $image = wp_get_attachment_image_src( $ev_intranet['default-image-list']['id'], 'news-col-3');
           }
           echo '<div class="news-list-image"><a href="'.get_the_permalink().'"><img src ="'.$image[0].'"></a></div>';
           ?>
           <div class="news-list-content">
               <div class="news-title">
                   <a href = "<?php the_permalink();?>"><?php the_title(); ?><span class="news-date">
                            <?php echo get_the_date('dmy');?>
                      </span>
                   </a>
               </div>
               <div class="restro-sub-title">
                   <a href="<?php the_permalink();?>">
                        <?php echo wp_trim_words( get_the_excerpt(), 8, '...' ); ?>
                   </a>
               </div>
           </div>
           <?php
           echo '</div>';/*news list item*/
       } else {
           $design_select = get_post_meta( get_the_ID() ,'intranet_news_design_select_meta', true );
           switch ($design_select) {
               case "design_1":
                   echo '<div class = "grid-item col-md-12 c-padding full-width-image">';
                   $image_size = 'news-col-1';
                   $wrapper_class = 'full-image-container';
                   break;
               case "design_2":
                   echo '<div class = "grid-item col-md-8 col-sm-6 c-padding news-lists">';
                   $image_size = 'news-col-2';
                   $wrapper_class = 'image-thumbnail-wrap';
                   break;
               case "design_3":
                   echo '<div class = "grid-item col-md-4 col-sm-6 c-padding news-lists">';
                   $image_size = 'news-col-3';
                   $wrapper_class = 'image-thumbnail-wrap';
                   break;
               default:
                   echo '<div class = "grid-item col-md-4 col-sm-6 c-padding news-lists">';
                   $image_size = 'news-col-3';
                   $wrapper_class = 'image-thumbnail-wrap';
           }
           echo '<a href="'.get_the_permalink().'" class="image-link">';
               if(has_post_thumbnail()){
                   $image = wp_get_attachment_image_src( get_post_thumbnail_id(), $image_size);
               }else{
                   $image = wp_get_attachment_image_src( $ev_intranet['default-image-list']['id'], $image_size);
               }
               echo '<div class="'.$wrapper_class.'" style="background-image:url('.$image[0].')">';
               ?>
               <div class="image-content">
                   <div class="news-title">
                       <span><?php the_title();?></span>
                       <span class="news-date"><?php echo get_the_date('dmy');?></span>
                   </div>
                   <div class="restro-sub-title"><?php echo wp_trim_words( get_the_excerpt(), 8, '...' ); ?></div>
               </div>
               <?php
               echo '</div>';/*background image close*/
           echo '</a>';
           echo '</div>';/*grid item close*/
       }
}
/**/

/* Load More Posts from network */
function item_load_posts_network($post_id,$blog_id){
    $url = '';
    if(isset($_POST['url']) && !empty($_POST['url'])){
        $url = $_POST['url'];
    }
    if ( strpos($_SERVER['REQUEST_URI'], "list") !== false || strpos($url, "list") !== false ){
        echo '<div class = "news-list-item clearfix">';
        $image = rosen_get_network_post_featured_image( $post_id, $blog_id, 'news-col-3');
        echo '<div class="news-list-image"><a href="'.network_get_permalink().'"><img src ="'.$image[0].'"></a></div>';
        ?>
        <div class="news-list-content">
            <div class="news-title">
                <a href="<?php echo network_get_permalink();?>"><?php network_the_title(); ?>
                    <span class="news-date">
                            <?php echo get_the_date('dmy');?>
                      </span>
                </a>
            </div>
            <div class="restro-sub-title">
                <a href="<?php echo network_get_permalink();?>">
                    <?php echo wp_trim_words( network_get_the_excerpt(), 8, '...' ); ?>
                </a>
            </div>
        </div>
        <?php
        echo '</div>';/*news list item*/
    } else {
        $design_select = rosen_get_network_post_meta($post_id, $blog_id,'intranet_news_design_select_meta');
        switch ($design_select) {
            case "design_1":
                echo '<div class = "grid-item col-md-12 c-padding full-width-image">';
                $image_size = 'news-col-1';
                $wrapper_class = 'full-image-container';
                break;
            case "design_2":
                echo '<div class = "grid-item col-md-8 col-sm-6 c-padding news-lists">';
                $image_size = 'news-col-2';
                $wrapper_class = 'image-thumbnail-wrap';
                break;
            case "design_3":
                echo '<div class = "grid-item col-md-4 col-sm-6 c-padding news-lists">';
                $image_size = 'news-col-3';
                $wrapper_class = 'image-thumbnail-wrap';
                break;
            default:
                echo '<div class = "grid-item col-md-4 col-sm-6 c-padding news-lists">';
                $image_size = 'news-col-3';
                $wrapper_class = 'image-thumbnail-wrap';
        }
        echo '<a href="'.network_get_permalink().'" class="image-link">';
        $image = rosen_get_network_post_featured_image( $post_id, $blog_id, $image_size);
        echo '<div class="'.$wrapper_class.'" style="background-image:url('.$image[0].')">';
        ?>
        <div class="image-content">
            <div class="news-title">
                <span><?php network_the_title();?></span>
                <span class="news-date"><?php echo get_the_date('dmy');?></span>
            </div>
            <div class="restro-sub-title"><?php echo wp_trim_words( ev_get_safe_content(network_get_the_excerpt()), 8, '...' ); ?></div>
        </div>
        <?php
        echo '</div>';/*background image close*/
        echo '</a>';
        echo '</div>';/*grid item close*/
    }
}

function rosen_get_network_post_meta($post_id, $post_blog_id, $key = ''){
    switch_to_blog( $post_blog_id );
    if(empty($key)){
        $post_meta = get_post_meta($post_id);
    }else{
        $post_meta = get_post_meta($post_id,$key,true);
    }
    restore_current_blog();
    return $post_meta;
}

function rosen_get_network_post_featured_image($post_id, $post_blog_id, $size = 'thumbnail'){
    $image = '';
    switch_to_blog( $post_blog_id );
    $image_id = get_post_thumbnail_id( $post_id );
    if($image_id){
        $image = wp_get_attachment_image_src( $image_id, $size);
    }else{
        switch ($size) {
            case "news-col-1":
                $opt = array(
                    'width' => 1348,
                    'height' => 600,
                );
                break;
            case "news-col-2":
                $opt = array(
                    'width' => 875,
                    'height' => 229,
                );
                break;
            case "news-col-3":
                $opt = array(
                    'width' => 420,
                    'height' => 229,
                );
                break;
            case "thumbnail":
                $opt = array(
                    'width' => 150,
                    'height' => 150,
                );
                break;
            default:
                $opt = array(
                    'width' => 420,
                    'height' => 229,
                );
        }
        $image[0] = 'http://placehold.it/'.$opt['width'].'x'.$opt['height'];
    }
    restore_current_blog();
    return $image;
}

add_filter('authenticate', 'staff_check_login', 100, 3);
function staff_check_login($user, $username, $password) {
    if(!empty($user)){
        if (!empty($username)) {
            if(property_exists($user,'roles')){
                if( in_array('rosen_staff', $user->roles)){
                    $user_status = get_user_meta($user->ID,'user_status',true);
                    if( 1 == $user_status ){
                        return $user;
                    }else{
                        return null;
                    }
                }
            }
        }
    }
    return $user;
}

/*set the post featured image to newly create post*/
function rosen_set_created_post_ft_image($thumbnail_url,$created_post_id){

    $wp_upload_dir = wp_upload_dir();
    $image_data = file_get_contents($thumbnail_url);
    $filename   = basename($thumbnail_url);
    $filetype = wp_check_filetype( $filename, null );

    if( wp_mkdir_p( $wp_upload_dir['path'] ) ) {
        $file = $wp_upload_dir['path'] . '/' . $filename;
    } else {
        $file = $wp_upload_dir['basedir'] . '/' . $filename;
    }

    file_put_contents( $file, $image_data );

    $attachment = array(
        'post_mime_type' => $filetype['type'],
        'post_title'     => preg_replace( '/\.[^.]+$/', '', $filename ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    $attach_id = wp_insert_attachment( $attachment, $file, $created_post_id );

    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    set_post_thumbnail( $created_post_id, $attach_id );
}
/**/

/*Notify staffs upon saving*/
add_action( 'post_submitbox_misc_actions', 'rosen_intranet_notify_staff_fn');
function rosen_intranet_notify_staff_fn() {
    global $post;
    if ( in_array(get_post_type($post), array('news','course','job')) ) {
        echo '<div class="misc-pub-section misc-pub-notify" style="border-top: 1px solid #eee;">';
        wp_nonce_field( basename( __FILE__ ), 'notify_staff_nonce' );
        $notify_staff = get_post_meta( $post->ID, 'notify_staff', true );
        ?>
        <input type="checkbox" name="notify_staff" id="notify-staff" value="1" <?php checked( $notify_staff, '1' ); ?> />
        <label for="notify-staff"><?php _e( 'Notify Staffs','ev-intranet' )?></label><br/>
        <?php echo '</div>';
    }
}
add_action( 'save_post', 'intranet_notify_staff' );
function intranet_notify_staff( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'notify_staff_nonce' ] ) && wp_verify_nonce( $_POST[ 'notify_staff_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if( isset( $_POST[ 'notify_staff' ] ) ) {

        update_post_meta( $post_id, 'notify_staff', $_POST[ 'notify_staff' ] );

        /*send mail to all the active staffs*/
        $staffs_mail = array();
        $staffs = get_users(array(
            'role' => 'rosen_staff'
        ));
        if(!empty($staffs)){
            foreach($staffs as $staff){
                $staff_status = get_user_meta($staff->ID,'user_status',true);
                if( 1 == $staff_status ){
                    $staffs_mail[] = $staff->data->user_email;
                }
            }
        }

        if(!empty($staffs_mail)){

            global $ev_intranet,$post_type;

            $subject = ( isset($ev_intranet["$post_type-subject"]) && !empty($ev_intranet["$post_type-subject"]) ) ? $ev_intranet["$post_type-subject"] : 'New Post on the site';

            $banner_image = ( isset($ev_intranet["$post_type-banner-image"]) && !empty($ev_intranet["$post_type-banner-image"]) ) ? $ev_intranet["$post_type-banner-image"]["url"] : '';
            $banner_title = ( isset($ev_intranet["$post_type-banner-title"]) && !empty($ev_intranet["$post_type-banner-title"]) ) ? $ev_intranet["$post_type-banner-title"] : get_the_title();
            $heading = ( isset($ev_intranet["$post_type-heading"]) && !empty($ev_intranet["$post_type-heading"]) ) ? $ev_intranet["$post_type-heading"] : '';
            $content = ( isset($ev_intranet["$post_type-content"]) && !empty($ev_intranet["$post_type-content"]) ) ? $ev_intranet["$post_type-content"] : '';
            $btn_text = ( isset($ev_intranet["$post_type-btn-text"]) && !empty($ev_intranet["$post_type-btn-text"]) ) ? $ev_intranet["$post_type-btn-text"] : 'Read More';

            $heading = str_replace( '{postTitle}', get_the_title(), $heading );
            $content = str_replace( '{postContent}', wp_trim_words( $_POST['content'], 40, '...' ), $content );

            $body = file_get_contents( __DIR__.'/mail-template/notify.htm');

            $body = str_replace( '{BannerImage}', $banner_image, $body );
            $body = str_replace( '{BannerTitle}', $banner_title, $body );
            $body = str_replace( '{postTitle}', $heading, $body );
            $body = str_replace( '{postContent}', $content, $body );
            $body = str_replace( '{ButtonText}', $btn_text, $body );
            $body = str_replace( '{ButtonUrl}', get_the_permalink(get_the_ID()), $body );

            $headers = array('Content-Type: text/html; charset=UTF-8');

            foreach($staffs_mail as $to){
               //wp_mail( $to, $subject, $body, $headers );
            }
        }
        /**/
    } else {
        update_post_meta( $post_id, 'notify_staff', 0 );
    }
}

//check if role exist before removing it
/*if( get_role('rosen_staff') ){
    remove_role( 'rosen_staff' );
}*/

/*add new role*/
/*$result = add_role(
    'rosen_staff',
    __( 'Staff' ),
    array(
        'read'         => true,
        'edit_posts'   => false,
        'delete_posts' => false,
        'NextGEN Gallery overview' => true,
        'NextGEN Use TinyMCE' => true,
        'NextGEN Upload images' => true,
        'NextGEN Manage gallery' => true,
        'NextGEN Manage tags' => true,
        'NextGEN Manage others gallery' => true,
        'NextGEN Edit album' => true,
        'NextGEN Change style' => true,
        'NextGEN Change options' => true,
        'NextGEN Attach Interface' => true,
    )
);
if ( null !== $result ) {
    echo 'Yay! New role created!';
}
else {
    echo 'Oh... the basic_contributor role already exists.';
}
die;*/