<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package EVision_Intranet
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="inner-wrap">
			<div class="above-container">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'image' );

				endwhile; // End of the loop.
				?>		
			</div>
			</div><!-- inner-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
