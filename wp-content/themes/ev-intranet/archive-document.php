<?php get_header();?>



	<div id="primary" class="content-area main-content">
		<main id="main" class="site-main" role="main">
			<div class="document-link">
				<div class="above-container">
					<?php require get_template_directory() . '/inc/breadcrumbs.php'; ?>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead> 
								<tr>
									<th><?php _e('Titel','ev-intranet')?></th>
									<th><?php _e('Beskrivning','ev-intranet')?></th>
									<th><?php _e('Ladda ner PDF','ev-intranet')?></th>
								</tr>
							 </thead>

							 <tbody>
							 	<?php
							 	while ( have_posts() ) : the_post();?>
							 	<tr>
							 		<td><?php the_title();?></td>
							 		<td><?php the_content();?></td>
							 		<td align="center">
								 		<a href = "<?php echo get_post_meta( get_the_ID(), 'document_upload', true); ?>">
								 			<i class="fa fa-download"></i>
								 		</a>
							 		</td>

							 	</tr>
							 	<?php endwhile; // End of the loop.
							 	?>
							 </tbody>	
						</table>
					</div><!-- table-responsive -->
				</div><!-- container -->
			</div><!-- document-link -->
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer();?>