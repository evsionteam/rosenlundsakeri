<?php

get_header(); ?>

	<div id="primary" class="content-area main-content">
		<main id="main" class="site-main" role="main">
			<div class="category-section">
				<div class="above-container">
					<?php require get_template_directory() . '/inc/breadcrumbs.php'; ?>
					<div class="row">
						<?php
						while ( have_posts() ) : the_post();?>
						<?php if (strpos($_SERVER['REQUEST_URI'], "list") !== false){ 
								echo '<div class = "full-width-content">';
							 } else { 
							 	$image_id = get_post_thumbnail_id( get_the_ID() );
							 	$image = wp_get_attachment_image_src( $image_id, 'news-col-3');
							echo '<div class="col-sm-4 c-padding">'; } ?>
							<div class="category-list">
								<div class="cat-image"><a href="<?php the_permalink();?>"><img src = "<?php echo $image[0];	?>"></a></div>

								<div class="category-content">
									<h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
									<p><?php the_excerpt(); ?></p>
									<div class="contact-info">
										<strong><?php _e('Kontakt: ','ev-intranet')?></strong>
										<?php $value = get_post_meta( $post->ID ,'intranet_job_contact_text', true ); 
										echo substr( $value, 0, (strlen($value) < 35)? strlen($value) : 35 );
										?>
									</div>
								</div><!-- category-content -->
							</div><!-- category-list -->
						</div><!-- col-sm-4 -->

						<?php endwhile; // End of the loop.
						?>		
						
					</div><!-- above-row -->
				</div><!-- above-container -->
			</div><!-- category-section -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();