<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package EVision_Intranet
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<div class="single-content-wrap">
			<?php $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );  
			$image = wp_get_attachment_image_src( $post_thumbnail_id, 'full'); ?>


		<div class="single-full-image" style="background-image:url(<?php echo $image[0];?>)"></div>

			<div class="single-text-content">
			    <!--Title-->
			    <h2><?php the_title(); ?>, <span class="post-date"><?php echo get_the_date('d m y');?></span></h2>

			    <!--Date-->
			    

			    <!--Content-->
			   <div class="text-content">
			   		<?php the_content(); ?>
			   </div>

			   <div class="contact-to">
					<strong>Kontakt:</strong>		
					<span><?php echo get_post_meta( $post->ID ,'intranet_course_contact_text', true );?></span>
					<span><?php echo get_post_meta( $post->ID ,'intranet_job_contact_text', true );?></span>
				</div>
				
			</div><!-- single-text-content -->
			
		</div><!-- single-content-wrap -->


</article><!-- #post-## -->