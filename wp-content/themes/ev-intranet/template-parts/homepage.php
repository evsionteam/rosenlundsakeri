<?php 
/*Template Name: Homepage Template*/
get_header(); 
global $ev_intranet; ?>

<div id="primary" class="content-area main-content">
    <main id="main" class="site-main" role="main">
        <div class = "above-container">
            
            <div class="row">
             <?php require get_template_directory() . '/inc/breadcrumbs.php'; ?>
            </div>
            
            <div class="c-row">
            <?php
            if ( strpos($_SERVER['REQUEST_URI'], "list") !== false){
                $wrapper_class = 'news-grid-wrapper new-class';
            }else{
                $wrapper_class = 'news-grid-wrapper';
            }
            ?>
            <div class="<?php echo $wrapper_class;?>">
                <?php
                $opt = get_option('home_featured_post');
                $featured_id = get_post_thumbnail_id($opt);
                $image = wp_get_attachment_image_src( $featured_id, 'news-col-1');

                if( !empty($opt) && !isset($_GET['q']) ):
                    if (strpos($_SERVER['REQUEST_URI'], "list") !== false){
                        echo '<div class = "news-list-item clearfix">';
                        if($image){
                            $image_list = wp_get_attachment_image_src( $featured_id, 'news-col-3');
                        } else {
                            $image_list[0] = $ev_intranet['default-image-list']['url'];
                        }
                        echo '<div class="news-list-image"><a href="'. get_the_permalink($opt).'"><img src ="'.$image_list[0].'"></a></div>';
                        ?>
                        <div class="news-list-content">
                            <div class="news-title">
                                <a href="<?php echo get_the_permalink($opt);?>"><?php echo get_the_title($opt);?></a>
                                <span class="news-date">
                                    <a href="<?php echo get_the_permalink($opt);?>">
                                        <?php echo get_the_date('dmy',$opt);?>
                                    </a>
                                </span>
                            </div>
                            <div class="restro-sub-title">
                                <a href="<?php echo get_the_permalink($opt);?>">
                                    <?php echo wp_trim_words( get_the_excerpt($opt), 9, '...' ); ?>
                                </a>
                            </div>
                        </div>
                    <?php
                    echo '</div>';
                    }else{ ?>
                    <div class = "grid-item clearfix col-md-12 c-padding full-width-image">
                        <a href="<?php echo get_the_permalink($opt);?>" class="image-link">
                        <div class="full-image-container" style="background-image:url(<?php echo $image[0];?>)">
                            <div class="image-content">
                                <div class="news-title">
                                    <span><?php echo get_the_title($opt); ?></span>
                                    <span class="news-date"><?php echo get_the_date('dmy',$opt);?></span>
                                </div>
                                <div class="restro-sub-title"><?php echo wp_trim_words( get_the_excerpt($opt), 8, '...' ); ?></div>
                            </div>
                        </div>
                        </a>
                    </div>
                    <?php }
                endif;
                ?>
                <?php
                $ppp = 10;
                $args = array(
                    'post_type' => array( 'news', 'job', 'course' ),
                    'post__not_in' => array($opt),
                    'posts_per_page' => $ppp
                );

                if( isset($_GET['q']) ){
                   $args['s'] =  $_GET['q'];
                }
                $query = new WP_Query($args);
                $found_posts = $query->found_posts;
                $load_text = ($found_posts > $ppp) ? 'Load More' : 'Inga fler inlägg';
                $load_class = ($found_posts > $ppp) ? 'posts-exists' : 'no-posts';

                while($query->have_posts()):$query->the_post();
                    item_load_posts();
                 endwhile;wp_reset_query();
                ?>
            </div>
            </div>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->
<div id="load-more-posts" class="home-page-load-more">
    <div class="container">
        <div id="loading-text" class="<?php echo $load_class;?>"><span class="loader-text"><?php echo $load_text;?></span>
            <span class="rosen-loader" style="display: none">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </span> <!-- rosen-loader -->
        </div>
    </div><!-- container -->
</div>
<?php
get_footer();