<?php
/*Template Name: Login Template*/

get_header();

while(have_posts()):the_post();
    ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="inner-wrap">
                <div class = "container">
                    <div class="row">
                        <?php the_content();?>
                    </div>
                </div><!-- .container -->
            </div><!-- inner-wrap -->
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
endwhile; wp_reset_postdata();
get_footer();
?>
