<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package EVision_Intranet
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="single-content-wrap">

			<div class="single-text-content">
			    <!--Title-->
			    <h2><?php the_title(); ?></h2>

			    <!--Date-->
			   <span class="post-date"><?php echo get_the_date('d m y');?></span> 

			    <!--Content-->
			   <div class="text-content">
			   		<?php the_content(); ?>
			   </div> 

				<div class="contact-to">
					<strong>Contact:</strong>		
					<span><?php echo get_post_meta( $post->ID ,'intranet_course_contact_text', true );?></span>
					<span><?php echo get_post_meta( $post->ID ,'intranet_job_contact_text', true );?></span>
				</div>
				
			</div><!-- single-text-content -->
				
		</div><!-- single-content-wrap -->
	</div><!-- container -->
</article><!-- #post-## -->
