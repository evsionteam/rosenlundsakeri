jQuery(window).load(function($){

    var page = 1;
    var offset_multiplier;
    var no_more_posts = false;
    var loading_from;

    /*Get search values*/
    var search = getParameterByName('q');

    var $grid = jQuery('body .news-grid-wrapper').imagesLoaded( function() {
        $grid.masonry({
            columnWidth: 1,
            itemSelector: '.grid-item'
        });
    });
    /**/

    /* load posts via ajax*/
   function load_posts_on_scroll(){

        var processing = false;
        var element = jQuery('#load-more-posts');
        if(element.length) {

            var posts_exist = jQuery('.posts-exists');
            if(posts_exist.length){

                var element_position = element.offset().top;
                var screen_height = jQuery(window).height();
                var activation_offset = 1;//determines how far up the the page the element needs to be before triggering the function
                var activation_point = element_position - (screen_height * activation_offset);
                var max_scroll_height = jQuery('body').height() - screen_height - 50;//-50 for a little bit of buffer

                var y_scroll_pos = window.pageYOffset;
                var element_in_view = y_scroll_pos > activation_point;
                var has_reached_bottom_of_page = max_scroll_height <= y_scroll_pos && !element_in_view;

                if(element_in_view || has_reached_bottom_of_page) {
                    if(false == processing){
                        if(false == no_more_posts){
                            jQuery(document).unbind('scroll');
                            processing = true;
                            offset_multiplier = 10;
                            var _that = jQuery(this);

                            if(jQuery('.home-page-load-more').length){
                                loading_from = 'home';
                            }

                            if(jQuery('.news-page-load-more').length){
                                loading_from = 'news';
                            }

                            jQuery.ajax({
                                type : 'post',
                                url : evAjax.ajaxurl,
                                data : {
                                    action : 'load_more_posts',
                                    loading_from : loading_from,
                                    offset: (page * offset_multiplier),
                                    url: window.location.href,
                                    search: search
                                },
                                dataType:'json',
                                beforeSend: function() {
                                    jQuery('.rosen-loader').css('display','inline-block');
                                },
                                success : function( response ) {
                                    page++;
                                    if(true == response.more_post){

                                        var $content = jQuery( response.data.join(''));
                                        jQuery('body .news-grid-wrapper').imagesLoaded( function() {
                                            jQuery('body .news-grid-wrapper').append( $content ).masonry( 'appended', $content );
                                        });
                                    }
                                    else{
                                        jQuery('.loader-text').html( response.data );
                                        no_more_posts = true;
                                    }
                                    processing = false;
                                    jQuery(document).bind('scroll',load_posts_on_scroll);
                                    jQuery('.rosen-loader').hide();
                                }
                            });
                        }
                    }
                }
            }

        }
    }

    jQuery(document).on('scroll', load_posts_on_scroll);

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});