	 jQuery(document).ready(function($) {

    //For image upload
    // Instantiates the variable that holds the media library frame.
    var ev_intranet_image_frame;
    var ev_intranet_document_frame;

    // Runs when the image button is clicked.
    $('#upload_video').on('click',function(e){

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it.
        if ( ev_intranet_image_frame ) {
            ev_intranet_image_frame.open();
            return;
        }

        // Sets up the media library frame
        ev_intranet_image_frame = wp.media.frames.ev_intranet_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button },
            library: { type: 'image' }
        });

        // Runs when an image is selected.
        ev_intranet_image_frame.on('select', function(){

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = ev_intranet_image_frame.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $('#video-upload').val(media_attachment.url);
            $('#image_preview p').text(media_attachment.url);
        });

        // Opens the media library frame.
        ev_intranet_image_frame.open();
    });
    
    $('#delete_image').on('click',function(){
        $('#video-upload').attr('value','');
        $('#image_preview p').empty();
    });




        $('#upload_document').on('click',function(e){

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it.
        if ( ev_intranet_document_frame ) {
            ev_intranet_document_frame.open();
            return;
        }

        // Sets up the media library frame
        ev_intranet_document_frame = wp.media.frames.ev_intranet_document_frame = wp.media({
            title: meta_image.document_title,
            button: { text:  meta_image.document_button },
            library: { type: 'image' }
        });

        // Runs when an image is selected.
        ev_intranet_document_frame.on('select', function(){

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = ev_intranet_document_frame.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $('#document-upload').val(media_attachment.url);
            $('#document_preview p').text(media_attachment.url);
        });

        // Opens the media library frame.
        ev_intranet_document_frame.open();
    });
    
    $('#delete_document').on('click',function(){
        $('#document-upload').attr('value','');
        $('#document_preview p').empty();
    });
});