module.exports = function( grunt ){
	
    grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),

		makepot: {
			target: {
				options: {
					mainFile:"./style.css",
					type: "wp-theme",
					domainPath:"./languages/"
				}
			}
		},

		sass: {                              
		    build: {                            
		    	options: {                       
		      		style: "compact"
		      	},
		      	files: { 
		      	//destiation:source                        
		        	"./assets/src/css/homepage-above-fold.css": "./assets/src/sass/homepage-above-fold.scss", 
		        	"./assets/src/css/inner-above-fold.css": "./assets/src/sass/inner-above-fold.scss",
		        	"./assets/src/css/main.css": "./assets/src/sass/main.scss"
		    	}
			}
		},

		concat: {
			options:{
				separator:"\n \n /*** New File ***/ "
			},

			homeAboveFold :{
				src: [
					'./assets/src/css/vendor/above-bootstrap.css',
					'./assets/src/css/homepage-above-fold.css'
				],

				dest: "./assets/build/css/homepage-above-fold.css"
			},

			innerAboveFold :{
				src: [
					'./assets/src/css/vendor/above-bootstrap.css',
					'./assets/src/css/inner-above-fold.css'
				],

				dest: "./assets/build/css/inner-above-fold.css"
			},

			css:{
				src: [
				'./node_modules/font-awesome/css/font-awesome.css',
				'./node_modules/jquery.mmenu/dist/css/jquery.mmenu.all.css',
				"./assets/src/css/vendor/below-bootstrap.css",
				"./assets/src/css/main.css"
				],

				dest: "./assets/build/css/main.css"
			},

			js: { 
				src: [ 
				"./assets/src/js/wrapper/start.js",
				"./assets/src/js/skip-link-focus-fix.js",
				"./node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js",
				'./node_modules/jquery.mmenu/dist/js/jquery.mmenu.all.min.js',
				"./assets/src/js/main.js",
				"./assets/src/js/wrapper/end.js",
				"./assets/src/js/imagesloaded.pkgd.min.js",
				"./assets/src/js/load-more.js"

				],

				dest: "./assets/build/js/script.js"
			},
			adminjs: { 
				src: [ 
				"./assets/src/js/meta-video.js",
				],

				dest: "./assets/build/js/admin-script.js"
			}
		},

		
		uglify: {
			options: {
				report: "gzip"
			},
			main: {
				src: ["./assets/build/js/script.js"],
				dest: "./assets/build/js/script.min.js"
			},
			adminmain: {
				src: ["./assets/build/js/admin-script.js"],
				dest: "./assets/build/js/admin-script.min.js"
			}
		},

		copy: {
			main: {
		      	files: [
		      		{ expand: true, cwd: "./assets/src/img", src: "**", dest: "./assets/build/img/", filter: "isFile"},
		      		{ expand: true, flatten: true,  cwd: './node_modules/font-awesome/fonts', src: '**', dest: './assets/build/fonts/', filter: 'isFile'}      		
		      	]
		  	}
		},
		
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1,
				keepSpecialComments : 0,
				sourceMap: false
			},
			target: {
				files: [{
					expand: true,
					cwd: "./assets/build/css/",
					src: ["*.css", "!*.min.css"],
					dest: "./assets/build/css/",
					ext: ".min.css"
				}]
			}
		},

		watch: {
			js : {
				files : ["./assets/src/js/*.js", "./assets/src/js/**/*.js"],
				tasks : [ "js" ]
			},
			css : {
				files : [ 
							"./assets/src/sass/*.scss",
							"./assets/src/sass/**/**/**/*.scss",
							"./assets/src/sass/**/**/*.scss",
							"./assets/src/sass/**/*.scss" 
						],
				tasks : [ "css"]
			},
			php : {
				files : [ "./*.php", "./**/*.php", "./**/**/*.php" ],
				tasks : [ "php"]
			}
		}

	});

	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-sass");
	grunt.loadNpmTasks("grunt-wp-i18n");

   
   	grunt.registerTask( "css", [ "sass", "concat:homeAboveFold","concat:innerAboveFold", "concat:css"] );
   	grunt.registerTask( "js", [ "concat:js", "uglify" ] );
   	grunt.registerTask( "php", [ "makepot" ] );
  	grunt.registerTask("default", [ "copy", "makepot", "sass", "concat", "cssmin", "uglify"] );
}