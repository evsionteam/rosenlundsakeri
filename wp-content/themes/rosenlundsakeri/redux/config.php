<?php
    
    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "rosel_opt";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Roselundsakeri', 'rosenlundsakeri' ),
        'page_title'           => __( 'Roselundsakeri', 'rosenlundsakeri' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'rosenlundsakeri' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'rosenlundsakeri' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'rosenlundsakeri' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'rosenlundsakeri' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'rosenlundsakeri' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'rosenlundsakeri' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'rosenlundsakeri' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'rosenlundsakeri' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'rosenlundsakeri' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'rosenlundsakeri' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'rosenlundsakeri' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header Section', 'rosenlundsakeri' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic field with no subsections.', 'rosenlundsakeri' ),
        'icon'   => 'el el-home',
/*        'fields' => array(
            array(
                'id'       => 'opt-text',
                'type'     => 'text',
                'title'    => __( 'Example Text', 'rosenlundsakeri' ),
                'desc'     => __( 'Example description.', 'rosenlundsakeri' ),
                'subtitle' => __( 'Example subtitle.', 'rosenlundsakeri' ),
                'hint'     => array(
                    'content' => 'This is a <b>hint</b> tool-tip for the text field.<br/><br/>Add any HTML based text you like here.',
                )
            )
        )*/
    ) );
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Fav Icons', 'rosenlundsakeri' ),
        'id'         => 'opt-favicon-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'favicon-16',
                'type'     => 'media',
                'title'    => __( 'Favicon (16x16)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'favicon-32',
                'type'     => 'media',
                'title'    => __( 'Favicon (32x32)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'favicon-96',
                'type'     => 'media',
                'title'    => __( 'Favicon (96x96)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-57',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (57x57)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-60',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (60x60)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-72',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (72x72)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-76',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (76x76)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-114',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (114x114)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-120',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (120x120)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-144',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (144x144)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-152',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (152x152)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'apple-icon-180',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (180x180)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'android-icon-192',
                'type'     => 'media',
                'title'    => __( 'Android Icon (192x192)', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'favicon-theme-color',
                'type'     => 'color',
                'title'    => __('Favicon Background Color', 'rosenlundsakeri'),
                'subtitle' => __('Pick a background color for Windows shortcut', 'rosenlundsakeri'),
                'default'  => '#FFFFFF',
            ),
            array(
                'id'       => 'favicon-title-color',
                'type'     => 'color',
                'title'    => __('Favicon Title Color', 'rosenlundsakeri'), 
                'subtitle' => __('Pick a title color for Windows shortcut', 'rosenlundsakeri'),
                'default'  => '#333333',
            )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banner Section', 'rosenlundsakeri' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'opt-text-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'logo-image',
                'type'     => 'media',
                'title'    => __( 'Site Logo', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Insert Site Logo', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Enter Title Text for Banner Image', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'banner-image',
                'type'     => 'media',
                'title'    => __( 'Banner Image', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Insert Banner Image', 'rosenlundsakeri' ),
            ),  
            array(
                'id'       => 'banner-button-text',
                'type'     => 'text',
                'title'    => __( 'Banner Button Text', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Enter Buttonb Text for Banner Image', 'rosenlundsakeri' ),
            ), 
            array(
                'id'       => 'banner-button-link',
                'type'     => 'text',
                'title'    => __( 'Banner Button Link', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Enter Button Link for Banner Image', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'default-image',
                'type'     => 'media',
                'title'    => __( 'Default Image', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Insert Default Image that is displayed if no image is selected', 'rosenlundsakeri' ),
            ),   
            array(
                'id'       => 'default-footer-image',
                'type'     => 'media',
                'title'    => __( 'Default Footer Image', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                'desc'     => __( 'Insert Default Footer Image that is displayed if no image is selected', 'rosenlundsakeri' ),
            ),  
        )
    ) );

    /*
     * <--- END SECTIONS
     */

// -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Home Page Section', 'rosenlundsakeri' ),
        'id'     => 'homepage',
        'desc'   => __( 'Basic field with no subsections.', 'rosenlundsakeri' ),
        'icon'   => 'el el-home',
/*        'fields' => array(
            array(
                'id'       => 'opt-text',
                'type'     => 'text',
                'title'    => __( 'Example Text', 'rosenlundsakeri' ),
                'desc'     => __( 'Example description.', 'rosenlundsakeri' ),
                'subtitle' => __( 'Example subtitle.', 'rosenlundsakeri' ),
                'hint'     => array(
                    'content' => 'This is a <b>hint</b> tool-tip for the text field.<br/><br/>Add any HTML based text you like here.',
                )
            )
        )*/
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Services', 'rosenlundsakeri' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'homepge-services',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'services-image',
                'type'     => 'media',
                'title'    => __( 'Services Image', 'rosenlundsakeri' )
            ),
            array(
                'id'       => 'services-title',
                'type'     => 'text',
                'title'    => __( 'Services Title', 'rosenlundsakeri' )
            ),
            array(
                'id'       => 'services-link-text',
                'type'     => 'text',
                'title'    => __( 'Services Link Text', 'rosenlundsakeri' )
            ),  
            array(
                'id'       => 'services-link',
                'type'     => 'text',
                'title'    => __( 'Services Link', 'rosenlundsakeri' ),
            ), 
            array(
                'id'       => 'row-desc-home',
                'type'     => 'textarea',
                'title'    => __( 'Homepage Row Description', 'rosenlundsakeri' ),
                'args'    => array(
                    'wpautop'       => true,
                )
            ),
            array(
                'id'       => 'row-desc',
                'type'     => 'textarea',
                'title'    => __( 'Homepage Row Description 2 column', 'rosenlundsakeri' ),
                'args'    => array(
                    'wpautop'       => true,
                )
            ), 
        )
    ) );

    /*
     * <--- END SECTIONS
     */


 // -> START Basic Fields2
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Contact Section', 'rosenlundsakeri' ),
        'id'     => 'menu',
        'desc'   => __( 'Basic field with no subsections.', 'rosenlundsakeri' ),
        'icon'   => 'el el-home',
/*        'fields' => array(
            array(
                'id'       => 'opt-text',
                'type'     => 'text',
                'title'    => __( 'Example Text', 'rosenlundsakeri' ),
                'desc'     => __( 'Example description.', 'rosenlundsakeri' ),
                'subtitle' => __( 'Example subtitle.', 'rosenlundsakeri' ),
                'hint'     => array(
                    'content' => 'This is a <b>hint</b> tool-tip for the text field.<br/><br/>Add any HTML based text you like here.',
                )
            )
        )*/
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contact Detail', 'rosenlundsakeri' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'opt-text-subsection2',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'contact-fakturaadres',
                'type'     => 'textarea',
                'title'    => __( 'Fakturaadres', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-besoksadress',
                'type'     => 'textarea',
                'title'    => __( 'Besoksadress', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-email',
                'type'     => 'text',
                'title'    => __( 'Email', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-email-desc',
                'type'     => 'text',
                'title'    => __( 'Email Desc', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-tel',
                'type'     => 'text',
                'title'    => __( 'Telephone', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-fax-admin',
                'type'     => 'text',
                'title'    => __( 'Fax Administration', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-fax-trafik',
                'type'     => 'text',
                'title'    => __( 'Fax Trafik', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-org-nr',
                'type'     => 'text',
                'title'    => __( 'Org nr', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-momserg',
                'type'     => 'text',
                'title'    => __( 'Momsreg nr/VAT-nr', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-swift',
                'type'     => 'text',
                'title'    => __( 'Swift/bic', 'rosenlundsakeri' ),
            ),
            array(
                'id'       => 'contact-iban',
                'type'     => 'text',
                'title'    => __( 'IBAN', 'rosenlundsakeri' ),
            ),
        )
    ) );

        
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contact Form', 'rosenlundsakeri' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'opt-text-subsection4',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'cf-shortcode',
                'type'     => 'textarea',
                'title'    => __( 'Contact Form Shortcode', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                /*'desc'     => __( 'Enter Title Text for Banner Image', 'rosenlundsakeri' ),*/
            ),
            array(
                'id'       => 'cf-desc',
                'type'     => 'textarea',
                'title'    => __( 'Contact Form Desc', 'rosenlundsakeri' ),
                /*'subtitle' => __( 'Subtitle', 'rosenlundsakeri' ),*/
                /*'desc'     => __( 'Enter Title Text for Banner Image', 'rosenlundsakeri' ),*/
            ),
        )
    ) );
       
    
        

    /*
     * <--- END SECTIONS
     */

