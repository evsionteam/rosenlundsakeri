<?php
get_header();
   global $rosel_opt;
  ?>
  <div id="primary" class="content-area">
      <main id="main" class="site-main" role="main">

          <?php get_template_part('template-parts/home', 'banner'); ?>

          <!-- :::::::::::::::::::::::::::::::::::::::::::
                      CASE-DESCRIPTION
          ::::::::::::::::::::::::::::::::::::::::::: -->
          <section class="description-block" id="rl-intro">
              <div class="container">
                  <div class="description-wrap">
                      <?php echo wpautop($rosel_opt['row-desc-home']); ?>
                  </div><!-- description-wrap -->
              </div>
          </section><!-- case-description -->

          <!-- ::::::::::::::::::::::::::::::::::::::
                      MAP SECTION
          ::::::::::::::::::::::::::::::::::::::::::: -->
          <section class="services-sec" style="background-image: url(<?php echo $rosel_opt['services-image']['url']; ?>);">
              <div class="container">
                  <div class="service-text">
                    <h2><?php echo $rosel_opt['services-title']; ?></h2>
                    <a class="rl-btn-blue" href="<?php echo $rosel_opt['services-link']; ?>"> <?php echo $rosel_opt['services-link-text']; ?></a>
                  </div><!-- service-text -->
              </div>
          </section><!-- services -->
          <?php get_template_part('template-parts/contact', 'address'); ?><!-- contact-address -->
      </main><!-- #main -->
  </div><!-- #primary -->
  <?php get_footer(); ?>

  </body>

</html>
