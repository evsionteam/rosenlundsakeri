<?php
/*Template Name: Contact Template*/
?>
<?php get_header('inner');
 global $rosel_opt;
 ?>

<div class="contact-page">
	<div class="kontakt-detail">
		<div class="container">
			<div class="col-sm-7">
				<div class="kontakt-info">
					<address>
						<p class="info-title"><?php _e('Fakturaadress:','rosenlundsakeri');?></p>
						<?php echo $rosel_opt['contact-fakturaadres']; ?>
					</address>
					<address>
						<p class="info-title"><?php _e('Besöksadress:','rosenlundsakeri');?></p>
						<?php echo $rosel_opt['contact-besoksadress']; ?>
					</address>
					<address>
						<p><?php echo $rosel_opt['contact-email']; ?> <?php echo $rosel_opt['contact-email-desc']; ?></p>
						<p><?php _e('Tel:','rosenlundsakeri');?><a href="<?php echo $rosel_opt['contact-fax-trafik']; ?>"><?php echo $rosel_opt['contact-tel']; ?></a></p>
						<p><?php _e('Fax administration:','rosenlundsakeri');?> <?php echo $rosel_opt['contact-fax-admin']; ?></p>
						<p><?php _e('Fax Trafik:','rosenlundsakeri');?><?php echo $rosel_opt['contact-fax-trafik']; ?></p>
					</address>
					<address>
						<p><?php _e('Org nr:','rosenlundsakeri');?> <?php echo $rosel_opt['contact-org-nr']; ?></p>
					</address>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="kontakt-form cont-detail">
					<address>
						<p><?php _e('Momsreg nr/VAT-nr:','rosenlundsakeri');?><?php echo $rosel_opt['contact-momserg']; ?></p>
						<p><?php _e('Swift/bic:','rosenlundsakeri');?><?php echo $rosel_opt['contact-swift']; ?></p>
						<p><?php _e('IBAN:','rosenlundsakeri');?><?php echo $rosel_opt['contact-iban']; ?></p>
					</address>

					<?php echo do_shortcode('[contact-form-7 id="272" title="Kontact Page"]');?>
				</div>
			</div>

		</div>
	</div><!-- kontakt-detail -->
</div><!-- rl-inner-page -->

<?php get_footer(); ?>
