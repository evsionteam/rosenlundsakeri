<?php global $rosel_opt; ?>

<div class="home-banner" style="background-image:url(<?php echo $rosel_opt['banner-image']['url'];?>)">
    <div class="above-container">
        <div class="banner-content">
            <div class="center-text">
                <h1>
                    <?php if (isset($rosel_opt['banner-title'])) {echo $rosel_opt['banner-title'];} ?>
                </h1>
                <p>Välkommen till Rosenlunds Åkeri AB, leverantör av punktliga transporter sedan starten 1964.</p>
                <a class="boka-link" href="<?php
                    if (isset($rosel_opt['banner-button-link'])) {echo $rosel_opt['banner-button-link'];}?>"><?php
                    if (isset($rosel_opt['banner-button-text'])) {echo $rosel_opt['banner-button-text'];}?>
                </a>
            </div>
        </div>
    </div>
   <!--  <div class="line"></div> -->
   <a href="" class="down-arrow rl-scroll-down"  data-target="#rl-intro" data-reduce-offset="#masthead" data-animation-duration="500">
      <img src=" <?php echo get_template_directory_uri(); ?>/assets/dist/img/down-arrow.png" alt="down-arrow"/>
   </a>

</div><!-- home-banner -->
