<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rosenlundsakeri
 */

?>
<div class="container">
	<div class="rl-inner-page">
<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'INGET HITTAT', 'rosenlundsakeri' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Redo att publicera ditt första inlägg? <a href="%1$s">Kom igång här</a>.', 'rosenlundsakeri' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Ledsen, men inget matchade dina söktermer. Vänligen försök igen med några andra sökord.', 'rosenlundsakeri' ); ?></p>
			<?php
				get_search_form();

		else : ?>

			<p><?php esc_html_e( 'Det verkar som om vi inte kan hitta vad du letar efter. Kanske söker kan hjälpa.', 'rosenlundsakeri' ); ?></p>
			<?php
				get_search_form();

		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
</div>
</div>
