<?php global $rosel_opt; ?>
<section class="description-block two-column"> 
    <div class="container">
        <div class="row">
            <p><?php echo wpautop(do_shortcode($rosel_opt['row-desc'])); ?></p>

        </div><!-- row -->
    </div><!-- container -->

</section><!-- contact-address -->
