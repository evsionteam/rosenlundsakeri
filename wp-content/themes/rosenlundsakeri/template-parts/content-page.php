<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rosenlundsakeri
 */

?>

	<div class="rl-inner-page">
		<div class="entry-content">
			<div class="container">
				<?php the_content(); ?>
			</div>
		</div><!-- .entry-content -->
	</div><!-- !.rl-inner-page -->
