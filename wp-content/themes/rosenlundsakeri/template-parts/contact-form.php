<?php global $rosel_opt;
if(!empty(get_post_meta( get_the_ID(), 'image_upload', true))){
$image_url = get_post_meta( get_the_ID(), 'image_upload', true);
} else {
    $image_url = $rosel_opt['default-footer-image']['url'];
}
?>

<div class="cont-form" style="background-image:url(<?php echo $image_url;?>)">
  <div class="container">
      <div class="row">

          <div class="col-sm-6 col-sm-offset-3">

              <div class="contact-info">
                  <div class="form-info">

                      <div class="cont-detail">
                          <div class="lines line-animation">
                              <span class="top-line"></span>
                              <span class="left-line"></span>
                              <span class="right-line"></span>
                          </div>
                          <?php
                          if (isset($rosel_opt['cf-desc'])) {
                              echo $rosel_opt['cf-desc'];
                          }
                          ?>
                      </div>

                      <div class="form-detail">
                          <?php
                          if (isset($rosel_opt['cf-shortcode'])) {
                              echo do_shortcode($rosel_opt['cf-shortcode']);
                          }
                          ?>
                          <?php //echo do_shortcode('[contact-form-7 id="50" title="Homepage Contact Form"]'); ?>
                      </div>

                  </div><!-- /.form-info -->
                </div><!-- /.contact-info -->

            </div><!-- /.col-sm-6 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.cont-form -->
