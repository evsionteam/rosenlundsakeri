<?php
/*Template Name: News Page*/
get_header('inner');
?>
    <section id="primary" class="content-area search-result news">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="rl-inner-page">
                    <?php
                    $args = array(
                        'post_type' => 'news',
                        'posts_per_page' => 5,
                        'paged' => $paged,
                    );
                    $query = new WP_Query( $args );
                    while ($query->have_posts()) : $query->the_post();
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium');
                        ?>
                        <article>
                            <div class = "search-image"><img src="<?php echo $image[0];?>"></div>
                            <div class="search-content">
                                <header class="entry-header">
                                    <h2 class = "entry-title">
                                        <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
                                    </h2>
                                    <div class="entry-meta"><?php echo get_the_date('M d Y'); ?></div>
                                </header>
                                <div class="entry-summary"><?php the_excerpt(); ?></div>
                            </div>
                        </article>

                    <?php endwhile;wp_reset_postdata(); ?>

                    <div class="pagination">
                        <?php
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links(array(
                            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                            'format' => '?paged=%#%',
                            'current' => max(1, get_query_var('paged')),
                            'total' => $query->max_num_pages
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </main>
    </section>

<?php get_footer();?>