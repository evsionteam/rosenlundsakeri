<?php get_header('inner'); ?>


<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();?>
				<div class="rl-inner-page">
					<div class="entry-content">
						<div class="container">
							<div class = "job-desc-wrap">
								<div class = "desc-content">
									<div class="text-desc-content">
											<div class="row">
												<div class = "col-sm-6">
													<div class="desc-wrap">
														<div class="desc-detail">
															<h4><?php _e('Arbetsbreskrivning:','rosenlundsakeri');?></h4>
															<?php the_content(); ?>	
														</div>
													</div><!-- desc-wrap -->
												</div>
												<div class = "col-sm-6">
													<div class="desc-wrap">
														<div class="desc-detail">
															<h4><?php _e('Kvalifikationer:','rosenlundsakeri');?></h4>
															<p><?php echo wpautop(get_post_meta( get_the_ID(), 'meta_job_qualification_more_text', true)); ?></p>
														</div>
														<div class="more-info">
														   <h5><?php _e('För mer info kontakta:','rosenlundsakeri');?></h5>
															
															<a href="<?php echo get_post_meta( get_the_ID(), 'jobbs_email', true); ?>"><?php echo get_post_meta( get_the_ID(), 'jobbs_email', true); ?></a>
															<!-- <a class="read-more" href="<?php //echo home_url('/')?>kontakt/"><?php //_e('Las mer','rosenlundsakeri');?></a> -->
														</div>
													</div>
												
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- .entry-content -->
				</div><!-- !.rl-inner-page -->
			<?php endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>