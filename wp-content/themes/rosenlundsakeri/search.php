<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Rosenlundsakeri
 */

get_header(); ?>

	<section id="primary" class="content-area search-result">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>


			<div class="container">
				<div class="rl-inner-page">
					<header class="page-header">
						<h1 class="page-title"><?php printf( esc_html__( 'Sökresultat för: %s', 'rosenlundsakeri' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					</header><!-- .page-header -->
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'search' );

					endwhile; ?>
				</div>
			</div><!-- container -->
			
			<?php the_posts_navigation();
				else :
				get_template_part( 'template-parts/content', 'none' );
			endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
