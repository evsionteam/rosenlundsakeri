<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rosenlundsakeri
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<script src="https://use.typekit.net/bct1apf.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<?php wp_head(); ?>
</head>
<?php global $rosel_opt; ?>
<body <?php body_class(); ?>>
<div id="page" class="site page">

		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'rosenlundsakeri' ); ?></a>

	  <header id="masthead" class="site-header" role="banner" data-stellar-background-ratio="0.5">
	      <div class="site-branding">
	              <h1><a href="<?php echo home_url('/');?>"><img src="<?php echo $rosel_opt['logo-image']['url']; ?>"></a></h1>
	              <!-- <?php
	              if ( is_front_page() && is_home() ) : ?>
	                      <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
	              <?php else : ?>
	                      <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
	              <?php
	              endif;

	              $description = get_bloginfo( 'description', 'display' );
	              if ( $description || is_customize_preview() ) : ?>
	                      <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
	              <?php
	              endif; ?> -->
	      </div><!-- .site-branding -->
	      <div class="nav-wrap">
	              <nav id="site-navigation" class="main-navigation" role="navigation">
	                      <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'rosenlundsakeri' ); ?></button>
	                      <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
	              </nav><!-- #site-navigation -->

	              <div class="nav-info">
	                    <div class="info-list">
                              <a class="icon-search" href="#">
	   								<svg class="icon">
										<path class="path1" d="M15.504 13.616l-3.79-3.223c-0.392-0.353-0.811-0.514-1.149-0.499 0.895-1.048 1.435-2.407 1.435-3.893 0-3.314-2.686-6-6-6s-6 2.686-6 6 2.686 6 6 6c1.486 0 2.845-0.54 3.893-1.435-0.016 0.338 0.146 0.757 0.499 1.149l3.223 3.79c0.552 0.613 1.453 0.665 2.003 0.115s0.498-1.452-0.115-2.003zM6 10c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"></path>
									</svg>
							  </a>
                                  <div class="search-box">
                                        <form method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		                                  <input type="search" id="" class="search-field" placeholder="Sök ..." value="" name="s" title="Sök:">
		                                  <!-- <input type="hidden" name="post_type" value="product"> -->
		                                  <button type="submit">
	                                        <i>
	                                           <svg class="icon">
             										<path class="path1" d="M15.504 13.616l-3.79-3.223c-0.392-0.353-0.811-0.514-1.149-0.499 0.895-1.048 1.435-2.407 1.435-3.893 0-3.314-2.686-6-6-6s-6 2.686-6 6 2.686 6 6 6c1.486 0 2.845-0.54 3.893-1.435-0.016 0.338 0.146 0.757 0.499 1.149l3.223 3.79c0.552 0.613 1.453 0.665 2.003 0.115s0.498-1.452-0.115-2.003zM6 10c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"></path>
             									</svg>
											</i>
		                                  </button>
                                 		</form>
                                    </div>
                              <a class="lang" href="#"><?php _e('SV','hugonorrkopng'); ?></a>
                              <a class="login-form" href="<?php echo get_site_url(3,'login') ?>"><?php _e('Logga in','hugonorrkopng'); ?></a>
  	                    </div>
	                      <a class="mobile-menu" href="#menu"><span class="bar"></span></a>
	              </div>
	      </div><!-- nav-wrap -->
	  </header><!-- #masthead -->

		<nav id="menu" style="display: none;">
    	<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_id' => 'mobile-menu' ) ); ?>
		</nav>

	<div id="content" class="site-content">
	<?php
	if(!empty(get_post_meta( get_the_ID(), 'header_image_upload', true))){
		$image[0] = get_post_meta( get_the_ID(), 'header_image_upload', true);
} elseif(has_post_thumbnail(get_the_ID())){
	$image_id = get_post_thumbnail_id( get_the_ID() );
	$image = wp_get_attachment_image_src( $image_id, 'full');
} else {
	$image[0] = $rosel_opt['default-footer-image']['url'];
}
	?>

	<header class="entry-header about-banner" style="background-image:url(<?php echo $image[0];?>)">
		<div class="above-container">
			<div class = "about-banner-content">
		        <div class = "about-center-text">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<?php if(!empty(get_post_meta( get_the_ID(), 'banner_page_subtitle', true))){?>
						<h2><?php echo get_post_meta( get_the_ID(), 'banner_page_subtitle', true); ?></h2>
					<?php } ?>
					<?php if(!empty(get_post_meta( get_the_ID(), 'banner_page_link', true))){?>
						<a href = "<?php echo get_post_meta( get_the_ID(), 'banner_page_link', true); ?>" class = "rl-btn-blue"><?php echo get_post_meta( get_the_ID(), 'banner_page_link_text', true); ?></a></h2>
					<?php } ?>
				</div>
			</div>
		</div>
	</header><!-- .entry-header -->
