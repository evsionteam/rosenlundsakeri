<?php
global $rosel_opt;
if(!is_page( 'kontakt' )){
get_template_part('template-parts/contact', 'form'); 
} ?>
	</div><!-- #content -->

			<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="site-info">
					<div class="footer-top">

							<div class="container">
								<div class="row">

									<div class="col-sm-4 footer-top-block contact-block">
										<?php dynamic_sidebar('footer-1');?>
									</div><!-- col-sm-4 -->

									<div class="col-sm-4 footer-top-block">
										<?php dynamic_sidebar('footer-2');?>
									</div><!-- col-sm-4 -->

									<div class="col-sm-4 footer-top-block">
										<?php dynamic_sidebar('footer-3');?>
									</div><!-- col-sm-4 -->

								</div><!-- row -->
							</div><!-- container -->

					</div><!-- footer-top -->
				</div><!-- .site-info -->
			</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>


</body>
</html>
