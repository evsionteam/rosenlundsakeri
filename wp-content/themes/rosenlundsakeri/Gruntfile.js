module.exports = function( grunt ){
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		//sass compile
		sass: {                              // Task
		    dist: {                            // Target
		    	options: {                       // Target options
		      		style: 'compact'
		      	},
		      	files: {                         // Dictionary of files
		        	'./assets/src/css/homepage-above-fold.css': './assets/src/sass/homepage-above-fold.scss', 
		        	'./assets/src/css/inner-above-fold.css': './assets/src/sass/inner-above-fold.scss',     // 'destination': 'source'
		        	'./assets/src/css/main.css': './assets/src/sass/main.scss'
		    	}
			}
		},
		
		//js and css concats
		concat: {
			options:{
				separator:"\n \n /*** New File ***/ "
			},
			js: { 
				src: [ 
				'./assets/src/js/ClassTigger.js',

				'./assets/src/js/wrapper/start.js',
				'./assets/src/js/jquery.mmenu.all.min.js',
				'./assets/src/js/skip-link-focus-fix.js',
				'./node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
				'./node_modules/jquery.mmenu/dist/js/jquery.mmenu.all.min.js',
				'./assets/src/js/ClassTigger.js',
				'./assets/src/js/jquery.carouFredSel-6.0.0.js',
				'./assets/src/js/TweenMax.min.js',
				'./assets/src/js/404.js',
				'./assets/src/js/main.js',
				'./assets/src/js/wrapper/end.js'

				],

				dest: './assets/dist/js/script.js'
			},
			adminjs: { 
				src: [ 
				"./assets/src/js/meta-video.js",
				],

				dest: "./assets/dist/js/admin-script.js"
			},
			 css: {
				src: [
					 './node_modules/jquery.mmenu/dist/css/jquery.mmenu.all.css',
					 './assets/src/css/main.css'
				],
				
				dest: './assets/src/css/main.css'
			 }
            
		},

		//js minify
		uglify: {
			options: {
				report: 'gzip'
			},
			main: {
				src: ['./assets/dist/js/script.js'],
				dest: './assets/dist/js/script.min.js'
			},
			adminmain: {
				src: ["./assets/dist/js/admin-script.js"],
				dest: "./assets/dist/js/admin-script.min.js"
			}
		},

		//css minify
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1,
				keepSpecialComments : 0,
				sourceMap: false
			},
			target: {
				files: [{
					expand: true,
					cwd: './assets/src/css/',
					src: ['*.css', '!*.min.css'],
					dest: './assets/dist/css/',
					ext: '.min.css'
				}]
			}
		},

		//copy content
		copy: {
			main: {
		      files: [
		      		{ expand: true, cwd: './assets/src/img', src: '**', dest: './assets/dist/img/', filter: 'isFile'},
		      		{ expand: true, cwd: './assets/src/css', src: '*.css', dest: './assets/dist/css/', filter: 'isFile'}
		      ]
		  }
		},
		
		watch: {
			js : {
				files : ['./assets/src/js/*.js', './assets/src/js/**/*.js'],
				tasks : [ 'default' ]
			},
			css : {
				files : [ './assets/src/sass/*.scss', './assets/src/sass/**/**/**/*.scss', './assets/src/sass/**/**/*.scss', './assets/src/sass/**/*.scss' ],
				tasks : [ 'default']
			}
		} 

	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-sass');

    //register grunt default task
   /// grunt.registerTask( 'css', [ 'sass', 'concat', 'cssmin'] );
   /// grunt.registerTask( 'js', [ 'concat', 'uglify' ] );
  

    grunt.registerTask('default', [ 'sass', 'concat', 'copy', 'uglify','cssmin'] );
}