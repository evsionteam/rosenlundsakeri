$(document).ready(function () {
    // 404 page animation 
    var $leftEye = $('#left-eye'),
            $rightEye = $('#right-eye'),
            $leftLeg = $('#left-leg'),
            $rightLeg = $('#right-leg'),
            $leftHand = $('#left-hand'),
            $rightHand = $('#right-hand'),
            $arrowUp = $('.arrow-up'),
            $bird = $('#bird'),
            $shadow = $('#shadow');

    // SHADOW
    TweenMax.from($shadow, 1, {
        scale: 0.7,
        opacity: 0.4,
        transformOrigin: 'center',
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1
    });

    //BIRD
    TweenMax.to($bird, 1, {
        y: 30,
        transformOrigin: 'center',
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });

    //EYES
    TweenMax.to($leftEye, 1, {
        x: -5,
        y: 1,
        transformOrigin: 'center',
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });
    TweenMax.to($rightEye, 1, {
        x: -5,
        y: 1,
        transformOrigin: 'center',
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });

    // HANDS
    TweenMax.to($rightHand, 0.2, {
        transformOrigin: 'left',
        rotationY: 30,
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });
    TweenMax.to($leftHand, 0.2, {
        transformOrigin: 'right',
        rotationY: 30,
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });

    TweenMax.from($leftLeg, 1, {
        transformOrigin: 'top',
        rotationX: 30,
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });
    TweenMax.from($rightLeg, 1, {
        transformOrigin: 'top',
        rotationX: 30,
        yoyo: true,
        ease: Power0.easeNone,
        repeat: -1

    });
//        TweenMax.to($arrowUp, 2, {
//            y:"+=20",
//            opacity:0,
//            ease: Power0.easeNone,
//            repeat: -1,
//            delay:2
//        }, "-=0.5");

    //text animation
    TweenMax.staggerFrom($('.message > *'), 0.6, {
        x: 40,
        opacity: 0,
        yoyo: true,
        ease: Power0.easeNone,
    }, 0.2);

});
