//scroll animation jquery Plugin
+(function ($) {
    $.fn.scrollAnimation = function (setting) {
        //defaultsetting
        var defaultSetting = {
            className: 'active'
        }
        $.extend(defaultSetting, setting);

        try{
          //variables declearation
          console.log(this.selector);

          var $window =$(window),
              windowHeight = $window.height(),
              _this    =$(this),
              elementOffsetTop  = $(this.selector).offset().top,
              revelAnimationOffset = elementOffsetTop - ( windowHeight*3/4 );
              //console.log("windowHeight "+windowHeight+"  elementOffsetTop "+elementOffsetTop+ "  revelAnimationOffset "+revelAnimationOffset);
              $window.scroll(addClass);
              //addclass function
              function addClass(){
                var scrollTopPosition = $window.scrollTop();
                    //console.log(scrollTopPosition);
                    if(scrollTopPosition >= revelAnimationOffset){
                      if(!_this.hasClass('active')){
                          _this.addClass(defaultSetting.className);
                      }
                    }
              }
        }catch(e){
          console.warn( "No element found for " + this.selector );
        }
    };
})(jQuery);


//scroll down plugin
+(function(){
  jQuery.fn.scrollDown = function () {
      this.click(function ( e ) {
          e.preventDefault();
          var $this                   = jQuery( this ),
              $element                = jQuery( 'body, html' ),  //don't select html to scroll it's won't work
              target                  = $this.attr( 'data-target' ),
              reduceHeightAttr        = $this.attr( 'data-reduce-offset' ),
              animationDuration       = $this.attr( 'data-animation-duration' ),
              //elementOffset
              targetOffset            = jQuery( target ).offset().top,
              //settingDefaultValue
              animationDurationValue  = ( typeof animationDuration != 'undefined' ) ? parseInt( animationDuration ) : 5000,
              //calculatingActualOffset
              actualOffset            = targetOffset - reduceHeight( reduceHeightAttr );

              function reduceHeight( param ){
                  var reduceHeight;
                  if( jQuery( param ).length > 0  ){
                      reduceHeight = jQuery( param ).outerHeight();
                      return reduceHeight;
                  }else if( jQuery.isNumeric( param ) ){
                      return parseInt( param );
                  }
                  return 0;
              }

              //scrollAnimation
              $element.stop().animate({
                  scrollTop: actualOffset
              }, animationDurationValue );
      });
  }
})();


//LOOK AND FILL CLASS
function lookNfill() {

    this.init = function () {
        var _this = this;
        //document ready
        jQuery(document).ready(function () {
            _this.search();
            _this.scrollDown();
        });
        //window load
        jQuery(window).load(function () {
            _this.mmenu();
            _this.scrollAnimation();
            _this.textSlider();
            _this.bannerAni();
            _this.loader();
            _this.bannerHeight();
        });
        jQuery(window).resize(function () {
          _this.bannerHeight();
        });
    }

    //header search
    this.search = function () {
        $searchIcon = jQuery('.icon-search'),
        $searchBox = jQuery('.search-box');
        $searchIcon.click(function () {
            $searchBox.slideToggle('300');
        });
    }

    //home page text slider
    this.textSlider = function () {
        $slider = jQuery('#text-carousel');
        $slider.carouFredSel({
            responsive: true,
            items: {visible: 1, },
            scroll: {pauseOnHover: true},
            auto: true,
            prev: ".previous",
            next: ".next"
        });
    }

    //mmenu
    this.mmenu = function () {
        $mmenu = jQuery('#menu'),
        $mmenu.show();
        $mmenu.mmenu({
            extensions: ['effect-slide-menu', 'pageshadow'],
            navbar: {
                title: 'Menu'
            },
            offCanvas: {
                position: "right"
            }
        });
    }

    //banner line animation
    this.bannerAni = function () {
        $homeBannerLine = jQuery('.home-banner .line');
        $homeBannerLine.addClass('active');
    }

    //scroll animatiaon
    this.scrollAnimation = function(){
        $('.cont-detail .lines').scrollAnimation();
    }

    //scroll down on click
    this.scrollDown = function(){
      jQuery('.rl-scroll-down').scrollDown();
    }

    //hide site loader
    this.loader = function(){
      var $loader = jQuery( '.loader-overlay' ),
          $body = jQuery( 'body');
          $loader.fadeOut(600, function(){ this.remove(); });
          $body.css( 'overflow','initial' );
    }

    //banner height
    this.bannerHeight = function(){
      var $window      = jQuery( window ),
          $header      = jQuery( '#masthead' ),
          $banner      = jQuery( '.home-banner' ),
          windowHeight = $window.height(),
          headerHeight = $header.outerHeight(),
          bannerHeight = windowHeight - headerHeight - 135;
          $banner.css( 'min-height', bannerHeight+'px' );
    }

}//lookNfill
new lookNfill().init();
