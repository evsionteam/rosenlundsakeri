<?php

if ( ! function_exists( 'rosenlundsakeri_setup' ) ) :

function rosenlundsakeri_setup() {

	load_theme_textdomain( 'rosenlundsakeri', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'rosenlundsakeri' ),
		'mobile' => esc_html__( 'Mobile', 'rosenlundsakeri' ),
		'footer' => esc_html__( 'Footer (SiteMap)', 'rosenlundsakeri' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_image_size( 'service', 750, 450, true);
	add_image_size( 'team', 262, 394, true );	

	/*$author = get_role('editor');
	add_role( 'slider_editor', __('Slider Editor', 'rosenlundsakeri'),
		$author->capabilities
	 );*/
}
endif;
add_action( 'after_setup_theme', 'rosenlundsakeri_setup' );

function rosenlundsakeri_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rosenlundsakeri_content_width', 640 );
}
add_action( 'after_setup_theme', 'rosenlundsakeri_content_width', 0 );


require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/extras.php';

require get_template_directory() . '/inc/widget.php';

require get_template_directory() . '/inc/enqueue.php';

require get_template_directory() . '/inc/shortcode/about.php';
require get_template_directory() . '/inc/shortcode/page.php';

require get_template_directory() . '/inc/shortcode/jobs.php';

require get_template_directory() . '/inc/post-types/team.php';
require get_template_directory() . '/inc/post-types/news.php';
require get_template_directory() . '/inc/post-types/jobs.php';
require get_template_directory() . '/inc/post-types/services.php';
require get_template_directory() . '/inc/favicon.php';

require get_template_directory() . '/inc/page-meta.php';

require_once (dirname(__FILE__) . '/redux/config.php');

function rosenlunds_add_attribute( $atts, $item, $args ) {
	$menu_items = array(164,33);
	if (in_array($item->ID, $menu_items)) {
	  $atts['class'] = 'rl-scroll-down';
	  $atts['data-target'] = '.cont-detail';
	  $atts['data-reduce-offset'] = '#masthead';
	  $atts['data-animation-duration'] = '500';
	}

    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'rosenlunds_add_attribute', 10, 3 );


add_filter( 'network_posts_where', 'include_blogs', 10, 2 );
if ( !function_exists( 'include_blogs' ) ) :
	/**
	 * Excludes blogs from network query.
	 *
	 * @since 3.0.4
	 *
	 * @param string $where Initial WHERE clause of the network query.
	 * @param Network_Query $query The network query object.
	 * @return string Updated WHERE clause.
	 */
	function include_blogs( $where, Network_Query $query ) {
		return !empty($query->post->query_vars['blogs_in'] )
			? $where . sprintf( ' AND %s.BLOG_ID IN (%s) ',$query->post->network_posts, implode( ', ', (array)$query->query_vars['blogs_in'] ) )
			: $where;
	}
endif;


function wp_get_post_featured_image_src_my( $post_id, $post_blog_id, $size = NULL ) {
    switch_to_blog( $post_blog_id );
    $image_id = get_post_thumbnail_id( $post_id );
    if($image_id){
        $image = wp_get_attachment_image_src( $image_id, 'medium');
        switch_to_blog(1);
        return $image;
    } else {
        switch_to_blog(1);
        return false;
    }
}


add_filter( 'nav_menu_link_attributes', 'change_menu_link', 10, 3 );
function change_menu_link( $atts, $item, $args ) {
 if(is_page('kontakt')){
	$menu_items = array(164,33);
	if (in_array($item->ID, $menu_items)) {

	  $atts['href'] = home_url( "/boka-transport" );
	  $atts['class'] = '';
	  $atts['data-target'] = '.cont-detail';
	  $atts['data-reduce-offset'] = '';
	  $atts['data-animation-duration'] = '';
	}
}
    return $atts;
}