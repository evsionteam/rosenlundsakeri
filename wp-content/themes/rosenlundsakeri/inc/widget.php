<?php 
function rosenlundsakeri_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'rosenlundsakeri' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'rosenlundsakeri' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'rosenlundsakeri' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'rosenlundsakeri' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'rosenlundsakeri' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'rosenlundsakeri' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'rosenlundsakeri' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'rosenlundsakeri' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
	) );
}
add_action( 'widgets_init', 'rosenlundsakeri_widgets_init' );