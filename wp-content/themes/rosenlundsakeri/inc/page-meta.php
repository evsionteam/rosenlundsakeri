<?php 
//Banner SubTitle

add_action( 'add_meta_boxes', 'meta_page_subtitle' );
function meta_page_subtitle()
{
    $types = array( 'page', 'job', 'services' );
    foreach( $types as $type ) {
        add_meta_box( 'meta-page-subtitle', 'Banner Subtitle', 'meta_banner_subtitle_callback', $type, 'normal', 'high' );
    }
}

function meta_banner_subtitle_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['banner_page_subtitle'] ) ? $values1['banner_page_subtitle'][0] : '';

    wp_nonce_field( 'banner_page_subtitle', 'banner_my_page_subtitle' );
    ?>
    <p>
        <!-- <label for="banner_page_subtitle"><p>Enter Subtitle</p></label> -->
        <input type="text" name="banner_page_subtitle" id="banner_page_subtitle" size="100" value="<?php echo $selected1; ?>"></textarea>
    </p>
<?php
}

add_action( 'save_post', 'meta_page_subtitle_save' );
function meta_page_subtitle_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['banner_my_page_subtitle'] ) || !wp_verify_nonce( $_POST['banner_my_page_subtitle'], 'banner_page_subtitle' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['banner_page_subtitle'] ) )
        update_post_meta( $post_id, 'banner_page_subtitle', $_POST['banner_page_subtitle'] );

}

//Banner Page Link

add_action( 'add_meta_boxes', 'meta_page_link' );
function meta_page_link()
{
    $types = array( 'page', 'job', 'services' );
    foreach( $types as $type ) {
        add_meta_box( 'meta-page-link', 'Banner link', 'meta_banner_link_callback', $type, 'normal', 'high' );
    }
}

function meta_banner_link_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['banner_page_link'] ) ? $values1['banner_page_link'][0] : '';

    wp_nonce_field( 'banner_page_link', 'banner_my_page_link' );
    ?>
    <p>
        <!-- <label for="banner_page_link"><p>Enter link</p></label> -->
        <input type="text" name="banner_page_link" id="banner_page_link" size="100" value="<?php echo $selected1; ?>"></textarea>
    </p>
<?php
}

add_action( 'save_post', 'meta_page_link_save' );
function meta_page_link_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['banner_my_page_link'] ) || !wp_verify_nonce( $_POST['banner_my_page_link'], 'banner_page_link' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['banner_page_link'] ) )
        update_post_meta( $post_id, 'banner_page_link', $_POST['banner_page_link'] );

}

//Banner Page Link

add_action( 'add_meta_boxes', 'meta_page_link_text' );
function meta_page_link_text()
{
    $types = array( 'page', 'job', 'services' );
    foreach( $types as $type ) {
        add_meta_box( 'meta-page-link_text', 'Banner Link Text', 'meta_banner_link_text_callback', $type, 'normal', 'high' );
    }
}

function meta_banner_link_text_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['banner_page_link_text'] ) ? $values1['banner_page_link_text'][0] : '';

    wp_nonce_field( 'banner_page_link_text', 'banner_my_page_link_text' );
    ?>
    <p>
        <!-- <label for="banner_page_link_text"><p>Enter link_text</p></label> -->
        <input type="text" name="banner_page_link_text" id="banner_page_link_text" size="100" value="<?php echo $selected1; ?>"></textarea>
    </p>
<?php
}

add_action( 'save_post', 'meta_page_link_text_save' );
function meta_page_link_text_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['banner_my_page_link_text'] ) || !wp_verify_nonce( $_POST['banner_my_page_link_text'], 'banner_page_link_text' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['banner_page_link_text'] ) )
        update_post_meta( $post_id, 'banner_page_link_text', $_POST['banner_page_link_text'] );

}

add_action('add_meta_boxes', 'meta_image_footer_upload', 1);
function meta_image_footer_upload() {
    $types = array( 'page', 'job', 'services' );
    foreach( $types as $type ) {
        add_meta_box( 'arrangement-post-meta', __( 'Upload Footer Image' ), 'upload_image_display', $type, 'side', 'default');
    }
}


function upload_image_display($post) {

    $image_upload = get_post_meta( $post->ID, 'image_upload', true);
    wp_nonce_field( 'footer_image_meta_box_nonce', 'image_meta_box' );
    ?>
    <div class="image_field123">
        <label for="field-hidden-1"><?php _e( 'Image Upload: ', 'ev-intranet' )?></label><br/>
        <input type="hidden" id="field-hidden-1" class="image-upload" name="image_upload" value="<?php echo $image_upload; ?>" />
        <input id="upload_img_1" data-img-ele="#field-img-1" data-hidden-ele="#field-hidden-1" type="button" class="button upload_image" value="<?php _e( 'image Upload', 'ev-intranet' ); ?>" />
        <?php if ( '' != $image_upload): ?>
            <input id="delete_img_1" type="button" class="button delete_image" value="<?php _e( 'Delete Image', 'ev-intranet' ); ?>" />
        <?php endif; ?>
    </div>
    <div class="image_preview">
        <p class="description">
            <img id="field-img-1" style="max-width:300px;" src="<?php echo $image_upload; ?>" />
        </p>
    </div>
<?php
} 

add_action('save_post', 'image_meta_box_save');
function image_meta_box_save($post_id) {

    if ( ! isset( $_POST['image_meta_box'] ) || ! wp_verify_nonce( $_POST['image_meta_box'], 'footer_image_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'image_upload' ] ) ) {
        update_post_meta( $post_id, 'image_upload', $_POST[ 'image_upload' ] );
    }
}


add_action('add_meta_boxes', 'meta_image_header_upload', 1);
function meta_image_header_upload() {
    $types = array( 'job', 'services' );
    foreach( $types as $type ) {
        add_meta_box( 'header-post-meta-jobs', __( 'Upload Header Banner Image' ), 'upload_header_image_display', $type, 'side', 'default');
    }
}


function upload_header_image_display($post) {
    /*echo '<pre>';
    print_r(get_post_meta( $post->ID));
    echo '</pre>';
    die('Rubal');*/
    $header_image_upload = get_post_meta( $post->ID, 'header_image_upload', true);
    wp_nonce_field( 'header_image_meta_box_nonce', 'header_image_meta_box' );
    ?>
    <div class="image_field">
        <label for="field-hidden-2"><?php _e( 'Image Upload: ', 'ev-intranet' )?></label><br/>
        <input type="hidden" id="field-hidden-2" class="image-upload" name="header_image_upload" value="<?php echo $header_image_upload; ?>" />
        <input id="upload_img_2" type="button" data-img-ele="#field-img-2" data-hidden-ele="#field-hidden-2" class="button upload_image" value="<?php _e( 'image Upload', 'ev-intranet' ); ?>" />
        <?php if ( '' != $header_image_upload): ?>
            <input id="delete_img_2" type="button" class="button delete_image" value="<?php _e( 'Delete Image', 'ev-intranet' ); ?>" />
        <?php endif; ?>
    </div>
    <div class="image_preview">
        <p class="description">
            <img id="field-img-2" style="max-width:300px;" src="<?php echo $header_image_upload; ?>" />
        </p>
    </div>
<?php
} 

add_action('save_post', 'header_image_meta_box_save');
function header_image_meta_box_save($post_id) {

    if ( ! isset( $_POST['header_image_meta_box'] ) || ! wp_verify_nonce( $_POST['header_image_meta_box'], 'header_image_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'header_image_upload' ] ) ) {
        update_post_meta( $post_id, 'header_image_upload', $_POST[ 'header_image_upload' ] );
    }
}
