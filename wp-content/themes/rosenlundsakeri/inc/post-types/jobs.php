<?php 
function custom_post_type_job() {

    $labels = array(
        'name'                  => _x( 'Lediga jobb', 'Post Type General Name', 'rosenlundsakeri' ),
        'singular_name'         => _x( 'Lediga jobb', 'Post Type Singular Name', 'rosenlundsakeri' ),
        'menu_name'             => __( 'Lediga jobb', 'rosenlundsakeri' ),
        'name_admin_bar'        => __( 'Lediga jobb', 'rosenlundsakeri' ),
        'archives'              => __( 'Item Archives', 'rosenlundsakeri' ),
        'parent_item_colon'     => __( 'Parent Item:', 'rosenlundsakeri' ),
        'all_items'             => __( 'All Items', 'rosenlundsakeri' ),
        'add_new_item'          => __( 'Add New Item', 'rosenlundsakeri' ),
        'add_new'               => __( 'Add New', 'rosenlundsakeri' ),
        'new_item'              => __( 'New Item', 'rosenlundsakeri' ),
        'edit_item'             => __( 'Edit Item', 'rosenlundsakeri' ),
        'update_item'           => __( 'Update Item', 'rosenlundsakeri' ),
        'view_item'             => __( 'View Item', 'rosenlundsakeri' ),
        'search_items'          => __( 'Search Item', 'rosenlundsakeri' ),
        'not_found'             => __( 'Not found', 'rosenlundsakeri' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'rosenlundsakeri' ),
        'featured_image'        => __( 'Featured Image', 'rosenlundsakeri' ),
        'set_featured_image'    => __( 'Set featured image', 'rosenlundsakeri' ),
        'remove_featured_image' => __( 'Remove featured image', 'rosenlundsakeri' ),
        'use_featured_image'    => __( 'Use as featured image', 'rosenlundsakeri' ),
        'insert_into_item'      => __( 'Insert into item', 'rosenlundsakeri' ),
        'upreaded_to_this_item' => __( 'Upreaded to this item', 'rosenlundsakeri' ),
        'items_list'            => __( 'Items list', 'rosenlundsakeri' ),
        'items_list_navigation' => __( 'Items list navigation', 'rosenlundsakeri' ),
        'filter_items_list'     => __( 'Filter items list', 'rosenlundsakeri' ),
    );
    $args = array(
        'label'                 => __( 'Lediga jobb', 'rosenlundsakeri' ),
        'description'           => __( 'Lediga jobb', 'rosenlundsakeri' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'rewrite' => array('slug' => 'lediga-tjanster')
    );
    register_post_type( 'job', $args );

}
add_action( 'init', 'custom_post_type_job', 0 );


//Job Qualification

add_action( 'add_meta_boxes', 'meta_job_qualification_more' );
function meta_job_qualification_more()
{
    add_meta_box( 'meta-job-qualification', 'Kvalifikationer', 'meta_job_qualification_callback', 'job', 'normal', 'high' );
}

function meta_job_qualification_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['meta_job_qualification_more_text'] ) ? $values1['meta_job_qualification_more_text'][0] : '';

    wp_nonce_field( 'meta_job_qualification_more_text_nonce', 'my_meta_job_qualification_more_text_nonce' );
    ?>
    <p>
        <textarea rows="10" cols="120" name="meta_job_qualification_more_text"  id="meta_job_qualification_more_text"><?php echo $selected1; ?></textarea>
    </p>
    <?php   
}

add_action( 'save_post', 'meta_job_qualification_more_save' );
function meta_job_qualification_more_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_meta_job_qualification_more_text_nonce'] ) || !wp_verify_nonce( $_POST['my_meta_job_qualification_more_text_nonce'], 'meta_job_qualification_more_text_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['meta_job_qualification_more_text'] ) )
        update_post_meta( $post_id, 'meta_job_qualification_more_text', $_POST['meta_job_qualification_more_text'] );

}

//Job Email

add_action( 'add_meta_boxes', 'meta_job_email' );
function meta_job_email()
{
    add_meta_box( 'meta-job-email', 'Email Address', 'meta_job_email_callback', 'job', 'normal', 'high' );
}

function meta_job_email_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['jobbs_email'] ) ? $values1['jobbs_email'][0] : '';

    wp_nonce_field( 'jobbs_email_nonce', 'my_jobbs_email_nonce' );
    ?>
    <p>
        <input type="text" name="jobbs_email" id="jobbs_email" size="40" value="<?php echo $selected1; ?>"></textarea>
    </p>
<?php
}

add_action( 'save_post', 'meta_job_email_save' );
function meta_job_email_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_jobbs_email_nonce'] ) || !wp_verify_nonce( $_POST['my_jobbs_email_nonce'], 'jobbs_email_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['jobbs_email'] ) )
        update_post_meta( $post_id, 'jobbs_email', $_POST['jobbs_email'] );

}