<?php 

function custom_post_type_services() {

    $labels = array(
        'name'                  => _x( 'Services', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Services', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Services', 'text_domain' ),
        'name_admin_bar'        => __( 'Services', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'upreaded_to_this_item' => __( 'Upreaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Services', 'text_domain' ),
        'description'           => __( 'Services', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'services', $args );

}
add_action( 'init', 'custom_post_type_services', 0 );

//services Qualification

add_action( 'add_meta_boxes', 'meta_services_qualification_more' );
function meta_services_qualification_more()
{
    add_meta_box( 'meta-services-qualification', 'Kontaktinformation', 'meta_services_qualification_callback', 'services', 'normal', 'high' );
}

function meta_services_qualification_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['meta_services_qualification_more_text'] ) ? $values1['meta_services_qualification_more_text'][0] : '';
    $email = isset( $values1['services_email'] ) ? $values1['services_email'][0] : '';
    $name = isset( $values1['services_name'] ) ? $values1['services_name'][0] : '';
    $phone = isset( $values1['services_phone'] ) ? $values1['services_phone'][0] : '';

    wp_nonce_field( 'meta_services_qualification_more_text_nonce', 'my_meta_services_qualification_more_text_nonce' );
    ?>
    <p>
        <textarea rows="10" cols="120" name="meta_services_qualification_more_text"  id="meta_services_qualification_more_text"><?php echo $selected1; ?></textarea>
    </p>
    <p><?php _e('Nam', 'rosenlundsakeri'); ?></p>
    <input type="text" name="services_name" id="services_name" size="40" value="<?php echo $name; ?>">
    <p><?php _e('Telefon', 'rosenlundsakeri'); ?></p>
    <input type="text" name="services_phone" id="services_phone" size="40" value="<?php echo $phone; ?>">
    <p><?php _e('Email', 'rosenlundsakeri'); ?></p>
    <input type="text" name="services_email" id="services_email" size="40" value="<?php echo $email; ?>">
    <?php   
}

add_action( 'save_post', 'meta_services_qualification_more_save' );
function meta_services_qualification_more_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_meta_services_qualification_more_text_nonce'] ) || !wp_verify_nonce( $_POST['my_meta_services_qualification_more_text_nonce'], 'meta_services_qualification_more_text_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['meta_services_qualification_more_text'] ) )
        update_post_meta( $post_id, 'meta_services_qualification_more_text', $_POST['meta_services_qualification_more_text'] );
     if( isset( $_POST['services_email'] ) )
        update_post_meta( $post_id, 'services_email', $_POST['services_email'] );
     if( isset( $_POST['services_name'] ) )
        update_post_meta( $post_id, 'services_name', $_POST['services_name'] );
     if( isset( $_POST['services_phone'] ) )
        update_post_meta( $post_id, 'services_phone', $_POST['services_phone'] );

}

//services Email

