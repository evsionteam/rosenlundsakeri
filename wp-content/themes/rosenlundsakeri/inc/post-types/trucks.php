<?php 

function custom_post_type_trucks() {

    $labels = array(
        'name'                  => _x( 'Trucks', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Trucks', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Trucks', 'text_domain' ),
        'name_admin_bar'        => __( 'Trucks', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'upreaded_to_this_item' => __( 'Upreaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Trucks', 'text_domain' ),
        'description'           => __( 'Trucks', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'trucks', $args );

}
add_action( 'init', 'custom_post_type_trucks', 0 );


//Trucks SVG Code

add_action( 'add_meta_boxes', 'meta_trucks_read_more' );
function meta_trucks_read_more()
{
    add_meta_box( 'meta-trucks-read', 'SVG Image Code', 'meta_box_callback3', 'trucks', 'normal', 'high' );
}

function meta_box_callback3( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['meta_trucks_read_more_text'] ) ? $values1['meta_trucks_read_more_text'][0] : '';

    wp_nonce_field( 'meta_trucks_read_more_text_nonce', 'my_meta_trucks_read_more_text_nonce' );
    ?>
    <p>
        <label for="meta_trucks_read_more_text"><p>SVG Image</p></label>
        <textarea rows="50" cols="120" name="meta_trucks_read_more_text"  id="meta_trucks_read_more_text"><?php echo $selected1; ?></textarea>
    </p>
    <?php   
}

add_action( 'save_post', 'meta_trucks_read_more_save' );
function meta_trucks_read_more_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_meta_trucks_read_more_text_nonce'] ) || !wp_verify_nonce( $_POST['my_meta_trucks_read_more_text_nonce'], 'meta_trucks_read_more_text_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['meta_trucks_read_more_text'] ) )
        update_post_meta( $post_id, 'meta_trucks_read_more_text', $_POST['meta_trucks_read_more_text'] );

}
