<?php 

function custom_post_type_team() {

    $labels = array(
        'name'                  => _x( 'Team', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Team', 'text_domain' ),
        'name_admin_bar'        => __( 'Team', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'upreaded_to_this_item' => __( 'Upreaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Team', 'text_domain' ),
        'description'           => __( 'Team', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'team', $args );
}
add_action( 'init', 'custom_post_type_team', 0 );


/* Team additional Information */
add_action( 'add_meta_boxes', 'meta_team_description' );
function  meta_team_description(){
    add_meta_box( 'meta-team-description', __('Additional Information','rosenlundsakeri'), 'meta_box_callback_team', 'team', 'normal', 'high' );
}

function meta_box_callback_team( $post ){
    wp_nonce_field( 'team_designation_nonce', 'my_team_designation_nonce' );
    $team_designation = get_post_meta( $post->ID, 'team_designation', true );
    $meta_team_phone = get_post_meta( $post->ID, 'meta_team_phone', true );
    $meta_team_email = get_post_meta( $post->ID, 'meta_team_email', true );
?>
    <table>
        <tbody>
            <tr>
                <th><label for="team_designation"><?php _e('Enter Designation','rosenlundsakeri'); ?></label></th>
                <td>
                    <input type="text" name="team_designation" id="team_designation" class="regular-text" size="20" value="<?php echo $team_designation; ?>" />
                </td>
            </tr>

            <tr>
                <th><label for="meta_team_phone"><?php _e('Phone Number','rosenlundsakeri'); ?></label></th>
                <td>
                    <input type="text" name="meta_team_phone" id="meta_team_phone" class="regular-text" size="20" value="<?php echo $meta_team_phone; ?>" />
                </td>
            </tr>

            <tr>
                <th><label for="meta_team_email"><?php _e('Email Address','rosenlundsakeri'); ?></label></th>
                <td>
                    <input type="text" name="meta_team_email" id="meta_team_email" class="regular-text" size="20" value="<?php echo $meta_team_email; ?>" />
                </td>
            </tr>
            

           
        </tbody>
    </table>
<?php
}

add_action( 'save_post', 'save_meta_team_description' );
function save_meta_team_description( $post_id ){
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    if( !isset( $_POST['my_team_designation_nonce'] ) || !wp_verify_nonce( $_POST['my_team_designation_nonce'], 'team_designation_nonce' ) ) return;

    if( !current_user_can( 'edit_post' ) ) return;

    update_post_meta( $post_id, 'team_designation', $_POST['team_designation'] );
    update_post_meta( $post_id, 'meta_team_phone', $_POST['meta_team_phone'] );
    update_post_meta( $post_id, 'meta_team_email', $_POST['meta_team_email'] );
}