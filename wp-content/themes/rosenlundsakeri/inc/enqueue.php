<?php
function rosenlundsakeri_scripts() {
	$ver = '1.0.0';

	$min = '.min';
	if( defined('SCRIPT_DEBUG' ) && SCRIPT_DEBUG === true ) {
		$min = '';
	}
	if(is_front_page()){
		wp_enqueue_style( 'rosenlundsakeri', get_template_directory_uri() . '/assets/dist/css/homepage-above-fold' . $min . '.css', array(), $ver );
	} else {
		wp_enqueue_style( 'rosenlundsakeri', get_template_directory_uri() . '/assets/dist/css/inner-above-fold' . $min . '.css', array(), $ver );
	}
	wp_dequeue_style('contact-form-7');
	wp_enqueue_style( 'contact-form-7', plugins_url('contact-form-7/includes/css').'style.css', array('rosenlundsakeri'), null );
	wp_enqueue_style( 'rosenlundsakeri-style', get_stylesheet_uri(), array('rosenlundsakeri') );
	wp_enqueue_script( 'rosenlundsakeri', get_template_directory_uri() . '/assets/dist/js/script' . $min . '.js', array('jquery'), $ver, true );

	$main_css = get_template_directory_uri() . '/assets/dist/css/main'.$min.'.css';
    $script = "
       var head  = document.getElementsByTagName('head')[0];
       var link  = document.createElement('link');
       link.rel  = 'stylesheet';
       link.type = 'text/css';
       link.href = '".$main_css."';
       link.media = 'all';
       head.appendChild(link);
    ";

    $webfont = "WebFontConfig = {
		google: { families: ['Noto Sans:400,400i,700,700i', 'Noto Serif:400,400i,700,700i','Open Sans:300,300i,400,400i,600,600i,700,700i,800:latin']}
	};
 	(function() {
		var wf = document.createElement('script');
		wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
	(function(d) {
    var config = {
      kitId: 'bct1apf',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,'')+' wf-inactive';},config.scriptTimeout),tk=d.createElement('script'),f=false,s=d.getElementsByTagName('script')[0],a;h.className+=' wf-loading';tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!='complete'&&a!='loaded')return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);";


	wp_enqueue_script( 'rosenlundsakeri-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'rosenlundsakeri-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_add_inline_script( 'rosenlundsakeri',$script );
    wp_add_inline_script( 'rosenlundsakeri',$webfont );

    $theme_stylesheet_handle = 'rosenlundsakeri-style';
	$custom_script_handle    = 'rosenlundsakeri';

	if(!is_home()){
		wp_add_inline_style( $theme_stylesheet_handle,'body{
			opacity: 0
		}');
		
		$preloader = 'function preloader( param ){

			this.time = param.time;

			this.init = function(){

				var time = this.time;

				jQuery(window).load( function(){
					jQuery( "body" ).animate({
						opacity : 1
					},time);
				})
			}
		} jQuery(document).ready(function(){ new preloader({ time: 500 }).init(); });';

		wp_add_inline_script( $custom_script_handle,$preloader );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'rosenlundsakeri_scripts' );


add_action( 'admin_enqueue_scripts', 'ev_intranet__admin_scripts' );
function ev_intranet__admin_scripts() {
	$assets_url = get_stylesheet_directory_uri().'/assets/dist/';
	$suffix = '.min';
	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}
	wp_enqueue_media();
        wp_register_script( 'ev-intranet-admin', $assets_url.'js/admin-script'.$suffix.'.js', array( 'jquery' ),null,true );
        wp_localize_script( 'ev-intranet-admin', 'meta_image',
            array(
                'title' => __( 'Choose or Upload an Image', 'ev-intranet' ),
                'button' => __( 'Use this image', 'ev-intranet' ),
            )
        );
	wp_enqueue_script( 'ev-intranet-admin');
}
