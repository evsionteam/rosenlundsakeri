<?php
function jobbs_no_title($atts, $content = null) {

$output = '';

    $output .='<div class = "descp-text-wrap">';
    $output .='<div class="container">';
    $output .='<div class = "content">'.$content.'</div>';
    $output .='</div>';
    $output .='</div>';

return $output;
}

function free_jobs_list() {
    ob_start(); ?>
<div class = "row">
    <div class="job-list-sec">
        <h2>Lediga jobb</h2>

        <div class = "job-listing clearfix">
            <?php
            $args = array('post_type' => 'job',);
            $query = new WP_Query($args);
             while ($query->have_posts()) : $query->the_post();?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class = "job-descrp-list">
                        <?php $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
                        <div class = "job-descrp-thumbnail"><a href = "<?php the_permalink(); ?>"><img src = "<?php echo $image?>" width="470" height="350"></a></div>
                        <div class="job-descrp-detail">
                            <h3 class = "job-list-title"><a href = "<?php the_permalink(); ?>"><?php the_title();?></a></h3>
                            <div class = "job-descrp"><?php the_excerpt();?></div>
                            <div class = "load-more-link"><a href = "<?php the_permalink(); ?>">Läs mer.</a></div>
                        </div><!-- job-descrp-detail -->
                    </div><!-- job-descrp-list -->
                </div>
            <?php endwhile;
            wp_reset_postdata();?>
        </div> 
    </div><!-- job-listing-sec -->
</div>

    <?php return ob_get_clean();

}


function free_jobs_list_intranet() {
    ob_start(); ?>
<div class = "row">
    <div class="job-list-sec">
        <h2>Lediga jobb</h2>

        <div class = "job-listing clearfix">
            <?php
           $args1 = array(
                    'blogs_in'   => array(1,3),
                    'post_type' => 'job',
                    'meta_query' => array(
                        'relation' => 'OR',
                        array(
                            'key'     => '_job_publish_public',
                            'value'   => 1
                        ),
                        array(
                            'key'     => '_job_publish_public',
                            'value'   => 1,
                            'compare' => 'NOT EXISTS'
                        ),
                    ),
                );
                $network_query = new Network_Query( $args1 );
                
                while ($network_query->have_posts()) : $network_query->the_post(); 
                /*echo '<pre>';
                print_r($network_query);
                echo '</pre>';*/
                    $the_permalink = network_get_permalink();
                    $the_title = network_get_the_title();
                    $the_content = network_get_the_excerpt();
                    $image = wp_get_post_featured_image_src_my($network_query->post->ID, $network_query->post->BLOG_ID, 'medium'); ?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                     <div class = "job-descrp-list">
                        <div class = "job-descrp-thumbnail"><a href = "<?php echo $the_permalink; ?>"><img src = "<?php echo $image[0];?>"></a></div>
                        <div class="job-descrp-detail">
                            <h3 class = "job-list-title"><a href = "<?php echo $the_permalink; ?>"><?php echo $the_title;?></a></h3>
                            <div class = "job-descrp"><?php echo $the_content;?></div>
                            <div class = "load-more-link"><a href = "<?php echo $the_permalink; ?>">Läs mer.</a></div>
                        </div><!-- job-descrp-detail -->
                    </div><!-- job-descrp-list -->
                </div>
            <?php endwhile;
            wp_reset_postdata();?>
        </div> 
    </div><!-- job-listing-sec -->
</div>

    <?php return ob_get_clean();

}



function jobbs_footer_banner($atts) {
global $rosel_opt;
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

    $output = '';

    $output .='<div class="footer-banner" style="background-image: url('.$rosel_opt['banner-image-footer-jobbs']['url'].')">';
    $output .='<div class = "footer-banner-title">';
    $output .='<h3>'.$title.'</h3>';
    $output .='</div>';
    $output .='</div>';

    return $output;
}

function jobs_desc_content_top_title($atts) {
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

    $output = '';  
    $output = '<div class = "row">';  
    $output .='<div class = "col-sm-12 job-desc-title">';
    $output .='<h2>'.$title.'</h2>';
    $output .='</div>';
    $output .='</div>';
    
    return $output;
}

function jobs_desc_content($atts, $content = null) {
    extract(shortcode_atts(array(
    ), $atts));

    $output = '';  

    $output .= '<div class = "job-desc-wrap">';
    $output .= '<div class = "desc-content">'. do_shortcode($content) .'</div>';
    $output .= '</div>';

    return $output;
}

function jobbs_desc_footer_banner($atts) {
global $rosel_opt;
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

 $output = '';

$output .= '<div class="footer-banner" style="background-image: url('.$rosel_opt['banner-image-footer-jobbs-desc']['url'].')">';
$output .= '<div class = "footer-banner-title">';
$output .= '<h3>'.$title .'</h3>';
$output .= '</div>';
$output .= '</div>';

return $output;


}

function register_jobbs_shortcodes(){
    add_shortcode('jobbs-no-title', 'jobbs_no_title');
    add_shortcode('free-jobs-list', 'free_jobs_list');
    add_shortcode('free-jobs-list-intranet', 'free_jobs_list_intranet');
    add_shortcode('jobs-footer-banner', 'jobbs_footer_banner');
    add_shortcode('jobs-desc-title', 'jobs_desc_content_top_title');
    add_shortcode('jobs-desc-content', 'jobs_desc_content');
    add_shortcode('jobs-desc-footer-banner', 'jobbs_desc_footer_banner');
}
add_action( 'init', 'register_jobbs_shortcodes');



/*Services*/


function services_list() {
    ob_start(); ?>
<div class = "row">
    <div class = "service-sec-wrap clearfix">
        <?php
        $args = array('post_type' => 'services',);
        $query = new WP_Query($args);
         while ($query->have_posts()) : $query->the_post();?>
            <div class="col-sm-6">
                 <div class = "service-list-wrap">
                    <?php //$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
                    <?php $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() ); ?> 
                    <?php $image = wp_get_attachment_image_src( $post_thumbnail_id,'service' );
                    /*echo"<pre>";
                    print_r($image);
                    echo"</pre>";
                    continue;*/
                    ?>
                    <div class = "serv-image-holder"><a href = "<?php the_permalink(); ?>"><img src = "<?php echo $image[0]; ?>" width="650" height="450"></a></div>
                    <div class="serv-info-detail">
                        <h3 class = "serv-title-name"><a href = "<?php the_permalink(); ?>"><?php the_title();?></a></h3>
                        <div class = "serv-detail"><?php the_excerpt();?></div>
                        <div class = "load-more-link"><a href = "<?php the_permalink(); ?>">Läs mer.</a></div>
                    </div><!-- serv-info-detail -->
                </div><!-- service-list-wrap -->
            </div>
        <?php endwhile;
        wp_reset_postdata();?>
    </div>
</div>
    <?php return ob_get_clean();

}

function services_footer_banner($atts) {
global $rosel_opt;
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

    $output = '';


    $output .= '<div class="footer-banner" style="background-image: url('.$rosel_opt['banner-image-footer-services']['url'].')">';
    $output .= '<div class = "footer-banner-title">';
    $output .= '<h3>'.$title .'</h3>';
    $output .= '</div>';
    $output .= '</div>';


return $output;
}

function services_desc_footer_banner($atts) {
global $rosel_opt;
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

       $output = '';

        $output .= '<div class="footer-banner" style="background-image: url('.$rosel_opt['banner-image-footer-services-desc']['url'].')">';
    $output .= '<div class = "footer-banner-title">';
    $output .= '<h3>'.$title .'</h3>';
    $output .= '</div>';
    $output .= '</div>';


return $output;

}


function register_services_shortcodes(){
    add_shortcode('services-no-title', 'jobbs_no_title');
    add_shortcode('services-list', 'services_list');
    add_shortcode('services-footer-banner', 'services_footer_banner');
    add_shortcode('services-desc-footer-banner', 'services_desc_footer_banner');
    add_shortcode('services-desc-title', 'jobs_desc_content_top_title');
    add_shortcode('services-desc-content', 'jobs_desc_content');
    
}
add_action( 'init', 'register_services_shortcodes');