<?php
function kvalitet_pdf_download($atts) {
    extract(shortcode_atts(array(
        'title' => '',
        'image' => '',
        'pdf' => '',
    ), $atts)); 

$output = '';

    $output .='<div class="container">';
    $output .='<div class = "image"><a href = "'.$pdf.'" target = "_blank"><img src = "'.$image.'" alt = "'.$title.'"></a></div>';
    $output .='<div class = "title"><a href = "'.$pdf.'" target = "_blank">'.$title.'</a></div>';
    $output .='</div>';

return $output;

}

function kvalitet_register_shortcodes(){
    add_shortcode('kvalitet-pdf', 'kvalitet_pdf_download');
}
add_action( 'init', 'kvalitet_register_shortcodes');