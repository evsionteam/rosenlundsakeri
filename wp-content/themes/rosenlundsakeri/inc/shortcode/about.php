<?php
function about_title($atts) {
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

    ob_start(); ?>

    <div class="about-banner">
	    <div class = "about-banner-content">
	        <div class = "about-center-text">
	        	<h1><?php echo $title; ?></h1>
	        </div><!-- about-center-text -->
	    </div><!-- .about-banner-content -->
	</div><!-- .about-banner -->

    <?php return ob_get_clean();
}

function about_content($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

    ob_start(); ?>

    <div class = "descp-text-wrap">
    	<div class="container">
       		<div class = "title"><?php echo $title; ?></div><!-- title -->
        	<div class = "content"><?php echo $content; ?></div><!-- content -->
        </div><!-- container -->
    </div><!-- about-quality -->

    <?php return ob_get_clean();
}

function about_content_blue($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => '',
    ), $atts));

    ob_start(); ?>

    <div class = "about-us-blue">
    	<div class="container">
        	<div class = "title"><?php echo $title; ?></div>
        	<div class = "content"><?php echo $content; ?></div>
        </div><!-- container -->
    </div>

    <?php return ob_get_clean();
}
//add our shortcode movie

function recent_team_function() {
    ob_start(); ?>
     
            <div class = "row about-team"  style="display: flex;flex-wrap: wrap;">
                <?php
                $args = array('post_type' => 'team','posts_per_page' => -1 );
                $query = new WP_Query($args);
                 while ($query->have_posts()) : $query->the_post();?>
                 	<div class="col-md-3 col-sm-6 col-xs-12 about-team-block">
                 		 <div class = "about-team-holder">
                 		 	<?php 
                            $image_id = get_post_thumbnail_id( get_the_ID() );
                            $image = wp_get_attachment_image_src( $image_id, 'full'); ?>
        	            	<div class = "team-member-image"><img src = "<?php echo $image[0]; ?>"></div>
        	            	<div class="member-info-holder">
        	            		<div class = "team-member-name"><?php the_title();?></div>
        	            		<div class = "team-member-designation"><?php echo get_post_meta( get_the_ID() ,'team_designation', true );?></div>
        	            		<div class = "team-member-phone"><a href="tel:<?php echo get_post_meta( get_the_ID() ,'meta_team_phone', true );?>"><?php echo get_post_meta( get_the_ID() ,'meta_team_phone', true );?></a></div>
        	            		<div class = "team-member-email"><a href="mailto:<?php echo get_post_meta( get_the_ID() ,'meta_team_email', true );?>"><?php echo get_post_meta( get_the_ID() ,'meta_team_email', true );?></a></div>
        	            	</div><!-- member-info-holder -->
        	            </div><!-- about-team-holder -->
                    </div>
                <?php endwhile;
                wp_reset_postdata();?>
            </div>
      
    <?php return ob_get_clean();

}


function about_content_footer($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => '',
        'sub_title' => '',
    ), $atts));

    ob_start(); ?>

    <div class = "about-history">
        <div class = "title"><?php echo $title; ?></div>
        <div class = "sub-title"><?php echo $sub_title; ?></div>
        <div class = "content"><?php echo do_shortcode($content); ?></div>
    </div>

    <?php return ob_get_clean();
}


function rosen_column_shortcode( $atts, $content = null ) {
   $atts = shortcode_atts( array(
       'class' => '',
   ), $atts, 'col' );
   return '<div class="'.$atts['class'].'">' . do_shortcode($content) . '</div>';
}



function register_shortcodes(){
    add_shortcode('recent-team', 'recent_team_function');
    add_shortcode('about-first', 'about_content');
    add_shortcode('about-second-colored', 'about_content_blue');
    add_shortcode('about-title', 'about_title');
    add_shortcode('col', 'rosen_column_shortcode');
    add_shortcode('about-footer', 'about_content_footer');
}
add_action( 'init', 'register_shortcodes');
add_action( 'init', 'about_content');
add_action( 'init', 'about_content_blue');
add_action( 'init', 'about_title');
add_action( 'init', 'rosen_column_shortcode');
add_action( 'init', 'about_content_footer');