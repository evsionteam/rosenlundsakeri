// LOGO ANIMATION
function logoAni(arg) {

    var param = {
        selector: '.letter',
        letterAnimationTime: 0.5,
        letterDelay: 0.2,
        wordDelay: 10
    }
    $.extend(param, arg);

    this.selector = param.selector,
            this.letterAnimationTime = param.letterAnimationTime,
            this.letterDelay = param.letterDelay,
            this.wordDelay = param.wordDelay;

    //constructor function
    this.init = function () {
        this.animation();
    }

    // letter animation
    this.animation = function () {
        var tl = new TimelineMax({repeat: -1, repeatDelay: this.wordDelay});

        tl.staggerFrom($(this.selector), this.letterAnimationTime, {
            cycle: {
                y: [100, -100],
                x: [100, 100],
                rotation: [60, -60]
            },
            opacity: 0
        }, this.letterDelay);
    }

} //logoAni


// CUSTOME SLIDER
function newsSlider(arg) {

    var param = {
        sliderWrapper: null,
        time: 2000,
        indicator: null,
        timeLine: null
    }

    $.extend(param, arg);
    this.interval = null,
    this.timeLine = param.timeLine,
    this.sliderWrapper = param.sliderWrapper,
    this.time = param.time,
    this.indicator = param.indicator,
    this.defaultTime = 4000;

    //constructor function
    this.init = function () {

        this.currentTime = null,
        this.totalSlider = 0,
        this.currentPosition = 1,
        this.customTime = [];

        this.clearAnimation();
        if (this.interval !== null) {
            clearInterval(this.interval);
            console.log('interval cleared at init');
        }

        jQuery(this.timeLine).css('width', '0px');
        $(this.indicator + " .current").html("1");

        this.customTimeArray();
        this.updateTotalSlides();
        this.totalSliderHTML();
        this.addClass();
        this.next();
    }

    //total slider
    this.totalSliderHTML = function () {
        var totalSlider = this.totalSlider;
        jQuery(this.indicator + " .total").html(totalSlider);
    }

    this.updateTotalSlides = function () {
        this.totalSlider = $(this.sliderWrapper + " > div").length;
    }

    //time array
    this.customTimeArray = function () {
        var _this = this;
        this.destroy();
        jQuery(this.sliderWrapper + " > div").each(function (index) {
            var duration = parseInt(jQuery(this).attr('data-time'));

            duration = isNaN(duration) ? _this.defaultTime : duration;
            _this.customTime.push(duration);
        });
    }
    
    //add class in first child
    this.addClass = function () {
        jQuery(this.sliderWrapper + " div:first-child").addClass('active');
    }

    this.destroy = function () {
        this.customTime = [];
    }

    this.slidEvent = $.Event('sliderChanged', {});

    this.clearAnimation = function () {
        var $time = jQuery(this.timeLine);
        if( $time.length && this.totalSlider > 1  ){
            $time.stop();
            $time.clearQueue();
            $time.css('width', '0px');
        }
    };
    
    this.lineAnimation = function(){
        var $time = jQuery(this.timeLine);
        if( $time.length && this.totalSlider > 1 ){
            $time.animate({width: '100%'}, this.currentTime, function () {
                $time.css('width', '0px');
            });
        }
    }

    //add class to another div
    this.next = function () {
        var that = this;
        this.currentTime = this.customTime[0];

        console.log('custom time ' + this.customTime);
        console.log('current time ' + this.currentTime);

        this.lineAnimation();

        //debugger;
        var timing = function () {
            that.interval = setInterval(function () {

                that.currentPosition++;
                
                //console.log(that.currentPosition);
                jQuery(that.sliderWrapper).trigger(that.slidEvent);
                
                $(that.sliderWrapper + " > div").removeClass("active");
                $(that.sliderWrapper + " > div").eq(that.currentPosition - 1).addClass('active');
                $(that.indicator + " .current").html(that.currentPosition);
                that.clearAnimation();
                
                if (that.currentPosition <= that.totalSlider) {
                    that.currentTime = that.customTime[that.currentPosition - 1];

                    if (that.currentPosition == that.totalSlider) {
                        that.currentPosition = 0;
                    }
                }

                //console.log(that.currentTime);
                
                that.lineAnimation();

                clearInterval(that.interval);
                if (typeof that.currentTime !== 'undefined' && !isNaN(that.currentTime))
                    timing();
                else {
                    console.error('Got undefined');
                }
            }, that.currentTime);
        } //timing
        
        if( this.totalSlider > 1 ){
            timing();
        }
    } //next function

} //newsSlider






function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}

var initReveal = function () {
    Reveal.initialize({
        history: false,
        width: "100%",
        height: "100%",
        margin: 0,
        minScale: 1,
        maxScale: 1,
        center: false,
        controls: false,
        progress: false,
        slideNumber: true,
        loop: true,
        autoSlide: 6000,
        autoSlideStoppable: true,
        slideNumber: 'c/t',
                dependencies: []
    });
}

function getJsonFromUrl() {
    var query = location.search.substr(1);
    var result = {};
    query.split("&").forEach(function (part) {
        var item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
}
var currentCategory = (getJsonFromUrl(window.location.href)['monitor']);

