+(function(){

    var ngApp = angular.module('RosenSlider', []);

    ngApp.constant( 'Const',{
        baseUrl      : ROSEN.baseurl,
        apiUrl       : ROSEN.baseurl + 'wp-json/rosen/',
        appPath      : ROSEN.template_url + '/assets/js/app/',
        templatePath : ROSEN.template_url + '/assets/js/app/templates/'
    });

    ngApp.controller('RosenSliderController', [ '$scope', 'DataServer', function ( $scope, DataServer ) {
            
        $scope.preloader = true;
        $scope.ready = false;

        var all = {
            success : function ( response ) {

                $scope.sliderItems = response.data.sliders;
                $scope.newsItems = response.data.news;
                $scope.noticeItems = response.data.notices;
                $scope.settings = response.data.settings;
                $scope.preloader = false;
                $scope.ready = true;
                jQuery('head').append('<link rel="stylesheet" href="' + $scope.settings.vc_style + '" type="text/css" />');

                setTimeout(function () {
                    try {
                        initReveal();
                    } catch (e) {
                        //console.log( e );
                    }

                    jQuery(".loader-overlay").hide();
                }, 100);
            },
            error : function ( error ) {
                console.error("Error while getting all slider");
                console.group();
                console.log( error );
                console.groupEnd();
            }
        }

        DataServer.getAll().then( all.success, all.error );

        $scope.getNotice = function (is_temp) {

            var notice = {
                success : function ( response ) {
                    if (typeof is_temp == 'undefined') {
                        $scope.noticeItems = response.data;
                    }
                    else {
                        $scope.tempNoticeItems = response.data;
                    }
                    $scope.preloader = false;
                },
                error : function ( error ) {
                        console.error("Error while getting noticer");
                        console.group();
                        console.log( error );
                        console.groupEnd();
                    }
            };

            DataServer.getNotice().then( notice.success, notice.error );
        }

        $scope.getSliders = function (is_temp) {

            var slider = {
                success: function ( response ) {
                    if (typeof is_temp == 'undefined') {
                        $scope.sliderItems = response.data;
                    }else {
                        $scope.tempSliderItems = response.data;
                    }

                    $scope.preloader = false;
                },
                error : function ( response ) {
                    console.error("Error while getting sliders");
                    console.group();
                    console.log( response );
                    console.groupEnd();
                }
            };

            DataServer.getSlider().then( slider.success, slider.error );
        }

        Reveal.addEventListener('slidechanged', function (event) {
            var totalSlider = Reveal.getTotalSlides();
            var currentIndex = event.indexh;
            
            if(jQuery(event.previousSlide).find('.animated').length > 0 ){
                jQuery(event.previousSlide).find('.animated').addClass('wpb_animate_when_almost_visible').removeClass('animated');
            }
            
            if(jQuery(event.currentSlide).find('.wpb_animate_when_almost_visible').length > 0 ){
                jQuery(event.currentSlide).find('.wpb_animate_when_almost_visible').removeClass('wpb_animate_when_almost_visible').addClass('animated');
            }
            
          

            if (totalSlider == currentIndex + 1) {
                $scope.getSliders(true);
            }

            if (0 == currentIndex) {
                // debugger;
                $scope.sliderItems = $scope.tempSliderItems;
                $scope.$apply();

                setTimeout(function () {
                    Reveal.slide(0);
                }, 100);

            }
        });
    }]);

    ngApp.factory( 'DataServer', [ '$http','Const', function( $http, Const ){

        var data = {};
        data.getNews = function(){

            var param = {
                method: 'GET',
                url   : Const.apiUrl + 'news'
            };

            return $http( param ).then( function( response ){
                return response.data;
            });
        };

        data.getAll = function(){
            var param = {
                method: 'GET',
                url   : Const.apiUrl + 'slider/all?cat=' + currentCategory
            };

            return $http( param ).then( function( response ){
                return response.data;
            });
        };

        data.getNotice = function(){
            var param = {
                method: 'GET',
                url   : Const.apiUrl + 'notice'
            };

            return $http( param ).then( function( response ){
                return response.data;
            });
        };

        data.getSlider = function(){
            var param = {
                method: 'GET',
                url   : Const.apiUrl + 'sliders/?cat='+currentCategory
            };

            return $http( param ).then( function( response ){
                return response.data;
            });
        };

        return data;
    }]);

    ngApp.directive('ngNoticeSlider', [ '$interval','DataServer',function( $interval, DataServer ) {

        var linkFn =  function (scope, element, attrs) {

            scope.tempNoticeItems = null;
            scope.isFetchingNotice = false;

            var param = {
                sliderWrapper: '#message', 
                time: 10000, 
                indicator: "#hello"
            };

            var notice = {
                success: function( response ){
                    scope.tempNoticeItems = response.data;
                    scope.preloader = false;
                    scope.isFetchingNotice = false;
                },
                error  : function( error ){
                    console.error("Error while getting Notice");
                    console.group();
                    console.groupEnd();
                    scope.isFetchingNotice = false;
                }
            };

            var slider = {
                param : {
                    sliderWrapper: '#new-slider',
                    time: 2000,
                    indicator: '#indicator',
                    timeLine:'#time'
                },
                change : function () {

                    if ( (SliderInstance.totalSlider - 1 == SliderInstance.currentPosition) && !scope.isFetchingNotice ) {
                        
                        scope.tempNoticeItems = null;
                        scope.isFetchingNotice = true;
                        scope.$apply();
                        DataServer.getNotice().then( notice.success, notice.error );
                    }
                    
                    if ( SliderInstance.currentPosition == 1 && scope.tempNoticeItems !== null ) {

                        scope.noticeItems = scope.tempNoticeItems;
                        scope.tempNoticeItems = null;
                        scope.$apply();
                        SliderInstance.init();
                    }
                }
            }

            var SliderInstance = new newsSlider( param );

            element.ready(function () {
                SliderInstance.init();
                jQuery( SliderInstance.sliderWrapper ).on( "sliderChanged", slider.change );
            });
        };

        return {
            restrict    : 'E',
            replace     : true,
            transclude  : true,
            scope       : { noticeItems: '=items' },
            templateUrl : ROSEN.template_url + '/assets/js/app/templates/noticeslider-template.html',
            link        : linkFn
        };
    }]);

    ngApp.directive('newsSlider', function ( Const, DataServer ) {

        var link = function (scope, element, attrs) {
                
            scope.tempNewsItems = null;
            scope.isFetchingNews = false;

            var news = {
                success : function( response ){
                    
                    scope.tempNewsItems = response.data;
                    scope.preloader = false;
                    scope.isFetchingNews = false;
                },
                error   : function( error ){
                    console.error("Error while getting news");
                    console.group();
                    console.groupEnd();
                    scope.isFetchingNews = false;
                }
            }

            var slider = {
                param : {
                    sliderWrapper: '#new-slider',
                    time: 2000,
                    indicator: '#indicator',
                    timeLine:'#time'
                },
                change : function (e) {
                    
                    if ( (slideInstance.totalSlider - 1 == slideInstance.currentPosition) && !scope.isFetchingNews ) {
                        scope.tempNewsItems = null;
                        scope.isFetchingNews = true;
                        DataServer.getNews().then( news.success, news.error );
                        scope.$apply();
                    }
                    
                    if (slideInstance.currentPosition == 1 && scope.tempNewsItems !== null ) {

                        scope.newsItems = scope.tempNewsItems;
                        scope.tempNewsItems = null;
                        scope.$apply();
                        slideInstance.init();
                    }
                }
            }

            var slideInstance = new newsSlider( slider.param );

            element.ready( function() {
                slideInstance.init();
                jQuery(slideInstance.sliderWrapper).on( "sliderChanged", slider.change );
            });
        };

        return{
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: { newsItems: '=items' },
            templateUrl: Const.templatePath + 'newsslider-template.html',
            link: link
        };
    });

    ngApp.directive('ngLogoAnimate', function () {
        return {
            restrict: 'EA',
            priority: 10,
            link: function (scope, element, attrs) {
                scope.$watch(attrs.ngBindHtml, function (newvalue) {
                    new logoAni().init();
                });

            }
        }
    });

    ngApp.filter("trustedHtml", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);

    //slider ready
    Reveal.addEventListener( 'ready', function( event ) {
        if(jQuery(event.currentSlide).find('.wpb_animate_when_almost_visible').length > 0 ){
            jQuery('.wpb_animate_when_almost_visible').removeClass('wpb_animate_when_almost_visible').addClass('animated');
        }
    });
    
    Reveal.addEventListener('fullWidthSlider', function () {
        jQuery('.side-news').hide();
        jQuery('body').addClass('fullwidth');
    }, true);

    Reveal.addEventListener('NoWidthSlider', function () {
        jQuery('.side-news').show();
        jQuery('body').removeClass('fullwidth');
    }, true);
    
})();