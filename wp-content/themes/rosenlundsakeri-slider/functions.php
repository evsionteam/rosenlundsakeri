<?php
if ( ! function_exists( 'rosenlundsakeri_slider_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rosenlundsakeri_slider_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Rosenlundsakeri, use a find and replace
	 * to change 'rosenlundsakeri' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'rosenlundsakeri', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );



	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'rosenlundsakeri' ),
		'mobile' => esc_html__( 'Mobile', 'rosenlundsakeri' ),
		'footer' => esc_html__( 'Footer (SiteMap)', 'rosenlundsakeri' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'rosenlundsakeri_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );


	$author = get_role('editor');
	add_role( 'slider_editor', __('Slider Editor', 'rosenlundsakeri'), 
		$author->capabilities

	 );
}
endif;
add_action( 'after_setup_theme', 'rosenlundsakeri_slider_setup' );

if( isset( $_POST['rosen_news_title'] ) ){
	update_option( 'rosen_news_title', $_POST['rosen_news_title'] );
}

if( isset($_POST['rosen_slider_logo'] ) ) {
	update_option( 'rosen_slider_logo', htmlentities(stripslashes( $_POST['rosen_slider_logo']  ))) ;	
}

/**
 * add Sub menu for sliders settings
 */

function rosen_remove_menus(){
  	global $current_user;
	$user_roles = $current_user->roles;

	if( in_array('administrator', $user_roles) ){
		add_submenu_page( 'edit.php?post_type=slider', 
			__("Slider inställningar", 'rosenlundsakeri'),
			__("Slider inställningar", 'rosenlundsakeri'),
			'manage_options', 
			'slider-settings', 
			'rosen_slider_settings_page_callback' 
		);	
	}
  	

  $current_user = wp_get_current_user();
   if( in_array('slider_editor', $current_user->roles) ) {
	  	remove_menu_page( 'tools.php' );                  //Tools 		
	  	remove_menu_page( 'edit.php?post_type=page' );    //Pages 		
	  	remove_menu_page( 'options-general.php' );        //Settings
	  	remove_menu_page( 'index.php' );                  //Dashboard
	  	remove_menu_page( 'edit-comments.php' );          //Comments
	  	remove_menu_page( 'jetpack' );                    //Jetpack* 

		add_action( 'admin_enqueue_scripts', 'rosen_slider_editor_styles' );	  	
   }
}
add_action( 'admin_menu', 'rosen_remove_menus' );


function rosen_slider_editor_styles() {
	$custom_css = "
            li#wp-admin-bar-my-sites,
            li.toplevel_page_vc-welcome {
			    display: none;
			}
			";
    wp_add_inline_style( 'nav-menus', $custom_css );
}

function rosen_slider_settings_page_callback(){

	// Check that the user is allowed to update options
	if (!current_user_can('manage_options')) {
	    wp_die('You do not have sufficient permissions to access this page.');
	}

	get_template_part('inc/slider', 'settings' );
}


# Rename menu labels
/*add_action( 'admin_menu', 'rosen_slider_change_post_menu_label' );
function rosen_slider_change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = __( 'News', 'rosenlundsakeri' );
	$submenu['edit.php'][5][0] = __( 'News', 'rosenlundsakeri' );
	$submenu['edit.php'][10][0] = __( 'Add new news', 'rosenlundsakeri' );
	$submenu['edit.php'][16][0] = __( 'Tags', 'rosenlundsakeri' );
	echo '';
}*/

/**
 * Add row to slider taxonomy
 */
add_filter('slider-category_row_actions', 'rosen_slider_tax_action_row' , 10, 2);
function rosen_slider_tax_action_row($actions, $tag ){
   $url = home_url("/" ). "?monitor=". $tag->slug;
   $actions['view'] = '<a target="_blank" class="v" href="'. $url .'">'.__("Preview", "rosenlundsakeri").'</a>';
   return $actions;
}


add_action( 'wp_enqueue_scripts','rosen_load_monitor_script' );
function rosen_load_monitor_script(){
	
	wp_register_style( 'reveal', get_stylesheet_directory_uri().'/assets/css/vendor/reveal.css', array(), null );
	wp_register_style( 'animation', get_stylesheet_directory_uri().'/assets/css/vendor/animate.css', array(), null );
        wp_enqueue_style( 'main', get_stylesheet_directory_uri().'/assets/css/main.css', array('reveal','animation'), null );

	wp_enqueue_script( 'angular', get_stylesheet_directory_uri().'/assets/js/vendor/angular.js', array(), null, false );
	
	wp_deregister_script('jquery');

	wp_register_script( 'rosen-jquery', get_stylesheet_directory_uri().'/assets/js/vendor/jquery-3.1.1.min.js', array(), null, true );
	wp_register_script( 'reveal', get_stylesheet_directory_uri().'/assets/js/vendor/reveal.js', array('rosen-jquery'), null, true );
	wp_register_script( 'tweenmax', get_stylesheet_directory_uri().'/assets/js/vendor/TweenMax.min.js', array('reveal'), null, true );

	wp_enqueue_script( 'main', get_stylesheet_directory_uri().'/assets/js/script.js', array('tweenmax'), null, true );
	wp_enqueue_script( 'ng-app', get_stylesheet_directory_uri().'/assets/js/app/app.js', array('main'), null, true );
	wp_localize_script( 'main', 'ROSEN', array( 
            'baseurl' => site_url().'/' ,
            'template_url' => get_stylesheet_directory_uri()    
            )
        );
	$webfont = "WebFontConfig = {
		google: { families: ['Raleway:400,500i,600i,700:latin']}
	};
 	(function() {
		var wf = document.createElement('script');
		wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})(); ";

	wp_add_inline_script('main', $webfont );
}


add_action( 'admin_enqueue_scripts','rosen_load_monitor_admin_script' );
function rosen_load_monitor_admin_script(){
	wp_enqueue_style( 'admin', get_stylesheet_directory_uri().'/assets/css/admin/admin.css', array(), null );
}

/* For adding Font options in editor */
function rosen_mce_buttons( $buttons ) {
    array_unshift( $buttons, 'fontselect' );
	array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
	return $buttons;
}
add_filter( 'mce_buttons_2', 'rosen_mce_buttons' );

// Customize mce editor font sizes
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
    function wpex_mce_text_sizes( $initArray ){
        $initArray['fontsize_formats'] = "8px 12px 16px 20px 24px 28px 32px 36px 40px 44px 48px 52px 56px 60px 64px 68px 72px 76px 80px 84px 88px 92px 96px 100px 104px 108px 112px 116px 120px";
        return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );

show_admin_bar( false );
//add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

add_action( 'add_meta_boxes', 'change_tag_meta_box', 0 );
function change_tag_meta_box() {
    global $wp_meta_boxes;
    /*echo '<pre>';
    print_r($wp_meta_boxes);
    exit;*/
    $wp_meta_boxes['slider']['side']['core']['slider-categorydiv']['title'] = 'Vart ska vi publicera?';
}

/*remove default post for slider editor user role*/
function rosen_remove_post_menu_for_user_role() {
    if(is_user_logged_in()){
        $current_user = wp_get_current_user();
        if ( 0 != $current_user->ID ) {
            if(is_array($current_user->roles)){
                if(in_array('slider_editor',$current_user->roles)){
                    remove_menu_page( 'edit.php' );
                }
            }
        }
    }
}
add_action( 'admin_menu', 'rosen_remove_post_menu_for_user_role' );

require __DIR__ . '/inc/vc-element/init.php';
require __DIR__ . '/inc/facebook-post/loader.php';