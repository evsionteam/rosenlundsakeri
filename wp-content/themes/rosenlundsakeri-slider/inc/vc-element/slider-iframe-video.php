<?php
add_action( 'vc_before_init', 'rosen_iframe_video_integrateWithVC' );
function rosen_iframe_video_integrateWithVC(){
    vc_map( array(
        "name"                    => __("Youtube Video", "rosenlundsakeri"),
        "base"                    => "rosen_iframe_video_cb",
        "description"             => __("Enter Youtube embed Link","rosenlundsakeri"),
        "category"                => __('Content', 'rosenlundsakeri'),
        "params"                  => array(
            array(
                "type"        => "textarea",
                "holder" 	  => "text",
                "heading"     => __("Youtube Code", "rosenlundsakeri"),
                "param_name"  => "content",
            )
        ),
    ) );
}
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_rosen_iframe_video_cb extends WPBakeryShortCode {

        protected function content( $atts, $content = null ) {

            $values = shortcode_atts( array(
                'iframe_content'  =>  ''
            ), $atts ) ;
            ob_start();
            ?>
            <div class="rosen-slider-element">
                <div class="rosen-iframe-content">
                    <iframe data-src="https://youtube.com/embed/<?php echo $content; ?>?autoplay=1&loop=-1&controls=0&showinfo=0&autohide=1"></iframe>
                    <!-- <?php //echo wp_oembed_get( $content ); ?> -->
                </div>
            </div>
            <?php
            $output = ob_get_clean();
            ob_flush();
            return $output;
        }
    }
}