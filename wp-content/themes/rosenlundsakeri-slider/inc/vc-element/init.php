<?php
 //require get_template_directory(). "/inc/vc-element/vc-element.php";
 require get_template_directory(). "/inc/vc-element/slider-image.php";
 require get_template_directory(). "/inc/vc-element/slider-html.php";
 require get_template_directory(). "/inc/vc-element/slider-iframe.php";
 require get_template_directory(). "/inc/vc-element/slider-video.php";
 require get_template_directory(). "/inc/vc-element/slider-iframe-video.php";
 require get_template_directory(). "/inc/vc-element/slider-rss.php";


add_action( 'vc_before_init', 'rosen_remove_unwanted_elements',999 );
function rosen_remove_unwanted_elements(){
	$elems = array( 'vc_icon', 'vc_separator', 'vc_text_separator', 'vc_message', 'vc_facebook', 'vc_tweetmeme', 'vc_googleplus', 'vc_pinterest', 'vc_toggle', 'vc_gallery', 'vc_images_carousel', 'vc_tta_tabs', 'vc_tta_tour', 'vc_tta_accordion', 'vc_tta_pageable', 'vc_tta_section', 'vc_custom_heading', 'vc_btn', 'vc_cta', 'vc_widget_sidebar', 'vc_posts_slider', 'vc_video', 'vc_gmaps', 'vc_raw_html', 'vc_raw_js', 'vc_flickr', 'vc_progress_bar', 'vc_pie', 'vc_round_chart', 'vc_line_chart', 'vc_empty_space', 'vc_basic_grid', 'vc_media_grid', 'vc_masonry_grid', 'vc_masonry_media_grid', 'vc_tabs', 'vc_tour', 'vc_accordion', 'vc_wp_search', 'vc_wp_meta', 'vc_wp_recentcomments', 'vc_wp_calendar', 'vc_wp_pages', 'vc_wp_tagcloud', 'vc_wp_custommenu', 'vc_wp_text', 'vc_wp_posts', 'vc_wp_categories', 'vc_wp_archives', 'vc_wp_rss');

	array_map(function($elem){
		vc_remove_element( $elem );
	},$elems);
}