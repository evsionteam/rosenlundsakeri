<?php
add_action( 'vc_before_init', 'rosen_video_integrateWithVC' );
function rosen_video_integrateWithVC(){
    vc_map( array(
        "name"                    => __("Uploaded Video", "rosenlundsakeri"),
        "base"                    => "rosen_video_cb",
        "description"             => __("Uploaded Video Link","rosenlundsakeri"),
        "category"                => __('Content', 'rosenlundsakeri'),
        "params"                  => array(
            array(
                "type"        => "textarea",
                "holder" => "iframe",
                "heading"     => __("Enter Video url", "rosenlundsakeri"),
                "param_name"  => "video_url",
            )
        ),
    ) );
}
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_rosen_video_cb extends WPBakeryShortCode {

        protected function content( $atts, $content = null ) {

            $values = shortcode_atts( array(
                'video_url'  =>  ''
            ), $atts ) ;
            ob_start();
            ?>
            <?php if(!empty($values['video_url'])):?>
                <div class="rosen-slider-element">
                    <div class="rosen-iframe-content">
                        <video width="100%" autoplay loop>
                            <source src="<?php echo $values['video_url'];?>" type="video/mp4">
                        </video>
                    </div>
                </div>
            <?php endif;?>
            <?php
            $output = ob_get_clean();
            ob_flush();
            return $output;
        }
    }
}