<?php
add_action( 'vc_before_init', 'rosen_iframe_slider_call_to_action_integrateWithVC' );
function rosen_iframe_slider_call_to_action_integrateWithVC(){
    vc_map( array(
        "name"                    => __("Iframe Url", "rosenlundsakeri"),
        "base"                    => "rosen_iframe_slider_call_to_action",
        "description"             => __("Iframe content","rosenlundsakeri"),
        "category"                => __('Content', 'rosenlundsakeri'),
        "params"                  => array(
            array(
                "type"        => "textarea",
                "holder" 	  => "iframe",
                "heading"     => __("Iframe content", "rosenlundsakeri"),
                "param_name"  => "content",
            )
        ),
    ) );
}
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_rosen_iframe_slider_call_to_action extends WPBakeryShortCode {

        protected function content( $atts, $content = null ) {
            
            $values = shortcode_atts( array(
                'iframe_content'  =>  ''
            ), $atts ) ;
            ob_start();
            ?>
            <div class="rosen-slider-element">
                <div class="rosen-iframe-content">
                    <iframe data-src="<?php echo $content; ?>"></iframe>
                    <!-- <?php //echo wp_oembed_get( $content ); ?> -->
                </div>
            </div>
            <?php
            $output = ob_get_clean();
            ob_flush();
            return $output;
        }
    }
}