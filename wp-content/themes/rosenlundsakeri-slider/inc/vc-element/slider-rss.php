<?php
add_action( 'vc_before_init', 'rosen_rss_feed_integrateWithVC' );
function rosen_rss_feed_integrateWithVC(){
    vc_map( array(
        "name"                    => __("Rss Feed", "rosenlundsakeri"),
        "base"                    => "rosen_rss_feed",
        "description"             => __("Display Rss Feeds.","rosenlundsakeri"),
        "category"                => __('Content', 'rosenlundsakeri'),
        "params"                  => array(
            array(
                "type" => "textfield",
                "heading" => __( "Enter Feed Url: ", "rosenlundsakeri" ),
                "holder" => "div",
                "param_name" => "feed_url",
            ),
            array(
                "type" => "attach_image",
                "heading" => __( "Enter Feed Logo: ", "rosenlundsakeri" ),
                "holder" => "img",
                "param_name" => "feed_logo",
            ),
            array(
                "type" => "dropdown",
                /*"holder" => "div",*/
                "heading" => __("Number of feeds", "rosenlundsakeri"),
                "param_name" => "no_of_feeds",
                'value' => array(
                    __('1', 'rosenlundsakeri') => 1,
                    __('2', 'rosenlundsakeri') => 2,
                    __('3', 'rosenlundsakeri') => 3,
                    __('4', 'rosenlundsakeri') => 4,
                    __('5', 'rosenlundsakeri') => 5,
                ),
            ),
        ),
    ) );
}
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_rosen_rss_feed extends WPBakeryShortCode {
        protected function content( $atts, $content = null ) {
            $values = shortcode_atts( array(
                'feed_url' => '',
                'feed_logo' => '',
                'no_of_feeds'  => 3
            ), $atts ) ;
            ob_start();
            ?>
            <?php if(!empty($values['feed_url'])): ?>
                <div class="rosen-rss-feeds">
                    <?php
                    include_once( ABSPATH . WPINC . '/feed.php' );
                        $rss = fetch_feed( $values['feed_url'] );
                        $max_items = 0;
                        if ( ! is_wp_error( $rss ) ) :
                            $max_items = $rss->get_item_quantity( $values['no_of_feeds'] );
                            $rss_items = $rss->get_items( 0, $max_items );
                        endif;
                        ?>

                        <?php if ( $max_items == 0 ) : ?>
                            <div class="no-items"><?php _e( 'No items found', 'rosenlundsakeri' ); ?></div>
                        <?php else : ?>
                        <?php if(!empty($values['feed_logo'])):?>
                            <?php $logo = wp_get_attachment_image_src($values["feed_logo"], "thumbnail"); ?>
                                <div class="rss-logo">
                                    <img src="<?php echo $logo[0];?>">
                                </div>
                        <?php endif;?>
                            <?php foreach ( $rss_items as $item ) : ?>
                                <div class="rss-wrapper">
                                    <div class="title">
                                        <h2><?php echo esc_html( $item->get_title() ); ?></h2>
                                    </div>
                                    <div class="description">
                                        <?php echo html_entity_decode($item->get_description()); ?>
                                    </div>
                                    <div class="published-date">
                                        <?php echo $item->get_date('Y-m-d'); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            <?php endif;?>

            <?php
            $output = ob_get_clean();
            ob_flush();
            return $output;
        }
    }
}