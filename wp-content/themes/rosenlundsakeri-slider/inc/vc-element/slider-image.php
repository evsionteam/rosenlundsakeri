<?php
add_action( 'vc_before_init', 'rosen_image_slider_call_to_action_integrateWithVC' );
function rosen_image_slider_call_to_action_integrateWithVC(){
    vc_map( array(
        "name"                    => __("Full Image", "rosenlundsakeri"),
        "base"                    => "rosen_image_slider_call_to_action",
        "description"             => __("Add Full Image for slider.","rosenlundsakeri"),
        "category"                => __('Content', 'rosenlundsakeri'),
        "params"                  => array(
            array(
                "type"        => "attach_image",
                "holder" 	  => "img",
                "heading"     => __("Select image for slider", "rosenlundsakeri"),
                "param_name"  => "slider_image",
            )
        ),
    ) );
}
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_rosen_image_slider_call_to_action extends WPBakeryShortCode {

        protected function content( $atts, $content = null ) {
            $values = shortcode_atts( array(
                'slider_image'  =>  ''
            ), $atts ) ;
            ob_start();
            ?>
            <?php if(!empty($values['slider_image'])):?>
                <?php $img = wp_get_attachment_image_src($values["slider_image"], "full"); ?>
                <div class="rosen-slider-element full-width-image-slider" style="background-image: url('<?php echo $img[0];?>')"></div>
            <?php endif;?>
            <?php
            $output = ob_get_clean();
            ob_flush();
            return $output;
        }
    }
}