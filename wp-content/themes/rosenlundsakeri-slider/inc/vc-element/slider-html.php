<?php
add_action( 'vc_before_init', 'rosen_text_img_slider_call_to_action_integrateWithVC' );
function rosen_text_img_slider_call_to_action_integrateWithVC(){
    vc_map( array(
        "name"                    => __("Text / html", "rosenlundsakeri"),
        "base"                    => "rosen_text_html_slider_call_to_action",
        "description"             => __("Add Slider Content.","rosenlundsakeri"),
        "category"                => __('Content', 'rosenlundsakeri'),
        "params"                  => array(
            array(
                "type" => "textarea_html",
                "holder" => "div",
                "class" => "",
                "heading" => __( "Content", "rosenlundsakeri" ),
                "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "rosenlundsakeri" ),
                "description" => __( "Enter your content.", "rosenlundsakeri" )
         )
        ),
    ) );
}
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_rosen_text_html_slider_call_to_action extends WPBakeryShortCode {

        protected function content( $atts, $content = null ) {
            $values = shortcode_atts( array(
                'content'  =>  ''
            ), $atts ) ;
            
            ob_start();
            ?>
            <div class="rosen-text-html-element">
            	<div class="rosen-text-html-content">
                    <?php echo esc_html( $content ); ?>
                </div>
            </div>
            <?php
            $output = ob_get_clean();
            ob_flush();
            return $output;
        }
    }
}