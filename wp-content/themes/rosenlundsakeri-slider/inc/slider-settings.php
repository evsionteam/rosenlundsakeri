<?php
/**
 * Settings page html 
 * rosenlundsakeri
 */
?>
<h1><?php _e("Settings", "rosenlundsakeri"); ?></h1>

<div class="rosen-slider-settings">
	<form name="rosen_slider_settings" method="post">
		<table class="form-table">
			<tr class="logo-section">
				<th scope="row"><label><?php _e("News Title", "rosenlundsakeri"); ?></label></th>
				<td><input name="rosen_news_title" value="<?php echo get_option('rosen_news_title' );?>" /></td>
			</tr>
			<tr class="logo-section">
				<th><label><?php _e("Logo", "rosenlundsakeri"); ?></label></th>
				<td>
					<textarea cols="50" rows="10" name="rosen_slider_logo"><?php echo html_entity_decode( get_option('rosen_slider_logo' ) );?></textarea>		
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td >
					<button class="button button-primary"><?php _e("Save", 'rosenlundsakeri'); ?> </button>
				</td>
			</tr>
		</table>
	</form>
</div>