<?php
namespace RosenFacebookPost\Facebook;
session_start();

class ROSEN_FACEBOOK{

	public $fb;

	protected static $instance = null;

	public $callback_url;

	private $host;

	private $page_id;

	public function __construct(){
		$this->rosen_fb = get_option('rosen_fb');

		if( !$this->rosen_fb ){
			return;
		}

		$app_id = isset($this->rosen_fb['app_id'])?$this->rosen_fb['app_id']:'';
		$app_secret = isset($this->rosen_fb['app_secret'])?$this->rosen_fb['app_secret']:'';
		$fb_page_id = isset($this->rosen_fb['fb_page_id'])?$this->rosen_fb['fb_page_id']:'';

		if( !isset($app_id,$app_secret,$fb_page_id) ){
			return;
		}		

		add_action('admin_init', array($this,'grab_authorization_token') );
		add_action('admin_init', array($this,'grab_authorization_expiry_request') );

		$this->callback_url = admin_url('admin.php?page=rosen-facebook&authorized=true');
		$this->host = 'https://graph.facebook.com/';
		$this->page_id = $fb_page_id;

		$this->fb = new \Facebook\Facebook([
			'app_id' => $app_id,
		  	'app_secret' => $app_secret,
		  	'default_graph_version' => 'v2.5',
		]);
	}

	public function validate_token(){
		$token = get_option('rosen_fb_token');
		if(!$token){
			return false;
		}

		$response = wp_remote_get('https://graph.facebook.com/v2.7/me/permissions?access_token='.$token);
		if ( is_wp_error( $response ) ) {
			return false;
		}

		$body = json_decode(wp_remote_retrieve_body($response),true);
		if(isset($body['error'])){
			return false;
		}

		return $token;
	}

	public function get_login_url(){

		$helper = $this->fb->getRedirectLoginHelper();
		$permissions = ['manage_pages', 'publish_pages'];
		try {
		  $loginUrl = $helper->getLoginUrl($this->callback_url, $permissions);
		  return $loginUrl;
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			return new \WP_Error('graph-error',$e->getMessage());
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
		  return new \WP_Error('sdk-error',$e->getMessage());
		}
	}

	public function grab_authorization_token(){
		if( isset($_GET['authorized'], $_GET['code']) && ( isset($_GET['page']) && $_GET['page'] == 'rosen-facebook' ) ){
			
			$rosen_fb_token = get_option('rosen_fb_token');
			if($rosen_fb_token){
				return $rosen_fb_token;
			}

			$app_id = isset($this->rosen_fb['app_id'])?$this->rosen_fb['app_id']:'';
			$app_secret = isset($this->rosen_fb['app_secret'])?$this->rosen_fb['app_secret']:'';

			$token_url = $this->host."/oauth/access_token?"
						. "client_id=" . $app_id . "&redirect_uri=" . urlencode($this->callback_url)
						. "&client_secret=" . $app_secret . "&code=" . $_GET['code'];


			$response = file_get_contents($token_url);
			$params = null;
			parse_str($response, $params);
			$access_token = $params['access_token'];

			$oAuth2Client = $this->fb->getOAuth2Client();
			$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($access_token);

			update_option('rosen_fb_token', $longLivedAccessToken );
		}
	}

	public function generate_page_token($authToken){

		$this->fb->setDefaultAccessToken($authToken);
		$response = wp_remote_get($this->host.$this->page_id.'?fields=access_token&access_token='.$authToken);
		if ( is_wp_error( $response ) ) {
			return $response;
		}

		$body = wp_remote_retrieve_body($response);

		if ( is_wp_error( $body ) ) {
			return $body;
		}

		$body = json_decode($body,true);
		return isset($body['access_token'])?$body['access_token']:'';
	}

	public function make_a_post($post,$token){
		$app_id = isset($this->rosen_fb['app_id'])?$this->rosen_fb['app_id']:'';
		$app_secret = isset($this->rosen_fb['app_secret'])?$this->rosen_fb['app_secret']:'';


		if( $this->rosen_fb['share_message'] ){
			$post = array_merge( $post, array('message' => $this->rosen_fb['share_message'] ));
		}

		if( !isset($post['picture']) || empty($post['picture']) ){
			if( $this->rosen_fb['default_placeholder'] ){
				$img = $this->rosen_fb['default_placeholder'];
			}else{
				$img = 'http://placehold.it/600/000000/ffffff?text=No+Image';
			}

			$post = array_merge( $post, array('picture' => $img ));
		}

		$session = new \Facebook\FacebookApp($app_id,$app_secret);
		$request = new \Facebook\FacebookRequest(
			$session,
			$token,
			'POST',
			$this->page_id.'/feed',
			$post
		);

		try {
			$response = $this->fb->getClient()->sendRequest($request);
		}catch(\Facebook\Exceptions\FacebookResponseException $e) {
			return new \WP_Error('graph-error',$e->getMessage());
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
		  return new \WP_Error('sdk-error',$e->getMessage());
		}
	}

	public function grab_authorization_expiry_request(){
		if( isset($_GET['expire']) && ( isset($_GET['page']) && $_GET['page'] == 'rosen-facebook' ) ){
			$this->expire();
		}
	}

	private function expire(){
		$token = get_option('rosen_fb_token');
		if(!$token){
			return false;
		}

		$response = wp_remote_request(
			'https://graph.facebook.com/v2.7/me/permissions?access_token='.$token,
			array(
				'method'=>'DELETE'
			)
		);

		if ( is_wp_error( $response ) ) {
			print_r($response);
			die();
		}


		$body = json_encode(wp_remote_retrieve_body($response));

		if ( is_wp_error( $body ) ) {
			print_r($body);
			die();
		}

		if(!isset($body['error'])){
			delete_option('rosen_fb_token');
		}

		wp_redirect( admin_url('admin.php?page=rosen-facebook') );
		exit();
		
	}

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}