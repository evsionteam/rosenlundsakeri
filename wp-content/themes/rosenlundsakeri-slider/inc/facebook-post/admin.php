<?php
namespace RosenFacebookPost\Admin;

use RosenFacebookPost\Facebook;

class ROSEN_FB_SETTINGS{
	
	private $rosen_fb;

	private $fb;

	protected static $instance = null;

	public function __construct(){
		$this->fb 				= Facebook\ROSEN_FACEBOOK::get_instance();
		$this->rosen_fb 		= get_option('rosen_fb');

		add_action('admin_menu', array($this,'register_menu'));
		add_action('admin_init', array($this,'register_menu_settings') );
		
	}

	public function register_menu() {
		add_menu_page('Rosen\'s Facebook', 'Rosen\'s Facebook', 'manage_options', 'rosen-facebook', array($this, 'get_page')  );
	}

	public function get_page(){ 

		$app_id 				= isset($this->rosen_fb['app_id'])?$this->rosen_fb['app_id']:'';
		$app_secret 			= isset($this->rosen_fb['app_secret'])?$this->rosen_fb['app_secret']:'';		
		$share_message 			= isset($this->rosen_fb['share_message'])?$this->rosen_fb['share_message']:'';		
		$default_placeholder 	= isset($this->rosen_fb['default_placeholder'])?$this->rosen_fb['default_placeholder']:'';		
		$fb_page_id 			= isset($this->rosen_fb['fb_page_id'])?$this->rosen_fb['fb_page_id']:'';		
?>
		<div class="wrap">
			<h1>Rosen's Facebook Settings</h1>		
			<form method="post" action="options.php">
				<?php settings_fields( 'rosen_fb_group' ); ?>
    			<?php do_settings_sections( 'rosen_fb_group' ); ?>
    			
    			<table class="form-table">
			        <tr valign="top">
			        	<th scope="row"><?php _e('App ID', 'rosenlundsakeri'); ?></th>
			        	<td><input type="text" name="rosen_fb[app_id]" class="regular-text" value="<?php echo $app_id; ?>" /></td>
			        </tr>
			         
			        <tr valign="top">
			        	<th scope="row"><?php _e('App Secret', 'rosenlundsakeri'); ?></th>
			        	<td><input type="text" name="rosen_fb[app_secret]" class="regular-text" value="<?php echo $app_secret; ?>" /></td>
			        </tr>

			        <tr valign="top">
			        	<th scope="row"><?php _e('Share Message', 'rosenlundsakeri'); ?></th>
			        	<td><textarea name="rosen_fb[share_message]" rows="6" class="regular-text" ><?php echo $share_message; ?></textarea></td>
			        </tr>

			        <tr valign="top">
			        	<th scope="row"><?php _e('Default Image Placeholder', 'rosenlundsakeri'); ?></th>
			        	<td><input type="text" name="rosen_fb[default_placeholder]" class="regular-text" value="<?php echo $default_placeholder; ?>" /></td>
			        </tr>

			         <tr valign="top">
			        	<th scope="row"><?php _e('Facebook Page ID', 'rosenlundsakeri'); ?></th>
			        	<td><input type="text" name="rosen_fb[fb_page_id]" class="regular-text" value="<?php echo $fb_page_id; ?>" /></td>
			        </tr>

			    </table>

			    <?php if( !$this->fb->validate_token() ): ?>
			    	<a href="<?php echo $this->fb->get_login_url(); ?>" class="button" ><?php _e('Authorize', 'rosenlundsakeri'); ?></a>
				<?php else: ?>
					<a href="<?php echo admin_url('admin.php?page=rosen-facebook&expire=true'); ?>" class="button" ><?php _e('Revoke', 'rosenlundsakeri'); ?></a>
				<?php endif; ?>
			    <?php submit_button(); ?>
			</form>
		</div>


	<?php
	
	}

	public function register_menu_settings(){
		register_setting( 'rosen_fb_group', 'rosen_fb' );
	}

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


}
