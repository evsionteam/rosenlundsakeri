<?php
	
	namespace RosenFacebookPost;
	
	use RosenFacebookPost\Facebook;
	use RosenFacebookPost\Facebook\Post;
	use RosenFacebookPost\Admin;


	require_once __DIR__ . '/sdk/autoload.php';
	require_once __DIR__ . '/facebook.php';
	require_once __DIR__ . '/post.php';
	require_once __DIR__ . '/admin.php';
	
	Admin\ROSEN_FB_SETTINGS::get_instance();
	Facebook\ROSEN_FACEBOOK::get_instance();
	Post\ROSEN_FACEBOOK_POST::get_instance();

	/*

		https://developers.facebook.com/docs/javascript/howto/jquery/v2.8
		https://developers.facebook.com/docs/pages
		https://developers.facebook.com/docs/pages/getting-started
		https://developers.facebook.com/quickstarts/621984488005007/?platform=web
		https://developers.facebook.com/docs/php/gettingstarted

	*/