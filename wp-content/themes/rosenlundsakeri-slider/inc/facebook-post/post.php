<?php

namespace RosenFacebookPost\Facebook\Post;

use RosenFacebookPost\Facebook;

class ROSEN_FACEBOOK_POST{

	protected static $instance = null;

	private $fb;

	public function __construct(){
		add_action('admin_head',array($this,'publish_post') );
		add_action( 'post_submitbox_misc_actions', array($this, 'publish_to_facebook_meta') );
		
		add_action( 'add_meta_boxes', array($this,'add_post_meta') );

		add_action( 'save_post', array($this,'save_info_box_for_social_media'), 10, 2 );

		add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_js'));
		add_action('admin_footer', array($this,'load_admin_scripts') );

		$this->fb = Facebook\ROSEN_FACEBOOK::get_instance();
	}

	public function enqueue_admin_js(){
		wp_enqueue_media();
	}

	public function load_admin_scripts(){
		$output = "
		<script>
			function renderMediaUploader() {		 
			    var file_frame, image_data;
			    if ( undefined !== file_frame ) {
			        file_frame.open();
			        return; 
			    }
			 
			    file_frame = wp.media.frames.file_frame = wp.media({
			        frame:    'post',
			        state:    'insert',
			        multiple: false
			    });

			    file_frame.on( 'insert', function(i) {
			    	console.log('Tests');
			    	var attachment = file_frame.state().get('selection').first().toJSON();
			    	jQuery('#fb-img-url').val(attachment.url);
			    });
			 
			    file_frame.open();
			}
			 
			(function() {
			   	jQuery( '#fb-upload-media' ).on( 'click', function( evt ) {
		            evt.preventDefault();
		            renderMediaUploader(); 
		        });


			})();
		</script>
	";

		echo $output;
	}

	public function add_post_meta(){
		
		add_meta_box( 'social-info', __( 'Social Information', 'rosenlundsakeri' ),
			array($this,'info_box_for_social_media'), 
			'slider', 
			'advanced',
			'default'
		);

		add_meta_box( 'social-info', __( 'Social Information', 'rosenlundsakeri' ),
			array($this,'info_box_for_social_media'), 
			'news', 
			'advanced',
			'default'
		);
	}

	public function publish_post(){
		global $post;

		if(!$post){
			return;
		}

		$post_id = $post->ID;
		
		if ( in_array( $post->post_type, array('news','slider') ) && ( isset($_GET['publish']) && $_GET['publish'] == 'facebook' ) ){
			$authToken = get_option('rosen_fb_token');
			if($authToken){
				$pageToken = $this->fb->generate_page_token($authToken);
				
				$rosen_fb_title 	= get_post_meta($post_id, 'rosen_fb_title', true);
				$rosen_fb_content 	= get_post_meta($post_id, 'rosen_fb_content', true);
				$rosen_fb_img 		= get_post_meta($post_id, 'rosen_fb_img', true);

				$name 				= empty($rosen_fb_title)?$post->post_title:$rosen_fb_title;
				$description 		= empty($rosen_fb_content)?do_shortcode($post->post_content):$rosen_fb_content;

				$params = array(
					'description'	=> $description,
					'name'			=> $name,
					'link'			=> site_url()
				);

				if($rosen_fb_img){
					$params['picture'] = $rosen_fb_img;
				}else if(has_post_thumbnail($post_id)){
					$params['picture'] = get_the_post_thumbnail( $post_id, 'full' );
				}
				
				$response = $this->fb->make_a_post($params,$pageToken);
				if(is_wp_error($response)){
					print_r($response);
					die();
				}
			}
		}
	}

	public function info_box_for_social_media($post){
	    if( isset($post) &&  ($post->post_type == 'slider' || $post->post_type == 'news') ){
	    	wp_nonce_field( 'social_info_nonce_action', 'social_info_nonce' );

	    	$fb_title 		= get_post_meta($post->ID, 'rosen_fb_title', true);
			$fb_content 	= get_post_meta($post->ID, 'rosen_fb_content', true);
			$fb_img 		= get_post_meta($post->ID, 'rosen_fb_img', true);

			$output = '
				<table class="form-table">
					<tbody>
						<tr>
							<th>'.__('Page Title', 'rosenlundsakeri').'</th>
							<td>
								<input type="text" name="rosen_fb_title" value="'.$fb_title.'" style="width: 100%;" />
							</td>
						</tr>

						<tr>
							<th>'.__('Content', 'rosenlundsakeri').'</th>
							<td>
								<textarea name="rosen_fb_content" style="width: 100%;" rows="5" >'.$fb_content.'</textarea>
							</td>
						</tr>

						<tr>
							<th>'.__('Media', 'rosenlundsakeri').'</th>
							<td>
								<input type="text" id="fb-img-url" name="rosen_fb_img" value="'.$fb_img.'" style="width: 88%;" />
								<a href="#" id="fb-upload-media" class="button" >'.__('Upload', 'rosenlundsakeri').'</a>
							</td>
						</tr>
					</tbody>
				</table>
			';

			echo $output;
	    }
	}

	public function save_info_box_for_social_media($post_id, $post){
 		if ( wp_is_post_revision( $post_id ) ){
			return $post_id;
 		}

        if ( !isset( $_POST['social_info_nonce'] ) || !wp_verify_nonce( $_POST['social_info_nonce'], 'social_info_nonce_action' ) ){
    		return $post_id;
        }

        //file_put_contents(__DIR__.'/ids.txt',$post_id."\n",FILE_APPEND);

        if ( in_array( $post->post_type, array('news','slider') ) ) {

			$rosen_fb_title 	= isset($_POST['rosen_fb_title'])?$_POST['rosen_fb_title']:'';
			$rosen_fb_content 	= isset($_POST['rosen_fb_content'])?$_POST['rosen_fb_content']:'';
			$rosen_fb_img 		= isset($_POST['rosen_fb_img'])?$_POST['rosen_fb_img']:'';
			
			update_post_meta($post_id, 'rosen_fb_title', $rosen_fb_title);
			update_post_meta($post_id, 'rosen_fb_content', $rosen_fb_content);
			update_post_meta($post_id, 'rosen_fb_img', $rosen_fb_img);
		}

        if( isset( $_POST[ 'notify_staff' ] ) ) {

            update_post_meta( $post_id, 'notify_staff', $_POST[ 'notify_staff' ] );

            $intranet_mirrored_post = get_post_meta( $post->ID, '_intranet_mirrored_post', true );
            if(!empty($intranet_mirrored_post)){
                $post_link = '';
            }else{
                $post_link = get_permalink($_POST['ID']);
            }

            /*send mail to all the active staffs*/
            /*switch to staff blog*/
            switch_to_blog(3);

            $staffs_mail = array();
            $staffs = get_users(array(
                'role' => 'rosen_staff'
            ));
            if(!empty($staffs)){
                foreach($staffs as $staff){
                    $staff_status = get_user_meta($staff->ID,'user_status',true);
                    if( 1 == $staff_status ){
                        $staffs_mail[] = $staff->data->user_email;
                    }
                }
            }

            if(!empty($staffs_mail)){

                if(empty($post_link)){
                    $post_link = get_permalink($intranet_mirrored_post);
                }

                global $post_type;
                $ev_intranet = get_option("ev_intranet");

                $subject = ( isset($ev_intranet["$post_type-subject"]) && !empty($ev_intranet["$post_type-subject"]) ) ? $ev_intranet["$post_type-subject"] : 'New Post on the site';

                $banner_image = ( isset($ev_intranet["$post_type-banner-image"]) && !empty($ev_intranet["$post_type-banner-image"]) ) ? $ev_intranet["$post_type-banner-image"]["url"] : '';
                $banner_title = ( isset($ev_intranet["$post_type-banner-title"]) && !empty($ev_intranet["$post_type-banner-title"]) ) ? $ev_intranet["$post_type-banner-title"] : $_POST["post_title"];
                $heading = ( isset($ev_intranet["$post_type-heading"]) && !empty($ev_intranet["$post_type-heading"]) ) ? $ev_intranet["$post_type-heading"] : '';
                $content = ( isset($ev_intranet["$post_type-content"]) && !empty($ev_intranet["$post_type-content"]) ) ? $ev_intranet["$post_type-content"] : '';
                $btn_text = ( isset($ev_intranet["$post_type-btn-text"]) && !empty($ev_intranet["$post_type-btn-text"]) ) ? $ev_intranet["$post_type-btn-text"] : 'Read More';

                $heading = str_replace( '{postTitle}', $_POST["post_title"], $heading );
                $content = str_replace( '{postContent}', wp_trim_words(ev_get_safe_content($_POST['content']), 40, '...' ), $content );

                $body = file_get_contents( get_template_directory().'/mail-template/notify.htm');

                $body = str_replace( '{BannerImage}', $banner_image, $body );
                $body = str_replace( '{BannerTitle}', $banner_title, $body );
                $body = str_replace( '{postTitle}', $heading, $body );
                $body = str_replace( '{postContent}', $content, $body );
                $body = str_replace( '{ButtonText}', $btn_text, $body );
                $body = str_replace( '{ButtonUrl}', $post_link, $body );

                $headers = array('Content-Type: text/html; charset=UTF-8');

                foreach($staffs_mail as $to){
                    //wp_mail( $to, $subject, $body, $headers );
                }
                restore_current_blog();
            }
            /**/
        } else {
            update_post_meta( $post_id, 'notify_staff', 0 );
        }
	}

	public function publish_to_facebook_meta() {
    	global $post;
    	global $wp;

	    $output = '';
    	
    	$base_url = site_url().$_SERVER['REQUEST_URI'];
    	
	    if ( in_array( $post->post_type, array('news','slider') ) ) {

            if( in_array( $post->post_type, array('news') ) ){
                $notify_staff = get_post_meta( $post->ID, 'notify_staff', true );
                ?>
                <div class="misc-pub-section misc-pub-notify">
                    <input id="notify-staff" type="checkbox" name="notify_staff" value="1" <?php checked( $notify_staff, '1' ) ?>/>
                    <label for="notify-staff"><?php _e('Notify Staffs','rosenlundsakeri');?></label>
                </div>
            <?php
            }

	   		$url = add_query_arg( array(
			    'publish' => 'facebook'
			), $base_url  );

	   		$output .='<div class="misc-pub-section misc-pub-social"><a href="'.$url.'" class="button" style="display: table;" ><i class="dashicons dashicons-facebook" style="display:table-cell; vertical-align: middle; padding-right: 5px;"></i>'.__('Publish to Facebook','rosenlundsakeri').'</a></div>';
	    }

	    echo $output;
	}

	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}