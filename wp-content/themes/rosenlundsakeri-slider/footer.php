<!-- FOOTER -->
<div class="footer">
    <div class="logo" ng-bind-html="settings.rosen_logo | trustedHtml" ng-logo-animate>

    </div><!-- /.logo -->

    <div class="wrapper">
        <div ng-if="ready" class="message" id="message">
            
            <div class="item">
                <div class="weather">
                    <div class="info">
                        
                        <!--Jönköping - Jönköping-->
                        <div id="c_eb82a1f58bc88171fb6fbbcb9e387ab4" class="normal">
                            <h2 style="color: #ffffff; margin: 0 0 3px; padding: 2px; font: bold 13px/1.2 Verdana; text-align: center;">klart.se</h2>
                        </div>
                        <script type="text/javascript" src="http://www.klart.se/widget/widget_loader/eb82a1f58bc88171fb6fbbcb9e387ab4"></script>
                    
                    </div><!-- /.info -->
                </div><!-- /.weather -->
            </div><!-- /item -->
            
            <div class="item">
                <div class="weather">
                    <div class="info">
                        
                        <!-- Stockholm - Stockholm -->
                        <div id="c_36a9d445cdd730b6a006b3c60dac57ed" class="normal">
                            <h2 style="color: #fff; margin: 0 0 3px; padding: 2px; font: bold 13px/1.2 Verdana; text-align: center;">Väder Stockholm</h2>
                        </div>
                        <script type="text/javascript" src="http://www.klart.se/widget/widget_loader/36a9d445cdd730b6a006b3c60dac57ed"></script>
                    
                    </div><!-- /.info -->
                </div><!-- /.weather -->
            </div><!-- /item -->
            
            <div class="item">
                <div class="weather">
                    <div class="info">     
                        <!-- Skåne - Malmö -->
                        <div id="c_2e72b7d0b50561f86d7e031efa8031fd" class="normal">
                            <h2 style="color: #fff; margin: 0 0 3px; padding: 2px; font: bold 13px/1.2 Verdana; text-align: center;">Väder Malmö</h2>
                        </div>
                        <script type="text/javascript" src="http://www.klart.se/widget/widget_loader/2e72b7d0b50561f86d7e031efa8031fd"></script>
                    
                    </div><!-- /.info -->
                </div><!-- /.weather -->
            </div><!-- /item -->
            
            <div class="item">
                <div class="weather">
                    <div class="info">
                        <!-- Dalarna - Borlänge -->
                        <div id="c_ddcf22f0df144ec08d862aa6766fd12e" class="normal">
                            <h2 style="color: #fff; margin: 0 0 3px; padding: 2px; font: bold 13px/1.2 Verdana; text-align: center;">Väder Borlänge</h2>
                        </div>
                        <script type="text/javascript" src="http://www.klart.se/widget/widget_loader/ddcf22f0df144ec08d862aa6766fd12e"></script>
                    </div><!-- /.info -->
                </div><!-- /.weather -->
            </div><!-- /item -->
            
            <div class="item">
                <div class="weather">
                    <div class="info">
                        <!-- Västra götaland - Göteborg ( gothenburg ) -->
                        <div id="c_d827fae298590c63c423ad0be5a7d193" class="normal">
                            <h2 style="color: #fff; margin: 0 0 3px; padding: 2px; font: bold 13px/1.2 Verdana; text-align: center;">Väder Gothenburg</h2>
                        </div>
                        <script type="text/javascript" src="http://www.klart.se/widget/widget_loader/d827fae298590c63c423ad0be5a7d193"></script>
                    </div><!-- /.info -->
                </div><!-- /.weather -->
            </div><!-- /item -->
            
            <div class="item">
                <div class="weather">
                    <div class="info">
                        <!-- Kalmar - Oskarshamn  -->
                        <div id="c_cd931dc28d712d83442c9da782dc5e11" class="normal">
                            <h2 style="color: #fff; margin: 0 0 3px; padding: 2px; font: bold 13px/1.2 Verdana; text-align: center;">Väder Oskarshamn</h2>
                        </div>
                        <script type="text/javascript" src="http://www.klart.se/widget/widget_loader/cd931dc28d712d83442c9da782dc5e11"></script>
                    </div><!-- /.info -->
                </div><!-- /.weather -->
            </div><!-- /item -->
            
            
            
            <!-- messages -->
            <ng-notice-slider items="noticeItems"></ng-notice-slider>
            
        </div><!-- /. -->
    </div><!-- /.wrapper -->

</div><!-- /.footer -->

<?php wp_footer(); ?>
</body>
</html>